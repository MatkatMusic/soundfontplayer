/*
 ==============================================================================
 
 SoundFontParser.cpp
 Created: 14 Nov 2018 7:15:05pm
 Author:  Charles Schiermeyer
 
 ==============================================================================
 */

#include "SoundFontParser.h"
/*
 these macros generate:
 
 const StringRef Types::riff {"RIFF"};
 const MemoryBlock Types::RIFF(Types::riff.text,Types::riff.length() );
 
 for all of the different 4-character codes in the SoundFont spec
 */
namespace SoundFont {
    namespace SF2 {

        //==============================================================================
        Types::DWORD Helpers::MemoryBlockToDWord(const MemoryBlock& mb)
        {
            jassert( mb.getSize() == 4 );
            
            BigInteger bi;
            bi.loadFromMemoryBlock(mb);
            return bi.toInteger();
        }
        
        String Helpers::MemoryBlockToString(const juce::MemoryBlock &mb)
        {
            jassert( mb.getSize() == 4 );
            
            auto hex = String::toHexString(mb.getData(),
                                           static_cast<int>(mb.getSize()));
            
            /*
             converts a single 8-bit char from the memoryblock into a CharPointer_UTF8
             */
            struct Blob
            {
                Blob(char& c)
                {
                    ch[0] = c;
                    ch[1] = 0;
                }
                char ch[2];
                operator CharPointer_UTF8()
                {
                    return CharPointer_UTF8(&ch[0]);
                }
            };
            
            auto ascii =    String( Blob(mb[0]) ) +
            String( Blob(mb[1]) ) +
            String( Blob(mb[2]) ) +
            String( Blob(mb[3]) );
            
            return hex + String(" ") + ascii;
        }
        
        bool Helpers::CompareFourCCIDWithToken(const Types::FOURCC& id,
                                               const MemoryBlock& token)
        {
            return ConvertFourCCToMemoryBlock(id) == token;
        }
        
        MemoryBlock Helpers::ConvertFourCCToMemoryBlock(Types::FOURCC id)
        {
            MemoryBlock mb(&id, sizeof(id) );
            return mb;
        }
        
        String Helpers::getChunkTypeAsString(Types::FOURCC id)
        {
            MemoryBlock mb( &id, sizeof(id) );
            
            return mb.toString();
        }
        //==============================================================================

//        //==============================================================================

        //==============================================================================

//        //==============================================================================

    } //end namespace SF2
} //end namespace SoundFont
