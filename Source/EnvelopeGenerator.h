/*
  ==============================================================================

    EnvelopeGenerator.h
    Created: 10 Dec 2018 7:37:23pm
    Author:  Charles Schiermeyer

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include <type_traits>

template<typename FloatingType>
class Ramp
{
public:
    static_assert( std::is_floating_point<FloatingType>::value, "Ramp must use a floating point type as the template argument" );
    
    Ramp() { reset(); }
    
    using StepFunc = std::function<FloatingType(size_t)>;
    using InitFunc = std::function<StepFunc(void)>;
    
    StepFunc getDefaultStepFunction()
    {
        /*
         @Sylveon came up with how to capture `this` pointer by copy so that I could break when a member of pThis is at a certain value.
         */
        return [pThis = this](size_t)
        {
            return pThis->getCurrentValue() + pThis->getStep();
        };
    }
    
    void setStepFunction( std::function<FloatingType(size_t)> f )
    {
        stepFunc = std::move(f);
    }
    ///returns true if the ramp has reached its target value
    bool tick()
    {
        currentValue = stepFunc(stepNum);
        countDown -= 1;
        stepNum += 1;
        if( countDown < 0 )
        {
            countDown = 0;
            currentValue = target;
            stepNum = 0;
            return true;
        }
        return false;
    }
    
    /**
     use this function to initialize your ramp if you need to change how it increments
     for example, if you wanted to change the ramp duration, you can use the getter functions to keep your startingValue, targetValue and stepFunc the same
     */
    void resetAndInit(FloatingType startingValue,
                      FloatingType targetValue,
                      size_t numSteps,
                      InitFunc initFunc = {})
    {
        reset();
        setStartingValue(startingValue);
        setTargetValue(targetValue, numSteps);
        stepFunc = initFunc ? initFunc() : getDefaultStepFunction();
    }
    
    FloatingType getCurrentValue() { return currentValue; }
    FloatingType getTarget() { return target; }
    FloatingType getStep() { return step; }
    int getCountDown() { return countDown; }
    int getStepNum() { return stepNum; }
    InitFunc getInitFunc() { return [&]() { return stepFunc; }; }
private:
    void reset()
    {
        currentValue = target = step = 0;
        countDown = stepNum = 0;
    }
    void setStartingValue(FloatingType startingValue)
    {
        currentValue = startingValue;
    }
    void setTargetValue(FloatingType targetValue, size_t numSteps)
    {
        //jassert( numSteps > 0 );
        numSteps = jmax(numSteps, static_cast<size_t>(1)); //prevent divide by 0
        target = targetValue;
        step = FloatingType(target - currentValue) / FloatingType(numSteps);
        countDown = static_cast<int>(numSteps);
        stepNum = 0;
    }
    FloatingType currentValue = 0, target = 0, step = 0;
    int countDown = 0, stepNum = 0;
    StepFunc stepFunc = {};
};

template<typename FloatingType>
struct EnvelopeGenerator
{
    using StepFunc = typename Ramp<FloatingType>::StepFunc;
    struct Segment
    {
        int length = 0;
        double targetValue = 0;
        String name;
        std::function<StepFunc(void)> initFunc;
        String getDescription() const;
    };
    
    static Segment MakeSegment(const String& name,
                               int length,
                               float target,
                               std::function<StepFunc(void)> i = {})
    {
        static_assert( std::is_floating_point<FloatingType>::value, "MakeSegment must use a floating point type as the template argument" );
        return {length, target, name, i};
    }
    
    EnvelopeGenerator() {}
    ~EnvelopeGenerator() = default;
    
    auto& getRamp() { return ramp; }
    //=======================================================
    auto getCurrentValue() { return ramp.getCurrentValue(); }
    auto getCurrentSegmentIndex() { return currentSegment; }
    bool hasCompleted() { return completed; }
    void setHasCompleted() { completed = true; }
    //=======================================================
    ///returns true if the envelope has run out of segments to tick and has completed.
    bool tick()
    {
        if( completed )
            return true;
        if( ramp.tick() ) //ramp finished
        {
            //try and advance to the next segment
            //if you're out of segments, return true, which means the envelope has completed.
            bool noSegmentsRemain = advance();
            return noSegmentsRemain; //try to advance to next segment
        }
        
        return false;
    }
 
    ///advances to the next segment in the envelope.  if you run out of segments, this will return true and mark the envelope as completed.
    bool advance()
    {
        ++currentSegment;
        if( currentSegment < segments.size() )
        {
            updateRampTarget(ramp.getCurrentValue());
            completed = false;
        }
        else
        {
            completed = true;
        }
        return completed;
    }

     ///resets the envelope to the initial state after adding segments, but before they begin executing.
    void reset(double startingLevel)
    {
        currentSegment = 0;
        completed = false;
        updateRampTarget(startingLevel);
    }
    //=======================================================
    void addSegment( Segment s ) //pass by value, not reference
    {
        segments.push_back( s );
    }
    
    void addSegments( std::vector<Segment> segments ) //pass by value, not reference
    {
        for( auto& segment : segments )
        {
            addSegment( segment );
        }
    }
    
    void clearSegments()
    {
        segments.clear();
        ramp.resetAndInit(0, 0, 1);
    }
    
    const Segment& getSegment(size_t segmentNum )
    {
        jassert( segmentNum < segments.size() );
        return segments[segmentNum];
    }
    
    /**
     initializes the ramp with the first segment
     */
    void initWithFirstSegment(double startingLevel)
    {
        jassert( segments.size() > 0 );
        update(startingLevel, segments.front() );
    }
    String getDescription() const;
    
private:
    bool completed = false;
    Ramp<double> ramp;
    std::vector<Segment> segments;
    size_t currentSegment = 0;
    
    void updateRampTarget(double startingValue)
    {
        if( segments.empty() ) return;
        
        jassert( segments.size() > currentSegment );
        update(startingValue, segments[currentSegment] );
    }
    
    void update(double startingValue, const Segment& target )
    {
        ramp.resetAndInit(startingValue, target.targetValue, target.length, target.initFunc);
//        if( target.stepFunc )
//            ramp.setStepFunction( target.stepFunc ); //pass a copy of the func so its not deleted if you happen to call getSegment()
//        else if( target.initFunc )
//            ramp.setStepFunction( target.initFunc() );
    }
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(EnvelopeGenerator)
};
//=======================================================
struct SF2Envelope
{
    SF2Envelope() = default;
    
    void setSustainLevel(double level) { sustainLevel = level; }
    ///resets the segments in the envelope
    void setAttackStages( std::vector<EnvelopeGenerator<double>::Segment> segments)
    {
        attackEnvelope.clearSegments();
        attackEnvelope.addSegments(segments);
    }
    void setReleaseStages( std::vector<EnvelopeGenerator<double>::Segment> segments)
    {
        releaseEnvelope.clearSegments();
        releaseEnvelope.addSegments(segments);
    }
    
    auto getLevel()
    {
        if( !attackEnvelope.hasCompleted()) //still attacking
        {
            return attackEnvelope.getCurrentValue();
        }
        else if( isSustaining ) //still sustaining
        {
            return sustainLevel;
        }
        else if( !releaseEnvelope.hasCompleted() ) //still releasing
        {
            return releaseEnvelope.getCurrentValue();
        }
        return 0.0;
    }
    
    void beginRelease()
    {
//        DBG( "Begin Release" );
        auto releaseStartingValue = attackEnvelope.getCurrentValue();
        attackEnvelope.setHasCompleted();
        isSustaining = false;
        releaseEnvelope.reset( releaseStartingValue );
        releaseEnvelope.initWithFirstSegment(releaseStartingValue);
//        DBG( "[release settings] ");
//        DBG( "starting value: " << releaseStartingValue  );
//        auto& segment = releaseEnvelope.getSegment(releaseEnvelope.getCurrentSegmentIndex());
//        DBG( "target value: " << segment.targetValue );
//        DBG( "length: " << segment.length );
    }
    
    ///returns true if the envelope has finished ticking through all stages
    bool tick()
    {
        if( !attackEnvelope.hasCompleted() )    //is attack done?
        {                                       //no!
            if( attackEnvelope.tick() /*|| attackEnvelope.hasCompleted()*/ )
            {
                DBG( "attack has completed. now Sustaining" );
                isSustaining = true;
            }
            return false;
        }
        //yes, attack is done
        if( isSustaining )
        {
            return false;
        }
        
        if( !releaseEnvelope.hasCompleted() )
        {
            if( releaseEnvelope.tick() /*|| releaseEnvelope.hasCompleted()*/ )
            {
                DBG( "release has completed" );
                return true;
            }
        }
        
        return false;
    }
    
    bool isReleasing()
    {
        return attackEnvelope.hasCompleted() && !isSustaining && !releaseEnvelope.hasCompleted();
    }
    
    bool hasCompleted()
    {
        return attackEnvelope.hasCompleted() && releaseEnvelope.hasCompleted();
    }
    
    void releaseQuick(int sampleRate)
    {
        //releaseQuickCallback = std::move(callback);
        auto currentLevel = getLevel();
        isSustaining = false;
        shouldReleaseQuick = true;
        attackEnvelope.setHasCompleted();
        releaseEnvelope.reset(currentLevel);
        auto& ramp = releaseEnvelope.getRamp();
        ramp.resetAndInit(currentLevel, 0.f, 5 * sampleRate / 1000 );
        ramp.setStepFunction( ramp.getDefaultStepFunction() );
    }
    
    void printDescription() const;
    void reset()
    {
        isSustaining = false;
        shouldReleaseQuick = false;
        attackEnvelope.reset(0.0);
        releaseEnvelope.reset(0.0);
        sustainLevel = 0.f;
    }
    
    void initAfterAddingStages()
    {
        attackEnvelope.initWithFirstSegment(0.0);
        releaseEnvelope.initWithFirstSegment(sustainLevel);
    }
    
    auto& getActiveRamp()
    {
        if(!attackEnvelope.hasCompleted()) return attackEnvelope.getRamp();
        
        return releaseEnvelope.getRamp();
    }
private:
    bool isSustaining = false;
    bool shouldReleaseQuick = false;
    EnvelopeGenerator<double> attackEnvelope;
    EnvelopeGenerator<double> releaseEnvelope;
    double sustainLevel = 0.0;
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(SF2Envelope)
};
