/*
  ==============================================================================

    BezierBox.h
    Created: 14 Dec 2018 3:28:03pm
    Author:  Charles Schiermeyer

  ==============================================================================
*/

#pragma once
#include "../JuceLibraryCode/JuceHeader.h"

struct Curve;
struct CurveNode : public Component
{
    enum ColorIDs
    {
        idleColor,
        draggedColor,
    };
    Point<double> pos;
    
    CurveNode(ComponentBoundsConstrainer* c);
    void mouseDown(const MouseEvent& e) override;
    void mouseDrag(const MouseEvent& e) override;
    void mouseUp(const MouseEvent& e) override;
    void paint(Graphics& g) override;
    
    ComponentDragger dragger;
    ComponentBoundsConstrainer* constrainer = nullptr;
    static const Colour NodeColor;
    static constexpr int NodeDiameter = 8;
    ///the curve that we are the right node of, which is attached to our left
    Curve* attachedCurveLeft = nullptr;
    ///the curve that we are the left node of, which is attached to our right.
    Curve* attachedCurveRight = nullptr;
    void setIsDragged(bool dragged);
private:
    bool isDragged = false;
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(CurveNode)
};

struct Curve : public Component
{
    static const Colour CurveColor;
    CurveNode* left = nullptr;
    CurveNode* right = nullptr;
    
    ///sets the node that will be the left node of this curve, and also updates the node->attachedCurveRight property on the node
    void linkLeft(CurveNode& l);
    ///sets the node that will be the right node of this curve, and also updates the node->attachedCurveLeft property on the node
    void linkRight(CurveNode& r);
    
    /*
     in Logic, when you drag up/down with curve tool
        you get the normal curve
     when you drag left/right with curve tool
        you get the S curve
     
     normal curve is a Quadratic with a single node
     S curve is a Cubic with 2 nodes mirrored along y=x
     */
    enum Mode
    {
        Linear,
        Quadratic,
        SCurve,
        MODES
    };
    CurveNode control;
    Mode mode = Mode::Linear;
    double controlX = 0.5, controlY = 0.5;
    
    enum ColorIDs
    {
        idleColor,
        draggedColor,
    };
    
    void setIsDragged(bool dragged);
    
    /*
     what I'd like to do for iterating the array of these is to
     for( auto* curve : curves )
     {
        if( curve->contains(x) ) return curve->getValueAt(x);
     }
     
     when you drag/add/delete a BezierControl, the curves[] array updates in the parent component
     */
    bool contains(double x);
    double getValueAt(double x);
    /*
     you should be able to drag a curve left/right/up/down
     that means there needs to be a clickable bounding box for it.
     2px on either side of the lineshould be fine.
     that means we'll need a hit-test
     
     that means this needs to be a component
     that also means this needs a ComponentDragger
     and the parent component needs to be alerted whenever this gets dragged
     */
    //ComponentDragger dragger;
    //ComponentBoundsConstrainer* constrainer = nullptr;
    
    Curve(ComponentBoundsConstrainer* c);
    //void mouseDown(const MouseEvent& e) override;
    /*
     when you drag a curve, the nodes for the curve need to be moved as well
     */
    //void mouseDrag(const MouseEvent& e) override;
    void paint(Graphics& g) override;
    void resized() override;
    bool hitTest(int x, int y) override;
    void showDragArea(bool showDrag);
    void showCurveArea(bool showCurve);
private:
    bool isDragged = false;
    bool showDrag = false;
    bool showCurve = false;
    static constexpr int curveMargin = 8;
    void updatePath();
    Path p;
    Point<float> getControlXY();
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(Curve)
};

struct CurveEditor : public Component
{
    enum class Type
    {
        Unipolar,
        Bipolar,
    };
    Type type = Type::Bipolar;
    CurveEditor(Type t);
    void resized() override;
    void paint(Graphics& g) override;
    void mouseDown(const MouseEvent& e) override;
    void mouseDrag(const MouseEvent& e) override;
    void mouseUp(const MouseEvent& e) override;
    void mouseMove(const MouseEvent& e) override;
    void mouseDoubleClick(const MouseEvent& e) override;
    OwnedArray<Curve>& getCurves() { return curves; }
private:
    OwnedArray<Curve> curves;
    OwnedArray<CurveNode> controlPoints;
    
    struct EdgeLimit
    {
    private:
        std::pair<int,int> limit;
    public:
        EdgeLimit() : left(limit.first), right(limit.second),
        top(limit.first), bottom(limit.second) { }
        int& left;
        int& right;
        int& top;
        int& bottom;
    };
    
    CurveNode* startNode = nullptr;
    EdgeLimit startNodeXLimit;
    CurveNode* endNode = nullptr;
    EdgeLimit endNodeXLimit;
    Component* draggedComponent = nullptr;
    Curve* clickedCurve = nullptr;
    ComponentBoundsConstrainer constrainer;
    ComponentDragger dragger;
    
    Rectangle<int> getUsableArea();
    /**
     returns the bounding box you can safely position nodes in so they look correct
     */
    Rectangle<int> getNodeArea();
    
    /**
     returns the area that is used to calculate the normalized x/y for the nodes
     */
    Rectangle<int> getXYArea();
    
    Array<float> dashLengths = {8, 8};
    void updateNodeNormalizedPosition(CurveNode& node);
    void updateCurvePosition(const MouseEvent& e, Curve& curve);
    Point<int> lastClickPos;
    CurveNode* createControlNode();
    ///Adds a curve to the curves[] OwnedArray, links it, adds it as a visible child, and returns it.
    Curve* createCurve(CurveNode* left, CurveNode* right);
    void resizeCurve(Curve* curve);
    
    void limitNodeX(CurveNode& node);
    void limitNodeY(CurveNode& node);
    void deleteNode(CurveNode* node);
    void insertNodeInCurve(Point<int> clickPos);
    void resortNodes();
    void resortCurves();
    
};

struct BezierNode : public Component
{
    static constexpr int nodeDiameter = 20;
    BezierNode(String name, ComponentBoundsConstrainer* constrainer_) : constrainer(constrainer_), n(name)
    {
        jassert( name.length() <= 1); //name must be a single character
        setSize(20,20);
    }
    void mouseDown(const MouseEvent& e) override;
    void mouseDrag(const MouseEvent& e) override;
    void paint(Graphics& g) override;
    ComponentDragger dragger;
    ComponentBoundsConstrainer* constrainer = nullptr;
    String n;
};

namespace Solvers
{
    /**
     maps the input within the range [min, max] as though it were on the line formed by [a,b] and returns the y value.
     
     you'll normally use this to map your normalized line [a,b] onto the t=0...NumSamples axis, and return the y value for position `t`
     */
    double linearSolver(double in,
                        double min,
                        double max,
                        Point<double> a,
                        Point<double> b,
                        double h = 1.0);
    
    
    double piecewiseSolver(int x, int xmin, int xmax, const std::vector<Point<double>>& points, double bias, double h=1.0 );
    
    Point<double> BezierSolver( double t, const std::vector<Point<double>>& points);
    
    double ParabolaSolver(double t, const std::vector<Point<double>>& points);
    
    static int factorial(int n);
}

template<class Derived>
struct BezierBase : public Component
{
    enum PolarityType
    {
        BiPolar,
        Unipolar
    };
    
    Path cachedPath;
    
    void paint(Graphics& g) override
    {
        g.fillAll(Colours::lightblue);
        g.setColour(Colours::white);
        auto bounds = getLocalBounds().toFloat();
        g.drawRect(bounds.reduced(1), 2);
        
        auto pathStart = getDerived().getPathStart();
        auto pathEnd = getDerived().getPathEnd();
        
        auto points = static_cast<Derived&>(*this).getPoints();
        Array<float> dashLengths = {8, 8};
        double h = getHeight();
        //double w = getWidth();
        
        auto threshold = 3.0;
        
        
        Array<Line<double>> lines;
        Rectangle<float> r(8,8);
        Point<double> lastQuadNode{-1,-1};
        cachedPath.clear();
        
        for( int i = 1; i < points.size(); ++i )
        {
            g.setColour(Colours::black);
            g.drawDashedLine({points[i-1].toFloat(), points[i].toFloat()},
                             dashLengths.getRawDataPointer(),
                             dashLengths.size());
            if( i < points.size() - 1)
            {
                Line<double> leftSide{points[i-1], points[i]};
                Line<double> rightSide{points[i], points[i+1]};
                
                auto leftNode = leftSide.getPointAlongLineProportionally(0.5 + bias/2.0);
                g.setColour(Colours::blue);
                r.setCentre(leftNode.toFloat());
                g.drawEllipse(r, 1.f);
                
                auto rightNode = rightSide.getPointAlongLineProportionally(0.5 - bias/2.0);
                g.setColour(Colours::green);
                r.setCentre(rightNode.toFloat());
                g.drawEllipse(r, 1.f);
                
                
                if( i == 1 ) //far left side
                {
                    Path p;
                    p.startNewSubPath( points[i-1].toFloat() );
                    p.lineTo( leftNode.toFloat() );
                    //g.setColour(Colours::orange);
                    //g.strokePath(p, PathStrokeType(3));
                    cachedPath.addPath(p);
                }
                else if( lastQuadNode != Point<double>(-1, -1) && lastQuadNode != leftNode )
                { //between two user-draggable nodes
                    Path p;
                    p.startNewSubPath(lastQuadNode.toFloat() );
                    p.lineTo( leftNode.toFloat() );
                    //g.setColour(Colours::green);
                    //g.strokePath(p, PathStrokeType(3));
                    cachedPath.addPath(p);
                }
                if( i == points.size()-2 ) //far right side
                {
                    Path p;
                    p.startNewSubPath(rightNode.toFloat() );
                    p.lineTo( points.back().toFloat() );
                    //g.setColour(Colours::black);
                    //g.strokePath(p, PathStrokeType(3));
                    cachedPath.addPath(p);
                }
                
                {
                    Path p;
                    p.startNewSubPath( leftNode.toFloat() );
                    p.quadraticTo( points[i].toFloat(), rightNode.toFloat() );
                    cachedPath.addPath(p);
                }
                
                
                lastQuadNode = rightNode;
            }
        }
        
        g.setColour(Colours::red);
        g.strokePath(cachedPath, PathStrokeType(3));
        
        return;
        for( int i = 0; i < lines.size()-1; ++i )
        {
            auto leftline = lines[i];
            auto rightLine = lines[i+1];
            Rectangle<float> r(8,8);
            
            if( i != 0 )
            {
                auto midPoint1 = leftline.getPointAlongLineProportionally(0.5 - bias/2.0);
                r.setCentre(midPoint1.toFloat());
                g.drawEllipse(r, 1.f);
            }
            
            if( i != lines.size() - 1 )
            {
                auto midPoint2 = rightLine.getPointAlongLineProportionally(0.5 + bias/2.0);
                r.setCentre(midPoint2.toFloat());
                g.drawEllipse(r, 1.f);
            }
        }
        
        return;
        g.setColour(Colours::green);
        std::vector<double> lineYpositions;
        Array<double> bezierYPositions;
        //auto h = getHeight();
        int idx = 0;
        for( int i = pathStart.x; i < pathEnd.x; ++i )
        {
            auto t = jmap(double(i), pathStart.x, pathEnd.x, 0.0, 1.0);
            Point<double> Bxy = Solvers::BezierSolver(t, points);
            
            if( Bxy.y < threshold || Bxy.y > bounds.getHeight()-threshold )
            {
                DBG( "Bxy.y: " << Bxy.y );
                Bxy.setY( jlimit(threshold,
                                 static_cast<double>(bounds.getHeight()-threshold),
                                 Bxy.y) );
            }
            bezierYPositions.add(Bxy.y);
            lineYpositions.push_back(0.0);
            for( auto& line : lines )
            {
                Point<double> p;
                Line<double> l(Point<double>(i,0), Point<double>(i,h));
                if( line.intersects( l, p) )
                {
                    lineYpositions.back() = p.y;
                }
            }
            Rectangle<float> rect(2, 2);
            auto y1 = lineYpositions[idx];
            auto y2 = bezierYPositions[idx];
            ++idx;
            
            rect.setCentre(i, (1-bias)*y1 + bias*y2);
            g.fillRect(rect);
            
            
        }
        
    }
    void resized() override
    {
        constrainer.setMinimumOnscreenAmounts(0xffffff, 0xffffff, 0xffffff, 0xffffff);
        constrainer.setSizeLimits(1, 1, getWidth()-1, getHeight()-1);
    }
    
    std::vector<Point<double>>& getNormalizedNodes() { return normalizedNodes; }
    /**
     affects how the curve is drawn.  a bias of 1.0 means the curve is entirely the bezier calculation. a bias of 0.0 means the curve is entirely the linear calculation
     */
    double bias = 1.0;
protected:
    std::vector<Point<double>> normalizedNodes;
    ComponentBoundsConstrainer constrainer;
    
    void updateNormalizedNodes()
    {
        auto points = getDerived().getPoints();
        auto bounds = getLocalBounds().withTrimmedTop(BezierNode::nodeDiameter/2)
                                      .withTrimmedBottom(BezierNode::nodeDiameter/2)
                                      .withTrimmedLeft(BezierNode::nodeDiameter/2)
                                      .withTrimmedRight(BezierNode::nodeDiameter/2);
        normalizedNodes.clear();
        normalizedNodes.resize( points.size() );
        
        auto w = bounds.getWidth();
        auto h = bounds.getHeight();
        for(int i = 0; i < points.size(); ++i )
        {
            auto& point = points[i];
            double x = (point.x-bounds.getX()) / w;
            double y = 1 - (point.y-bounds.getY()) / h;
            //DBG( "point[" << point.toString() << "]: (" << x << ", " << y << ")" );
            normalizedNodes[i] = Point<double>{x,y};
        }
    }
    
private:
    Derived& getDerived() { return static_cast<Derived&>(*this); }
    friend Derived;
};

struct QuadraticBezierComponent : public BezierBase<QuadraticBezierComponent>
{
    QuadraticBezierComponent();
    //void paint(Graphics& g) override;
    void resized() override;
    void mouseDrag(const MouseEvent& e) override;
private:
    friend BezierBase<QuadraticBezierComponent>;
    BezierNode controlNode{"c", &constrainer};
    std::vector<Point<double>> getPoints();
    Point<double> getPathStart();
    Point<double> getPathEnd();
};

struct CubicBezierComponent : public BezierBase<CubicBezierComponent>
{
    CubicBezierComponent();
    //void paint(Graphics& g) override;
    void resized() override;
    void mouseDrag(const MouseEvent& e) override;
    void mouseDown(const MouseEvent& e) override;
private:
    friend BezierBase<CubicBezierComponent>;
    BezierNode topRightNode{"",&constrainer}, bottomLeftNode{"", &constrainer};
    Point<int> lastTopRightNodePos, lastBottomLeftNodePos;
    std::vector<Point<double>> getPoints();
    Point<double> getPathStart();
    Point<double> getPathEnd();
    Component* draggedNode = nullptr;
};

struct BezierBox : public BezierBase<BezierBox>
{
    static constexpr int maxNodes = 12;
    BezierBox();
    void mouseDoubleClick(const MouseEvent& e) override;
    void mouseDrag(const MouseEvent& e) override;
    void mouseDown(const MouseEvent& e) override;
    void mouseMove(const MouseEvent& e) override;
    void resized() override;
    std::vector<Point<double>> getPoints();
    void setScale(double scale);
    OwnedArray<BezierNode>& getNodes() { return nodes; }
    
private:
    friend BezierBase<BezierBox>;
    //ComponentBoundsConstrainer startNodeConstrainer, endNodeConstrainer;
    BezierNode startNode{"s", nullptr}, endNode{"e",nullptr};
    OwnedArray<BezierNode> nodes;
    Point<double> getPathStart();
    Point<double> getPathEnd();
    Component* draggedNode = nullptr;
    void renumberNodes();
    double scale = 1.0;
};

struct BezierWidget : public Component
{
    BezierWidget();
    void resized() override;
    BezierBox& getBox() { return box; }
    Slider& getSlider() { return slider; }
private:
    BezierBox box;
    Slider slider{Slider::SliderStyle::LinearHorizontal, Slider::TextEntryBoxPosition::NoTextBox};
};
