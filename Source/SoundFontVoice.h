/*
  ==============================================================================

    SoundFontVoice.h
    Created: 14 Nov 2018 12:19:56am
    Author:  Charles Schiermeyer

  ==============================================================================
*/

#pragma once
#include "../JuceLibraryCode/JuceHeader.h"
//#include "SoundFontParser.h"
#include "SoundFont/Parser.h"
#include "EnvelopeGenerator.h"
#include <array>
/**
 A subclass of SynthesiserVoice that can play a SamplerSound.
 
 To use it, create a Synthesiser, add some SamplerVoice objects to it, then
 give it some SampledSound objects to play.
 
 @see SamplerSound, Synthesiser, SynthesiserVoice
 
 @tags{Audio}
 */

struct SoundFont2Sound; //forward declaration

class JUCE_API  SoundFont2Voice    : public SynthesiserVoice
{
public:
    //==============================================================================
    /** Creates a SamplerVoice. */
    SoundFont2Voice();
    
    /** Destructor. */
    ~SoundFont2Voice();
    
    //==============================================================================
    bool canPlaySound (SynthesiserSound*) override;
    
    void startNote (int midiNoteNumber, float velocity, SynthesiserSound*, int pitchWheel) override;
    void stopNote (float velocity, bool allowTailOff) override;
    
    void pitchWheelMoved (int newValue) override;
    void controllerMoved (int controllerNumber, int newValue) override;
    
    void renderNextBlock (AudioBuffer<float>&, int startSample, int numSamples) override;
    
    bool isPlayingOneShot();
    void stopPlayingAndSkipRelease();
    
    void setGenerator(SoundFont::SF2::GeneratorType type,
                      SoundFont::SF2::GeneratorAmountType amount);
    void offsetGenerator(SoundFont::SF2::GeneratorType type,
                         SoundFont::SF2::GeneratorAmountType amount);
    
    void overwriteModulator(const SoundFont::SF2::ZoneModulator* mod);
    void appendModulator(const SoundFont::SF2::ZoneModulator* mod);
    void resetModulators();
    void setBuffer(const std::shared_ptr<SoundFont::SF2::SF2Sample>& buffer);
    const std::shared_ptr<SoundFont::SF2::SF2Sample>& getBuffer() { return buffer; }
private:
    //==============================================================================
    double pitchRatio = 0;
    double sourceSamplePosition = 0;
    float lgain = 0, rgain = 0, attackReleaseLevel = 0, attackDelta = 0, releaseDelta = 0;
    bool isInAttack = false, isInRelease = false;
    bool skipRelease = false;
    
    void calcPitchRatio();
    int pitchWheel = 0;
    int playingNoteNum = 0;
    //float playedVelocity = 0.f;
    
    int loopStart = 0;
    int loopEnd = 0;
    int loopPosition = 0;
    int loopLength = 0;
    bool isLoopable = false;
    bool isLooping = false;
    
    void limitGenerator(SoundFont::SF2::GeneratorType type);
    
    std::array<SoundFont::SF2::GeneratorAmountType, SoundFont::SF2::GeneratorType::GENERATOR_LIST_END> generatorValues;
    Array<SoundFont::SF2::ZoneModulator> modulatorValues;
    std::shared_ptr<SoundFont::SF2::SF2Sample> buffer;
    
    void applyModulators();
    std::array<int, 127> ccValues;
    void updateVoiceParams();
    double fractionalMidiNoteInHz(double note, double freqOfA = 440.0);
    
    SF2Envelope volumeEG;
    
    float timecents2Secs(int timecents) { return static_cast<float>(pow(2.0, timecents / 1200.0)); }
    
    JUCE_LEAK_DETECTOR (SoundFont2Voice)
};
