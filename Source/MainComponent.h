/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "SoundFontAudioSource.h"
#include "SoundFontSynth.h"
//#include "SoundFontParser.h"
#include "SoundFont/Parser.h"
#include "EnvelopeGenerator.h"
#include "BezierBox.h"
//==============================================================================



struct SineWaveSound   : public SynthesiserSound
{
    SineWaveSound() {}
    bool appliesToNote    (int) override        { return true; }
    bool appliesToChannel (int) override        { return true; }
};

struct SineOsc
{
    void updatePitch(int midiNoteNumber)
    {
        auto cyclesPerSecond = MidiMessage::getMidiNoteInHertz (midiNoteNumber);
        auto cyclesPerSample = cyclesPerSecond / sampleRate;
        angleDelta = cyclesPerSample * 2.0 * MathConstants<double>::pi;
    }
    void setSampleRate(double rate) { sampleRate = rate; }
    float tick()
    {
        auto output = (float)std::sin (currentAngle);
        currentAngle += angleDelta;
        return output;
    }
    bool isInitialized() { return angleDelta != 0.0; }
    void reset() { angleDelta = currentAngle = 0.0; }
private:
    double sampleRate = 1.0;
    double currentAngle = 0.0;
    double angleDelta = 0.0;
};


#define USE_VOLUME_ENVELOPE 1
#define DEBUG_ENVELOPE_OUTPUT 1
struct SineWaveVoice : public SynthesiserVoice
{
    SineWaveVoice(BezierWidget& wi) : widget(wi) { }
    
    bool canPlaySound (SynthesiserSound* sound) override
    {
        return dynamic_cast<SineWaveSound*> (sound) != nullptr;
    }
    
    void pitchWheelMoved(int) override {}
    void controllerMoved(int, int) override {}
    
    void startNote (int midiNoteNumber, float velocity,
                    SynthesiserSound*, int /*currentPitchWheelPosition*/) override
    {
#if USE_VOLUME_ENVELOPE
        DBG( "startNote() voice ID: " << idNum );
        level = velocity * 0.15;
        tailOff = 0.0;
        
        osc.setSampleRate(getSampleRate());
        osc.reset();
        osc.updatePitch(midiNoteNumber);
        
        volumeEG.reset();
        
        constexpr double ramp4ms = 4 * 48000.0 /*getSampleRate()*/ / 1000.0;
        auto expFunc = [&](size_t step)
        {
            auto stepSize = 1.0 / (ramp4ms * 50);  //it takes ramp4ms steps to travel around the unit circle
            /*
             x * y^n = 1.0;
             so, x * y^step = some intermediate value when 0 < step < n
             */
            constexpr auto x = 0.001;
            static const auto y = std::pow(1.0 / x, stepSize);
            return x * std::pow(y, step);
        };
        
        auto attackLength = getSampleRate();
        auto bezierFunc = [this, al=attackLength](size_t step)
        {
            auto points = this->widget.getBox().getNormalizedNodes();
            //scale points so the last point is at x=attackLength
            for( auto& p : points )
            {
                p.x *= al;
            }

            return Solvers::piecewiseSolver(static_cast<int>(step),
                                            0,
                                            al,
                                            points,
                                            this->widget.getSlider().getValue()
                                            );
        };
        
        volumeEG.setAttackStages({
            EnvelopeGenerator<double>::MakeSegment("Attack",
                                                   //ramp4ms * 50,
                                                   attackLength,
                                                   1.0,
                                                   //[&]() { return expFunc; }),
                                                   [&]() { return bezierFunc; }),
            EnvelopeGenerator<double>::MakeSegment("Decay",
                                                   ramp4ms * 50,
                                                   0.65),
        });
        
        volumeEG.setSustainLevel(0.65);
        volumeEG.setReleaseStages({
            EnvelopeGenerator<double>::MakeSegment("Release",
                                                   ramp4ms * 50,
                                                   0.0f),
        });
        
        volumeEG.initAfterAddingStages();
#else
        tailOff = 0.0;
        tailIn = 0.001;
        level = velocity * 0.15;
        osc.setSampleRate(getSampleRate());
        osc.reset();
        osc.updatePitch(midiNoteNumber);
        tailInTime = int(getSampleRate() / 1000.0 * 5);
        tailInY = std::pow(1.0 / tailIn, 1.0 / double(tailInTime));
        //DBG( "tailInY: " << tailInY );
#endif
    }
    
    void stopNote (float /*velocity*/, bool allowTailOff) override
    {
#if USE_VOLUME_ENVELOPE
        if (allowTailOff)
        {
            DBG(" BEGIN RELEASE! voice ID: " << idNum );
            volumeEG.beginRelease();
            //volumeEG.releaseQuick(getSampleRate());
        }
        else
        {
            DBG(" KILL NOTE! ");
            volumeEG.releaseQuick(getSampleRate());
        }
#else
        if( allowTailOff )
        {
            if( tailOff == 0.0 )
                tailOff = 1.0;
        }
        else
        {
            clearCurrentNote();
            osc.reset();
        }
#endif
    }
    
    void renderNextBlock (AudioSampleBuffer& outputBuffer, int startSample, int numSamples) override
    {
#if USE_VOLUME_ENVELOPE
        if( osc.isInitialized() )
        {
            //DBG( "render output dB level: " << juce::Decibels::gainToDecibels(volumeEG.getLevel()));
            while (--numSamples >= 0)
            {
                if( !volumeEG.hasCompleted() )
                {
                    float volumeLevel = volumeEG.getLevel();
#if DEBUG_ENVELOPE_OUTPUT
                    /*
                     write volume level to output to visualize the envelope in Logic ProX
                     */
                    for (auto i = outputBuffer.getNumChannels(); --i >= 0;)
                        outputBuffer.addSample (i, startSample, volumeLevel);
                    ++startSample;
#else
                    process(outputBuffer, startSample, volumeLevel);
#endif
                    
                    if( volumeEG.tick() )
                    {
                        //DBG( "render: volumeEG.tick() completed envelope" );
                        
                        clearCurrentNote();
                        osc.reset();
                        volumeEG.reset();
                        break;
                    }
                }
            }
        }
#else
        if( osc.isInitialized() )
        {
            if( tailOff > 0.0 )
            {
                while( --numSamples >= 0 )
                {
#if DEBUG_ENVELOPE_OUTPUT
                    /*
                     tailOff is our envelope data.
                     write it to the output buffer to look at it in the DAW
                     */
                    for (auto i = outputBuffer.getNumChannels(); --i >= 0;)
                        outputBuffer.addSample (i, startSample, tailOff);
                    ++startSample;
#else
                    process(outputBuffer, startSample, level * tailOff );
#endif
                    tailOff *= 0.995;

                    if( tailOff <= 0.005 )
                    {
                        clearCurrentNote();
                        osc.reset();
                        break;
                    }
                }
            }
            else
            {
                while( --numSamples >= 0 )
                {
#if DEBUG_ENVELOPE_OUTPUT
                    /*
                     tailIn is our envelope data.
                     write it to the output buffer to look at it in the DAW
                     */
                    for (auto i = outputBuffer.getNumChannels(); --i >= 0;)
                        outputBuffer.addSample (i, startSample, tailIn);
                    ++startSample;
#else
                    process(outputBuffer, startSample, level * tailIn);
#endif
                    if( tailIn < 1.0 )
                    {
                        tailIn *= tailInY;
                    }

                }
            }
        }
#endif
    }
    double level = 0.0, tailOff = 0.0, tailIn = 0.0, tailInY = 0.001;
    int tailInTime = 192;
    SF2Envelope volumeEG;
    SineOsc osc;
private:
    inline void process(juce::AudioSampleBuffer &outputBuffer,
                        int &startSample,
                        float volumeLevel)
    {
        auto currentSample = osc.tick() * volumeLevel;
        for (auto i = outputBuffer.getNumChannels(); --i >= 0;)
            outputBuffer.addSample (i, startSample, currentSample);
        ++startSample;
    }
    
    static int voiceIDCounter;
    BezierWidget& widget;
    int idNum = voiceIDCounter++;
};

class SynthAudioSource   : public AudioSource
{
public:
    SynthAudioSource (MidiKeyboardState& keyState, BezierWidget& widget)
    : keyboardState (keyState)
    {
        for (auto i = 0; i < 2; ++i)
            synth.addVoice (new SineWaveVoice(widget));
        synth.addSound (new SineWaveSound());
    }
    ~SynthAudioSource() {}
    void setUsingSineWaveSound()
    {
        synth.clearSounds();
    }
    void prepareToPlay (int /*samplesPerBlockExpected*/, double sampleRate) override
    {
        synth.setCurrentPlaybackSampleRate (sampleRate); // [3]
    }
    void releaseResources() override {}
    void getNextAudioBlock (const AudioSourceChannelInfo& bufferToFill) override
    {
        bufferToFill.clearActiveBufferRegion();
        MidiBuffer incomingMidi;
        keyboardState.processNextMidiBuffer (incomingMidi, bufferToFill.startSample,
                                             bufferToFill.numSamples, true);
        
        tempBuffer.setSize(bufferToFill.buffer->getNumChannels(),
                           bufferToFill.buffer->getNumSamples());
        tempBuffer.clear();
        
        synth.renderNextBlock(tempBuffer,
                              incomingMidi,
                              0,
                              tempBuffer.getNumSamples());
        
        //simple hard clip of each sample.  apparently causes distortion???
//        for( int channelNum = 0; channelNum < tempBuffer.getNumChannels(); ++channelNum )
//        {
//            for( int sampleNum = 0; sampleNum < tempBuffer.getNumSamples(); ++sampleNum )
//            {
//                float clipped = jlimit(-0.95f,
//                                       0.95f,
//                                       tempBuffer.getSample(channelNum, sampleNum));
//                tempBuffer.setSample(channelNum,
//                                     sampleNum,
//                                     clipped);
//            }
//        }
        
        for( int i = 0; i < tempBuffer.getNumChannels(); ++i )
        {
            bufferToFill.buffer->addFrom(i,
                                         0,
                                         tempBuffer,
                                         i,
                                         0,
                                         tempBuffer.getNumSamples() );
        }
        
//        synth.renderNextBlock (*tempBuffer.buffer, incomingMidi,
//                               tempBuffer.startSample, tempBuffer.numSamples);
    }
    Synthesiser synth;
private:
    AudioBuffer<float> tempBuffer;
    MidiKeyboardState& keyboardState;
};
//==============================================================================
class MainComponent   : public AudioAppComponent, public MidiInputCallback
{
public:
    //==============================================================================
    MainComponent();
    ~MainComponent();

    //==============================================================================
    void prepareToPlay (int samplesPerBlockExpected, double sampleRate) override;
    void getNextAudioBlock (const AudioSourceChannelInfo& bufferToFill) override;
    void releaseResources() override;

    //==============================================================================
    void paint (Graphics& g) override;
    void resized() override;

    //==============================================================================
    void handleIncomingMidiMessage(MidiInput *source, const MidiMessage &message) override;
    void handlePartialSysexMessage(MidiInput *source, const uint8 *messageData, int numBytesSoFar, double timestamp) override {}
private:
    //==============================================================================
    // Your private member variables go here...
    MidiKeyboardState state;
    MidiKeyboardComponent keyboard{state, MidiKeyboardComponent::Orientation::horizontalKeyboard};
    SoundFontAudioSource audioSource{state};
    
    BezierWidget bezierBox;
    CurveEditor curveEditor{CurveEditor::Type::Bipolar};
    SynthAudioSource synthAudioSource{state, bezierBox};
    
    ComboBox availablePresetsComboBox;
    
    WeakReference<SoundFont::SF2::Parser> parser;
    
    CubicBezierComponent bezierComponent;
    QuadraticBezierComponent quadComponent;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};
