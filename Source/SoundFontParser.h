/*
 ==============================================================================
 
 SoundFontParser.h
 Created: 14 Nov 2018 7:15:05pm
 Author:  Charles Schiermeyer
 
 ==============================================================================
 */

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include <map>
#include <any>

#include "SoundFont/Types.h"

namespace SoundFont
{
    /*
     if I ever add support for SfArk:
     https://github.com/raboof/sfArkLib
     https://github.com/raboof/sfarkxtc
     */
    /*
     if I ever add support for SFZ
     http://www.sfzformat.com/legacy/
     */
    /*
     if I ever add support for SF3
     https://musescore.org/en/node/63911
     basically, Decode wav data as OGG. 
     */
    namespace SF2
    {

        
        namespace Helpers
        {
            /** converts a 4-byte memoryblock to a DWORD(uint32) */
            Types::DWORD MemoryBlockToDWord(const MemoryBlock& mb);
            
            /** converts a 4-byte memoryBlock to a hexadecimal string */
            String MemoryBlockToString(const MemoryBlock& mb);
            
            MemoryBlock ConvertFourCCToMemoryBlock(Types::FOURCC id);
            
            bool CompareFourCCIDWithToken(const Types::FOURCC& id,
                                          const MemoryBlock& Token);
            
            /** returns the data type of this chunk's data as a string */
            String getChunkTypeAsString(Types::FOURCC id);
        }

//        namespace detail
//        {
//            struct InstrumentSample
//            {
//                SF2::SampleHeader* header = nullptr; //points to file.sampleHeadersList[n]
//                AudioBuffer<float> buffer;
//            };
//
//            struct InstrumentZone
//            {
//                /*
//                 an InstrumentZone is global if the last generator is not a sampleID generator
//                 */
//                Array<SF2::Generator*> instZoneGenerators;
//                Array<SF2::ZoneModulator*> instZoneModulators;
//                InstrumentSample* sample = nullptr;
//                bool isGlobal = false;
//            };
//
//            struct Instrument
//            {
//                SF2::Instrument* instrument; //points to file.instrumentsList[n]
//                Array<InstrumentZone> zones;
//            };
//
//            struct PresetZone
//            {
//                //(potentially global) preset generators
//                Array<SF2::Generator*> generators;
//                //(potentially global) preset modulators
//                Array<SF2::ZoneModulator*> modulators;
//
//                /*
//                 this is the result of processing generators
//                 a global zone is a zone with no Instrument generator in the list of generators for the zone
//                 instrument will be a nullptr if this zone is global
//                 */
//                std::unique_ptr<Instrument> instrument;
//                bool isGlobal = false;
//            };
//
//            struct Preset
//            {
//                Array<PresetZone> presetZones;
//                PresetHeader* presetHeader = nullptr; //points to file.presetHeadersList[n]
//                void printDescription();
//            };
//        }; //end namespace detail
        
//        struct SampleBufferPool : private Timer
//        {   //thanks @timur.audio for this
//            SampleBufferPool() { startTimer(3000); }
//            ~SampleBufferPool() { stopTimer(); }
//            
//            void add(const std::shared_ptr<detail::InstrumentSample>& obj)
//            {
//                if( obj.get() == nullptr ) return;
//                const ScopedLock sl(lock);
//                pool.emplace_back( obj );
//            }
//            const std::vector< std::shared_ptr< detail::InstrumentSample > >& getPool() { return pool; }
//        private:
//            void timerCallback() override
//            {
//                const ScopedLock sl(lock);
//                pool.erase(std::remove_if(pool.begin(),
//                                          pool.end(),
//                                          [](auto& object)
//                                          {
//                                              return object.use_count() <= 1;
//                                          }),
//                           pool.end() );
//            }
//            CriticalSection lock;
//            std::vector< std::shared_ptr< detail::InstrumentSample > > pool;
//            
//            
//        };
        
    } //end namespace SF2
    
} //end SoundFont namespace
