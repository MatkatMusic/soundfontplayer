/*
  ==============================================================================

    EnvelopeGenerator.cpp
    Created: 10 Dec 2018 7:37:23pm
    Author:  Charles Schiermeyer

  ==============================================================================
*/

#include "EnvelopeGenerator.h"

template<typename FloatType>
String EnvelopeGenerator<FloatType>::Segment::getDescription() const
{
    String str;
    
    str << "[Envelop Segment] name: " << name << " ";
    str << "length(samples): " << length << " ";
    str << "target value: " << targetValue;
    
    return str;
}
//=======================================================
template<typename FloatType>
String EnvelopeGenerator<FloatType>::getDescription() const
{
    String str;
    
    str << "[Envelope Generator] segments: " << "\n";
    for( const auto& segment : segments )
    {
        str << segment.getDescription() << "\n";
    }
    str << "completed: " << (completed ? "true" : "false" ) << "\n";
    str << "current segment: " << (int)currentSegment << "\n";
    
    return str;
}
//=======================================================
void SF2Envelope::printDescription() const
{
    String str;
    
    str << "[SF2Envelope Description]" << "\n";
    str << "    [attack envelope] \n";
    str << attackEnvelope.getDescription() << "\n";
    str << "    sustain level: " << sustainLevel << " sustainLevel(dB): " << juce::Decibels::gainToDecibels(sustainLevel)<< "\n";
    str << "    is sustaining: " << (isSustaining ? "true" : "false" ) << "\n";
    str << "    [release envelope] \n";
    str << releaseEnvelope.getDescription() << "\n";
    
    std::cout << str << std::endl;
}
