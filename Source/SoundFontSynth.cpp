/*
 ==============================================================================
 
 SoundFontSynth.cpp
 Created: 14 Nov 2018 12:19:35am
 Author:  Charles Schiermeyer
 
 ==============================================================================
 */

#include "SoundFontSynth.h"
#include "SoundFontVoice.h"
#include "SoundFontSound.h"

void SoundFontSynth::changePreset(int presetIndex, SoundFont::SF2::File &file)
{
    const ScopedLock sl(presetLock);
    auto* parser = SoundFont::SF2::Parser::getInstance();
    preset = parser->buildCompletePreset(presetIndex, file);
    //preset->printDescription();
}

void SoundFontSynth::noteOn (int midiChannel,
                             int midiNoteNumber,
                             float velocity)
{
    DBG( "SoundFontSynth::noteOn" );
    const ScopedLock sl(presetLock);
    /*
     this function's logic is a port of MuseScore's Fluid synth's 'Preset::noteOn()'
     */
    /*
     for( all layers in preset)
        find matching layer
        find free voice
        set up voice (apply generators & modulators)
        start voice
     */
    if( preset.get() == nullptr )
    {        jassertfalse;
        return;
    }
    
    auto between = [](auto val, auto low, auto high)
    {
        return low <= val && val <= high;
    };
    
    auto* parser = SoundFont::SF2::Parser::getInstance();
    
    /* run thru all the zones of this preset */
    for( const auto& preset_zone : preset->zones )
    {
        /* check if the note falls into the key and velocity range of this preset */
        SoundFont::SF2::Generator presetKeyRange = parser->getGenerator(preset_zone.generators,
                                                                  SoundFont::SF2::GeneratorType::keyRange);
        
        
        if( !between(midiNoteNumber,
                     presetKeyRange.genAmount.range.byLo,
                     presetKeyRange.genAmount.range.byHi ) )
            continue;
        
        //note falls within keyRange for this zone
        SoundFont::SF2::Generator presetVelRange = parser->getGenerator(preset_zone.generators,
                                                                  SoundFont::SF2::GeneratorType::velRange);
        if( !between(midiNoteNumber,
                     presetVelRange.genAmount.range.byLo,
                     presetVelRange.genAmount.range.byHi) )
            continue;
        //note falls within velocity range for this preset zone
        
        auto* inst = preset_zone.instrument.get();
        if( inst == nullptr )
        {
            jassertfalse; //only global zones can omit the instrument
            continue;
        }
        
        /* run thru all the zones of this instrument */
        for( const auto& inst_zone : inst->zones )
        {
            /* make sure this instrument zone has a valid sample */
            if( inst_zone.sample.get() == nullptr ) continue;
            
            /* check if the note falls into the key and velocity range of this instrument */
            SoundFont::SF2::Generator instKeyRange = parser->getGenerator(inst_zone.generators,
                                                                            SoundFont::SF2::GeneratorType::keyRange);
            
            
            if( !between(midiNoteNumber,
                         instKeyRange.genAmount.range.byLo,
                         instKeyRange.genAmount.range.byHi ) )
                continue;
            
            //note falls within keyRange for this zone
            SoundFont::SF2::Generator instVelRange = parser->getGenerator(inst_zone.generators,
                                                                            SoundFont::SF2::GeneratorType::velRange);
            if( !between(midiNoteNumber,
                         instVelRange.genAmount.range.byLo,
                         instVelRange.genAmount.range.byHi) )
                continue;
            //note falls within velocity range for this instrument zone
            
            /*
             this is a good zone. find a free SynthesiserVoice and initialize it
             */
            DBG( "valid zone sample: ");
            inst_zone.sample->printDescription();
            DBG( "num samples: " <<  inst_zone.sample->buffer.getNumSamples() );
            DBG( "num channels: " << inst_zone.sample->buffer.getNumChannels() );
            DBG( "RMS: " << inst_zone.sample->buffer.getRMSLevel(0, 0, inst_zone.sample->buffer.getNumSamples()));
            /*
             if a voice is already playing this sound and its not a one-shot sound,
                stop that voice, we're going to retrigger it
             */
            for( auto* voice : voices )
            {
                auto* sfVoice = dynamic_cast<SoundFont2Voice*>(voice);
                if( sfVoice == nullptr ) continue;
                
                if( sfVoice->isPlayingChannel(midiChannel) )
                    if( !sfVoice->isPlayingButReleased() )
                        if( sfVoice->getCurrentlyPlayingNote() == midiNoteNumber )
                            if( !sfVoice->isPlayingOneShot() )
                                sfVoice->stopPlayingAndSkipRelease();
            }
            
            SynthesiserVoice* voice = nullptr;
            SynthesiserSound* synthSound = nullptr;
            for( auto* sound : sounds )
            {
                if( sound->appliesToChannel(midiChannel) && sound->appliesToNote(midiNoteNumber) )
                {
                    voice = findFreeVoice(sound, midiChannel, midiNoteNumber, isNoteStealingEnabled());
                    synthSound = sound;
                    break;
                }
            }
            
            if( synthSound == nullptr )
            {
                //no matching synth sound
                jassertfalse;
                return;
            }
            
            if( voice == nullptr )
            {
                //no voices available!
                jassertfalse;
                return;
            }
            
            SoundFont2Voice* sf2Voice = dynamic_cast<SoundFont2Voice*>(voice);
            if( sf2Voice == nullptr )
            {
                //somehow you tricked a non-SoundFont2Voice into playing a SoundFont2Sound!
                jassertfalse;
                continue;
            }
            SoundFont2Sound* sf2Sound = dynamic_cast<SoundFont2Sound*>(synthSound);
            if( sf2Sound == nullptr )
            {
                //somehow you passed a non-SoundFont2Sound into the voice!
                jassertfalse;
                continue;
            }
            /*
             * SF 2.04 section 9.4 bullet 2:
             * A generator is defined as identical to another generator if its generator operator is the same in both generators.
             */
            auto isGeneratorIdentical = [](const SoundFont::SF2::Generator& a, const SoundFont::SF2::Generator& b)
            {
                return a.genOper == b.genOper;
            };
            /*
             * apply all generators
             * SF 2.01 section 9.4 'bullet' 4:
             *
             * A generator in a global instrument zone that is identical to a default generator supersedes or replaces the default generator.
             • A generator in a local instrument zone that is identical to a default generator or to a generator in a global instrument zone supersedes or replaces that generator.
             */
            for( auto i = 0; i < SoundFont::SF2::GeneratorType::GENERATOR_LIST_END; ++i )
            {
                auto genType = static_cast<SoundFont::SF2::GeneratorType>(i);
                SoundFont::SF2::GeneratorAmountType amount;
                
                if( auto* hasGen = parser->findGenerator(inst_zone.generators,
                                                         genType) )
                {
                    amount = hasGen->genAmount;
                }
                else if( auto* hasGen1 = parser->findGenerator(inst->globalZone.generators, genType))
                {
                    amount = hasGen1->genAmount;
                }
                else
                {
                    amount = parser->getDefaultGenerator(genType)->defaultValue;
                }
                sf2Voice->setGenerator(genType, amount);
            }
            
            /*
             SF Spec 9.4
             bullet 5: Points below (until noted) apply to Value Generators ONLY.
             bullet 6: A generator at the preset level adds to a generator at the instrument level if both generators are identical.
             bullet 7: A generator in a global preset zone that is identical to a default generator or to a generator in an instrument adds to that generator.
             bullet 8: A generator in a local preset zone that is identical to a generator in a global preset zone supersedes or replaces that generator in the global preset zone.
             
             */
            for( auto i = 0; i < SoundFont::SF2::GeneratorType::GENERATOR_LIST_END; ++i )
            {
                auto genType = static_cast<SoundFont::SF2::GeneratorType>(i);
                
                switch (genType)
                {
                    case SoundFont::SF2::GeneratorType::startAddressOffset:
                    case SoundFont::SF2::GeneratorType::endAddressOffset:
                    case SoundFont::SF2::GeneratorType::startLoopAddressOffset:
                    case SoundFont::SF2::GeneratorType::endLoopAddressOffset:
                    case SoundFont::SF2::GeneratorType::startAddressCoarseOffset:
                    case SoundFont::SF2::GeneratorType::endAddrsCoarseOffset:
                    case SoundFont::SF2::GeneratorType::startloopAddrsCoarseOffset:
                    case SoundFont::SF2::GeneratorType::keynum:
                    case SoundFont::SF2::GeneratorType::velocity:
                    case SoundFont::SF2::GeneratorType::endloopAddrsCoarseOffset:
                    case SoundFont::SF2::GeneratorType::sampleModes:
                    case SoundFont::SF2::GeneratorType::exclusiveClass:
                    case SoundFont::SF2::GeneratorType::overridingRootKey:
                        continue;
                        break;
                    default:
                    {
                        SoundFont::SF2::GeneratorAmountType amount;
                        //A generator in a local preset zone that is identical to a generator in a global preset zone supersedes or replaces that generator in the global preset zone.
                        if( auto* gen = parser->findGenerator(preset_zone.generators, genType) )
                        {
                            amount = gen->genAmount;
                        }
                        else if( auto* gen2 = parser->findGenerator(preset->globalZone.generators, genType) )
                        {
                            amount = gen2->genAmount;
                        }
                        //A generator in a global preset zone that is identical to a default generator or to a generator in an instrument adds to that generator.
                        sf2Voice->offsetGenerator(genType, amount);
                        break;
                    }
                }
            }
            
            //finished applying generators to voice
            //now apply modulators
            Array<SoundFont::SF2::ZoneModulator*> modulators;
            /* global instrument zone first, modulators: Put them all into a
             * list. */
            for( auto* mod : inst->globalZone.modulators )
            {
                modulators.add(mod);
            }
            
            /*
             A Modulator is defined as identical to another modulator if its source, destination, amount source, and transform
             are the same in both modulators.
             */
            auto isModulatorIdentical = [](const SoundFont::SF2::ZoneModulator& a, const SoundFont::SF2::ZoneModulator& b)
            {
                return
                    a.modSrcOper == b.modSrcOper &&
                    a.modDestOper == b.modDestOper &&
                    a.modAmtSrcOper == b.modAmtSrcOper &&
                    a.modTransOper == b.modTransOper;
            };
            
            /* local instrument zone, modulators.
             * Replace modulators with the same definition in the list:
             * SF 2.01 page 69, 'bullet' 8
             */
            for( auto* mod : inst_zone.modulators )
            {
                for( int i = 0; i < modulators.size(); ++i )
                {
                    if( auto* existingMod = modulators.getReference(i) )
                    {
                        if( existingMod->isIdenticalTo(*mod) )
                        {
                            modulators.set(i, nullptr);
                        }
                    }
                }
                modulators.add(mod);
            }
            
            
            //pass default modulators to sf2Voice
            for( const auto& mod : SoundFont::SF2::DefaultModulatorValues )
            {
                sf2Voice->overwriteModulator(&mod);
            }
            
            /* Add instrument modulators (global / local) to the voice, overwriting current(default) value */
            for( auto* mod : modulators )
            {
                sf2Voice->overwriteModulator(mod);
            }
            
            /* Global preset zone, modulators: put them all into a
             * list. */
            modulators.clear();
            for( auto* mod : preset->globalZone.modulators )
            {
                modulators.add(mod);
            }
            /* Process the modulators of the local preset zone.  Kick
             * out all identical modulators from the global preset zone
             * (SF 2.01 page 69, second-last bullet) */
            for( auto* mod : preset_zone.modulators )
            {
                for( int i = 0; i < modulators.size(); ++i )
                {
                    if( auto* existingMod = modulators.getReference(i) )
                    {
                        if( existingMod->isIdenticalTo(*mod) )
                        {
                            modulators.set(i, nullptr);
                        }
                    }
                }
                modulators.add(mod);
            }
            /* Add preset modulators (global / local) to the voice. */
            for( auto* mod : modulators )
            {
                
                sf2Voice->appendModulator(mod);
            }
            
            //generators are set
            //modulators are set
            //start the voice!!
            sf2Voice->setBuffer(inst_zone.sample);
            startVoice(sf2Voice, sf2Sound, midiChannel, midiNoteNumber, velocity);
            
        }//end  for( const auto& inst_zone : inst->zones )
        
    } //end for( const auto& preset_zone : preset->zones )
}


void SoundFontSynth::noteOff (int midiChannel,
                              int midiNoteNumber,
                              float velocity,
                              bool allowTailOff)
{
    const ScopedLock sl(presetLock);
    
    /*
    for( all layers in preset)
        find matching layer
        is there a voice playing this layer?
            trigger the release stage of this voice
     */
    if( preset.get() == nullptr ) return;
    
    auto matchingZones = findCompatibleZones(midiChannel, midiNoteNumber, velocity);
    if( matchingZones.size() == 0 )
    {
        /*
         there are no zones that match this channel/note/velocity combination
         that means we never turned on a voice
         */
        return;
    }
    
    for( auto* zone : matchingZones )
    {
        for( auto* voice : voices )
        {
            if( auto* sf2Voice = dynamic_cast<SoundFont2Voice*>(voice) )
            {
                if( sf2Voice->getBuffer().get() == zone->sample.get() )
                {
                    sf2Voice->stopNote(velocity, allowTailOff);
                }
            }
        }
    }
    
    //Synthesiser::noteOff(midiChannel, midiNoteNumber, velocity, allowTailOff);
    
     //#TODO: We are delegating the task of triggering the release stage of a sample to the SoundFontVoice class
    //Synthesiser::noteOff() will trigger the appropriate voices to stop looping and start releasing.
}

void SoundFontSynth::allNotesOff (int midiChannel,
                                  bool allowTailOff)
{
    const ScopedLock sl(presetLock);
    Synthesiser::allNotesOff(midiChannel, allowTailOff);
}

void SoundFontSynth::handlePitchWheel (int midiChannel,
                                       int wheelValue)
{
//    Synthesiser::handlePitchWheel(midiChannel, wheelValue);
    if( preset.get() == nullptr ) return;
}

void SoundFontSynth::handleController (int midiChannel,
                                       int controllerNumber,
                                       int controllerValue)
{
    //#TODO: SoundFont spec has default behavior requirements for certain CC messages
    Synthesiser::handleController(midiChannel, controllerNumber, controllerValue);
    if( preset.get() == nullptr ) return;
}

Array<SoundFont::SF2::SF2InstrumentZone*> SoundFontSynth::findCompatibleZones(int midiChannel, int midiNoteNumber, float velocity)
{
    if( preset.get() == nullptr ) return {};
    
    auto between = [](auto val, auto low, auto high)
    {
        return low <= val && val <= high;
    };
    
    auto* parser = SoundFont::SF2::Parser::getInstance();
    
    Array<SoundFont::SF2::SF2InstrumentZone*> compatibleZones;
    /* run thru all the zones of this preset */
    for( const auto& preset_zone : preset->zones )
    {
        /* check if the note falls into the key and velocity range of this preset */
        SoundFont::SF2::Generator presetKeyRange = parser->getGenerator(preset_zone.generators,
                                                                        SoundFont::SF2::GeneratorType::keyRange);
        
        
        if( !between(midiNoteNumber,
                     presetKeyRange.genAmount.range.byLo,
                     presetKeyRange.genAmount.range.byHi ) )
            continue;
        
        //note falls within keyRange for this zone
        SoundFont::SF2::Generator presetVelRange = parser->getGenerator(preset_zone.generators,
                                                                        SoundFont::SF2::GeneratorType::velRange);
        if( !between(velocity * 127,
                     presetVelRange.genAmount.range.byLo,
                     presetVelRange.genAmount.range.byHi) )
            continue;
        //note falls within velocity range for this preset zone
        
        auto* inst = preset_zone.instrument.get();
        /* run thru all the zones of this instrument */
        if( inst == nullptr )
        {
            jassertfalse; //only global zones can omit the instrument
            continue;
        }
        for( auto& inst_zone : inst->zones )
        {
            /* make sure this instrument zone has a valid sample */
            if( inst_zone.sample.get() == nullptr ) continue;
            
            /* check if the note falls into the key and velocity range of this instrument */
            SoundFont::SF2::Generator instKeyRange = parser->getGenerator(inst_zone.generators,
                                                                          SoundFont::SF2::GeneratorType::keyRange);
            
            
            if( !between(midiNoteNumber,
                         instKeyRange.genAmount.range.byLo,
                         instKeyRange.genAmount.range.byHi ) )
                continue;
            
            //note falls within keyRange for this zone
            SoundFont::SF2::Generator instVelRange = parser->getGenerator(inst_zone.generators,
                                                                          SoundFont::SF2::GeneratorType::velRange);
            if( !between(velocity * 127,
                         instVelRange.genAmount.range.byLo,
                         instVelRange.genAmount.range.byHi) )
                continue;
            //note falls within velocity range for this instrument zone
            
            /* this is a good zone. */
            compatibleZones.add( &inst_zone );
        }
    }
    
    return compatibleZones;
}
