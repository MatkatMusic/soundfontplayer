/*
  ==============================================================================

    SoundFontAudioSource.cpp
    Created: 14 Nov 2018 12:34:55am
    Author:  Charles Schiermeyer

  ==============================================================================
*/

#include "SoundFontAudioSource.h"
#include "SoundFontVoice.h"
#include "SoundFontSound.h"

SoundFontAudioSource::SoundFontAudioSource(MidiKeyboardState& keyState) :
    state( keyState )
{
    for( auto i = 0; i < 4; ++i )
    {
        synth.addVoice( new SoundFont2Voice() );
    }
}

void SoundFontAudioSource::prepareToPlay(int, double sampleRate)
{
    synth.setCurrentPlaybackSampleRate(sampleRate);
}

void SoundFontAudioSource::releaseResources()
{
    
}

void SoundFontAudioSource::getNextAudioBlock(const AudioSourceChannelInfo &bufferToFill)
{
    bufferToFill.clearActiveBufferRegion();
    
    MidiBuffer incomingMidi;
    state.processNextMidiBuffer(incomingMidi,
                                bufferToFill.startSample,
                                bufferToFill.numSamples,
                                true);
    
    synth.renderNextBlock(*bufferToFill.buffer,
                          incomingMidi,
                          bufferToFill.startSample,
                          bufferToFill.numSamples);
}
