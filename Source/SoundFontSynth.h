/*
  ==============================================================================

    SoundFontSynth.h
    Created: 14 Nov 2018 12:19:35am
    Author:  Charles Schiermeyer

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

/*
 Look at MuseScore's Preset::noteOn for:
    logic to find proper zone/sample
    populate generator values into synth parameters from preset
    start the voice playing back
 
 look at MuseScore's Fluid::alloc_voice for:
    initializing the voice
    adding default modulators to synth
 
 look at MuseScore's Voice::init for:
    setup of initial voice parameters for playback including:
        volume envelope initialization
        modulation envelope initialization
        modulation LFO initialization
        vibrato LFO initialization
 
 look at MuseScore's Voice::write for:
    writing of audio data to the audio buffer.
    this is where all of the DSP gets applied to the original SoundFont sample
 
 look at MuseScore's Fluid::start_voice and Voice::kill_excl for:
    handling of the Exclusive Class generator
 
 look at MuseScore's Voice::GEN() for:
    computation of generator's output after applying mods and NRPN
 
 what is NRPN?
    see section 9.6 of soundfont spec
 where are generators applied to voice?
    see Voice::update_param()
    see Voice::voice_start()
 Where are the default generators defined?
    gen.cpp, line 27
 where are the default modulators defined?
    MuseScore's fluid.cpp line 48
 
 MuseScore's Fluid voice uses 2 arrays for modulators and generators
    the generator array uses the Generator enumerator to index into the array.
    so, gen[startAddressOffset] would hold the startAddressOffset amount
    it seems like a good system to follow
    modulators are handled in a similar manner
 
 */

#include "SoundFont/File.h"
#include "SoundFont/Parser.h"

struct SoundFontSynth : public juce::Synthesiser
{
    void changePreset(int presetIndex, SoundFont::SF2::File& file);
    /** Triggers a note-on event.
     
     The default method here will find all the sounds that want to be triggered by
     this note/channel. For each sound, it'll try to find a free voice, and use the
     voice to start playing the sound.
     
     Subclasses might want to override this if they need a more complex algorithm.
     
     This method will be called automatically according to the midi data passed into
     renderNextBlock(), but may be called explicitly too.
     
     The midiChannel parameter is the channel, between 1 and 16 inclusive.
     */
    void noteOn (int midiChannel,
                 int midiNoteNumber,
                 float velocity) override;
    
    /** Triggers a note-off event.
     
     This will turn off any voices that are playing a sound for the given note/channel.
     
     If allowTailOff is true, the voices will be allowed to fade out the notes gracefully
     (if they can do). If this is false, the notes will all be cut off immediately.
     
     This method will be called automatically according to the midi data passed into
     renderNextBlock(), but may be called explicitly too.
     
     The midiChannel parameter is the channel, between 1 and 16 inclusive.
     */
    void noteOff (int midiChannel,
                  int midiNoteNumber,
                  float velocity,
                  bool allowTailOff) override;
    
    /** Turns off all notes.
     
     This will turn off any voices that are playing a sound on the given midi channel.
     
     If midiChannel is 0 or less, then all voices will be turned off, regardless of
     which channel they're playing. Otherwise it represents a valid midi channel, from
     1 to 16 inclusive.
     
     If allowTailOff is true, the voices will be allowed to fade out the notes gracefully
     (if they can do). If this is false, the notes will all be cut off immediately.
     
     This method will be called automatically according to the midi data passed into
     renderNextBlock(), but may be called explicitly too.
     */
    void allNotesOff (int midiChannel,
                      bool allowTailOff) override;
    
    /** Sends a pitch-wheel message to any active voices.
     
     This will send a pitch-wheel message to any voices that are playing sounds on
     the given midi channel.
     
     This method will be called automatically according to the midi data passed into
     renderNextBlock(), but may be called explicitly too.
     
     @param midiChannel          the midi channel, from 1 to 16 inclusive
     @param wheelValue           the wheel position, from 0 to 0x3fff, as returned by MidiMessage::getPitchWheelValue()
     */
    void handlePitchWheel (int midiChannel,
                           int wheelValue) override;
    
    /** Sends a midi controller message to any active voices.
     
     This will send a midi controller message to any voices that are playing sounds on
     the given midi channel.
     
     This method will be called automatically according to the midi data passed into
     renderNextBlock(), but may be called explicitly too.
     
     @param midiChannel          the midi channel, from 1 to 16 inclusive
     @param controllerNumber     the midi controller type, as returned by MidiMessage::getControllerNumber()
     @param controllerValue      the midi controller value, between 0 and 127, as returned by MidiMessage::getControllerValue()
     */
    void handleController (int midiChannel,
                           int controllerNumber,
                           int controllerValue) override;
private:
    std::shared_ptr<SoundFont::SF2::SF2Preset> preset;
    CriticalSection presetLock;
    
    Array<SoundFont::SF2::SF2InstrumentZone*> findCompatibleZones(int midiChannel,
                                                  int midiNoteNumber,
                                                  float velocity);
};
