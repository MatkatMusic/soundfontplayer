/*
  ==============================================================================

    SoundFontSound.cpp
    Created: 14 Nov 2018 12:19:50am
    Author:  Charles Schiermeyer

  ==============================================================================
*/

#include "SoundFontSound.h"

/*
 SoundFont preset zones contain references to soundfont instruments
 
 soundFont instruments are separate from preset zones
 
 read presetHeader
    preset header lists zones
    zones might point to an instrument
 
 load a pointed-to instrument
    an instrument is a wrapper around an array of samples
 
 
 */



//struct SF2Sample
//{
//    SoundFont::SF2::SampleHeader* header;
//    AudioBuffer<float> buffer;
//};
//
//struct SF2InstrumentZone
//{
//    ///contains all of the generators for an InstrumentZone
//    Array<SoundFont::SF2::Generator*> generators;
//    Array<SoundFont::SF2::Modulator*> modulators; //
//    SF2Sample* sample = nullptr; //points to a loaded sample
//};
//struct SF2Instrument
//{
//    Array<SF2Sample*> samples;
//    Array<SF2InstrumentZone> zones;
//    Array<SoundFont::SF2::Modulator*> modulators; //affects global layer
//    SF2InstrumentZone globalZone;
//};
//
////=====================================================================
//struct SF2PresetZone
//{
//    Array<SoundFont::SF2::Generator*> generators;
//    Array<SoundFont::SF2::Modulator*> modulators; //affects global layer
//    SF2Instrument* instrument = nullptr; //points to loadedInstrument
//};
//struct SF2Preset
//{
//    Array<SF2PresetZone> zones;
//    SF2PresetZone globalZone;
//    SoundFont::SF2::PresetHeader* header = nullptr;
//};
//
//struct SF2Pool
//{
//    ReleasePool<SF2Sample> loadedSamples;
//    ReleasePool<SF2Instrument> loadedInstruments;
//    ReleasePool<SF2Preset> loadedPresets;
//};
/*
SoundFont2Sound::SoundFont2Sound (const String& soundName,
                                  AudioFormatReader& source,
                                  const BigInteger& notes,
                                  int midiNoteForNormalPitch,
                                  double attackTimeSecs,
                                  double releaseTimeSecs,
                                  double maxSampleLengthSeconds)
: name (soundName),
sourceSampleRate (source.sampleRate),
midiNotes (notes),
midiRootNote (midiNoteForNormalPitch)
{
    if (sourceSampleRate > 0 && source.lengthInSamples > 0)
    {
        length = jmin ((int) source.lengthInSamples,
                       (int) (maxSampleLengthSeconds * sourceSampleRate));
        
        data.reset (new AudioBuffer<float> (jmin (2, (int) source.numChannels), length + 4));
        
        source.read (data.get(), 0, length + 4, 0, true, true);
        
        attackSamples  = roundToInt (attackTimeSecs  * sourceSampleRate);
        releaseSamples = roundToInt (releaseTimeSecs * sourceSampleRate);
    }
}
*/
SoundFont2Sound::SoundFont2Sound(int presetIndex,
                                 SoundFont::SF2::File& file) :
soundFontFile(file)
{
    //auto* parser = SoundFont::SF2::Parser::getInstance();
    //auto completePreset = parser->buildCompletePreset(presetIndex, file);
    /*
     the preset contains everything you need to know about the sound
     it would probably be a good idea to cache some of the values in the preset
     for example: velocity ranges
     
     for every zone used in the preset, a SF2InstrumentZoneParameters object should be created
     */
    
}

SoundFont2Sound::~SoundFont2Sound()
{
}

bool SoundFont2Sound::appliesToNote (int midiNoteNumber)
{
    
    /*
     keyNum
     This enumerator forces the MIDI key number to effectively be interpreted as the
     value given. This generator can only appear at the instrument level.
     */
    return true; //midiNotes[midiNoteNumber];
    /*
    auto* parser = SoundFont::SF2::Parser::getInstance();
    int keyNum = -1;
    for( auto& zone : preset->zones )
    {
        if( auto* gen = parser->findGenerator(zone., GeneratorType::keynum) )
        {
            keyNum = gen->genAmount.wordAmount;
            break;
        }
    }
    if( keyNum != -1 )
    {
        if( keyNum == midiNoteNumber ) return true;
        return false;
    }
    
//     keyRange
//     This is the minimum and maximum MIDI key number values for which this preset
//     zone or instrument zone is active
    
    
    auto withinRange = [&midiNoteNumber](Generator* gen) -> bool
    {
        return  midiNoteNumber >= gen->genAmount.range.byLo &&
                midiNoteNumber <= gen->genAmount.range.byHi;
    };
    //global preset zone
    if( auto* gen = findGenerator(preset->globalZone.generators, GeneratorType::keyRange) )
    {
        return withinRange(gen);
    }
    //each preset zone
    for( auto& zone : preset->zones )
    {
        //first check preset zone globals
        if( auto* gen = findGenerator(zone.generators, GeneratorType::keyRange) )
        {
            return withinRange(gen);
        }
        //each preset zone's instrument's generators (global/zone)
        if( auto* gen = findGenerator(zone.instrument, GeneratorType::keyRange) )
        {
            return withinRange(gen);
        }
    }
    
    return false;
    */
}



bool SoundFont2Sound::appliesToChannel (int /*midiChannel*/)
{
    return true;
}

void SF2InstrumentZoneParameters::parseGenerator(const SoundFont::SF2::Generator &generator, bool fromPreset)
{
    auto& amt = generator.genAmount;
    auto& signedAmt = amt.shortAmount;
    auto& unsignedAmt = amt.wordAmount;
    auto& rangeAmt = amt.range;
    
    auto lambda = [fromPreset](auto& lhs, auto& rhs )
    {
        if( fromPreset ) { lhs += rhs; }
        else                lhs = rhs;
    };
    
    switch (generator.genOper.type)
    {
        case SoundFont::SF2::GeneratorType::startAddressOffset:
            lambda(startAddressOffset, unsignedAmt);
            break;
        case SoundFont::SF2::endAddressOffset:
            lambda(endAddressOffset, signedAmt);
            break;
        case SoundFont::SF2::startLoopAddressOffset:
            lambda(startLoopAddressOffset, signedAmt);
            break;
        case SoundFont::SF2::endLoopAddressOffset:
            lambda(endLoopAddressOffset, signedAmt);
            break;
        case SoundFont::SF2::startAddressCoarseOffset:
            lambda(startAddressCoarseOffset, signedAmt);
            break;
        case SoundFont::SF2::modLfoToPitch:
            lambda(modLFOtoPitch, signedAmt);
            break;
        case SoundFont::SF2::vibLfoToPitch:
            lambda(vibLFOtoPitch, signedAmt);
            break;
        case SoundFont::SF2::modEnvToPitch:
            lambda(modENVtoPitch, signedAmt);
            break;
        case SoundFont::SF2::initialFilterFC:
            lambda(initialFilterFrequency, unsignedAmt);
            break;
        case SoundFont::SF2::initialFilterQ:
            lambda(initialFilterQ, unsignedAmt);
            break;
        case SoundFont::SF2::modLfoToFilterFc:
            lambda(modLFOtoFilterFc, signedAmt);
            break;
        case SoundFont::SF2::modEnvToFilterFc:
            lambda(modENVtoFilterFc, signedAmt);
            break;
        case SoundFont::SF2::endAddrsCoarseOffset:
            lambda(endAddressCoarseOffset, signedAmt);
            break;
        case SoundFont::SF2::modLfoToVolume:
            lambda(modLFOtoVolume, signedAmt);
            break;
        case SoundFont::SF2::chorusEffectsSend:
            lambda(chorusEffectsSend, unsignedAmt);
            break;
        case SoundFont::SF2::reverbEffectsSend:
            lambda(reverbEffectsSend, unsignedAmt);
            break;
        case SoundFont::SF2::pan:
            lambda(pan, signedAmt);
            break;
        case SoundFont::SF2::delayModLFO:
            lambda(delayModLFO, signedAmt);
            break;
        case SoundFont::SF2::freqModLFO:
            lambda(freqModLFO, signedAmt);
            break;
        case SoundFont::SF2::delayVibLFO:
            lambda(delayVibLFO, signedAmt);
            break;
        case SoundFont::SF2::freqVibLFO:
            lambda(freqVibLFO, signedAmt);
            break;
        case SoundFont::SF2::delayModEnv:
            lambda(delayModENV, signedAmt);
            break;
        case SoundFont::SF2::attackModEnv:
            lambda(attackModENV, signedAmt);
            break;
        case SoundFont::SF2::holdModEnv:
            lambda(holdModENV, signedAmt);
            break;
        case SoundFont::SF2::decayModEnv:
            lambda(decayModENV, signedAmt);
            break;
        case SoundFont::SF2::sustainModEnv:
            lambda(sustainModENV, unsignedAmt);
            break;
        case SoundFont::SF2::releaseModEnv:
            lambda(releaseModENV, signedAmt);
            break;
        case SoundFont::SF2::keynumToModEnvHold:
            lambda(keynumToModENVhold, signedAmt);
            break;
        case SoundFont::SF2::keynumToModEnvDecay:
            lambda(keynumToModENVdecay, signedAmt);
            break;
        case SoundFont::SF2::delayVolEnv:
            lambda(delayVolENV, signedAmt);
            break;
        case SoundFont::SF2::attackVolEnv:
            lambda(attackVolENV, signedAmt);
            break;
        case SoundFont::SF2::holdVolEnv:
            lambda(holdVolENV, signedAmt);
            break;
        case SoundFont::SF2::decayVolEnv:
            lambda(decayVolENV, signedAmt);
            break;
        case SoundFont::SF2::sustainVolEnv:
            lambda(sustainVolENV, unsignedAmt);
            break;
        case SoundFont::SF2::releaseVolEnv:
            lambda(releaseVolENV, signedAmt);
            break;
        case SoundFont::SF2::keynumToVolEnvHold:
            lambda(keynumToVolENVhold, signedAmt);
            break;
        case SoundFont::SF2::keynumToVolEnvDecay:
            lambda(keynumToVolENVdecay, signedAmt);
            break;
        case SoundFont::SF2::instrument:
            lambda(instrument, unsignedAmt);
            break;
        case SoundFont::SF2::keyRange:
            lambda(keyRange.byHi, rangeAmt.byHi);
            lambda(keyRange.byLo, rangeAmt.byLo);
            break;
        case SoundFont::SF2::velRange:
            lambda(velRange.byHi, rangeAmt.byHi);
            lambda(velRange.byLo, rangeAmt.byLo);
            break;
        case SoundFont::SF2::startloopAddrsCoarseOffset:
            lambda(startLoopAddressCoarseOffset, signedAmt);
            break;
        case SoundFont::SF2::keynum:
            lambda(keyNum, unsignedAmt);
            break;
        case SoundFont::SF2::velocity:
            lambda(velocity, unsignedAmt);
            break;
        case SoundFont::SF2::initialAttenuation:
            lambda(initialAttenuation, unsignedAmt);
            break;
        case SoundFont::SF2::endloopAddrsCoarseOffset:
            lambda(endloopAddressCoarseOffset, signedAmt);
            break;
        case SoundFont::SF2::coarseTune:
            lambda(coarseTune, signedAmt);
            break;
        case SoundFont::SF2::fineTune:
            lambda(fineTune, signedAmt);
            break;
        case SoundFont::SF2::sampleID:
            lambda(sampleID, unsignedAmt);
            break;
        case SoundFont::SF2::sampleModes:
            lambda(sampleModes, signedAmt);
            break;
        case SoundFont::SF2::scaleTuning:
            lambda(scaleTuning, unsignedAmt);
            break;
        case SoundFont::SF2::exclusiveClass:
            lambda(exclusiveClass, unsignedAmt);
            break;
        case SoundFont::SF2::overridingRootKey:
            lambda(overridingRootkey, unsignedAmt);
            break;
        default:
            DBG("unhandled generator: " );
            generator.printDescription();
            jassertfalse;
            break;
    }
}


