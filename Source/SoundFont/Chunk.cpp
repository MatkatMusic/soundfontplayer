/*
  ==============================================================================

    Chunk.cpp
    Created: 25 Nov 2018 2:22:15am
    Author:  Charles Schiermeyer

  ==============================================================================
*/

#include "Chunk.h"
#include "File.h"

namespace SoundFont { namespace SF2 {
    
Chunk::Chunk(const juce::File& f, int64 start_) : start(start_), begin(start_), file(f)
{
    if( ScopedPointer<FileInputStream> fis = file.createInputStream() )
    {
        fis->setPosition(start);
        chunkID = Reader::readFourCC(*fis);
//        DBG( Helpers::getChunkTypeAsString(chunkID) );
        DBG( "chunkID: " << Types::fourCCtoString(chunkID) );
        
        //type = Types::getChunkType(chunkID);
        
        chunkDataSize = Reader::readDword(*fis);
        DBG( "size: " << (int)chunkDataSize );
        
        start = fis->getPosition();
        end = start + chunkDataSize;
    }
    else
    {
        //failed to read from file
        jassertfalse;
    }
}

String Chunk::getDescription()
{
    String str;
    str << "type: " << Types::fourCCtoString(chunkID) << " ";
    str << "start: " << start << " ";
    str << "chunk size: " << static_cast<int>(chunkDataSize) << " ";
    str << "end: " << end;
    return str;
}

bool Chunk::read(void *dest)
{
    if( ScopedPointer<FileInputStream> fis = file.createInputStream() )
    {
        jassert( start != -1 );
        jassert( end != -1 );
        fis->setPosition(start);
        int bytesRead = 0;
        
        bytesRead += fis->read(dest, chunkDataSize);
        
        jassert( bytesRead == chunkDataSize );
        return bytesRead == chunkDataSize;
    }
    jassertfalse; //failed to read from file
    return false;
}
} //end namespace SF2
}//end namespace SoundFont
