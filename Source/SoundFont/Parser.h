/*
 ==============================================================================
 
 Parser.h
 Created: 25 Nov 2018 2:08:00am
 Author:  Charles Schiermeyer
 
 ==============================================================================
 */

#pragma once
#include "Types.h"
#include "File.h"
#include "Chunk.h"
#include "ResourcePool.h"
#include <array>
/*
 
 The soundfont structure is kind of confusing.
 
 at the top level, you have the preset
 the preset contains zones.  These zones reference instruments, which can be used by multiple presets.
 
 Instruments contain zones.
 instrument zones reference individual samples.
 
 samples can be used by multiple instruments.
 
 think of it as:
 Samples have their default set of playback properties (length, loop point, root key, duration)
 
 The SoundFont wavetable synthesizer makes uses of these properties to facilitate playback of the samples in a meaningful way.
 
 Instrument Zones can modify/override these sample playback properties and more properties of the synth
 Instruments can augment/modify/override these properties of the zones via global Instrument properties
 
 PresetZones can modify/override the instrument zone properties
 Presets can modify/override the PresetZone properties via the Global Preset properties
 
 */
#define BEGIN_SOUNDFONT_NAMESPACE namespace SoundFont {
#define BEGIN_SF2_NAMESPACE namespace SF2 {

BEGIN_SOUNDFONT_NAMESPACE
BEGIN_SF2_NAMESPACE

struct SF2Sample
{
    SoundFont::SF2::SampleHeader* header = nullptr;
    AudioBuffer<float> buffer;
    void printDescription();
};

struct SF2InstrumentZone
{
    ///contains all of the non-global generators for an Instrument Zone
    Array<SoundFont::SF2::Generator*> generators;
    ///contains all of the non-global modulators for an Instrument Zone
    Array<SoundFont::SF2::ZoneModulator*> modulators; //
    ///points to a sample in Parser::getInstance()->resourcePool.loadedSamples
    std::shared_ptr<SF2Sample> sample = nullptr; //points to a loaded sample
    
    void printDescription();
};
struct SF2Instrument
{
    ///all of the zones for this instrument
    Array<SF2InstrumentZone> zones;
    ///the global zone
    SF2InstrumentZone globalZone;
    
    ///points to file.instrumentsList[n]
    SoundFont::SF2::Instrument* inst = nullptr;
    void printDescription();
};

//=====================================================================
struct SF2PresetZone
{
    ///the global generators for this preset zone
    Array<SoundFont::SF2::Generator*> generators;
    
    ///the global modulators for this preset zone
    Array<SoundFont::SF2::ZoneModulator*> modulators;
    
    ///points to an instrument in Parser::getInstance()->resourcePool.loadedInstruments
    std::shared_ptr<SF2Instrument> instrument = nullptr;
    void printDescription();
    bool operator==(const SF2PresetZone& other) const
    {
        return  instrument == other.instrument &&
        generators == other.generators &&
        modulators == other.modulators;
    }
};
struct SF2Preset
{
    ///all of the zones in this preset that contain an instrument
    Array<SF2PresetZone> zones;
    
    ///the global zone that affects all other zones in this preset
    SF2PresetZone globalZone;
    
    ///points to file.presetHeadersList[n]
    SoundFont::SF2::PresetHeader* header = nullptr;
    void printDescription();
};
//=====================================================================
struct SF2ResourcePool
{
    ReleasePool<SF2Sample> loadedSamples{"Sample Pool"};
    ReleasePool<SF2Instrument> loadedInstruments{"Instrument Pool"};
    ReleasePool<SF2Preset> loadedPresets{"Preset Pool"};
};

extern const std::array<GeneratorInfo, GeneratorType::GENERATOR_LIST_END> DefaultGeneratorValues;

extern const std::array<ZoneModulator, ModulatorDestinations::MODULATOR_LIST_END> DefaultModulatorValues;
struct Parser
{
    SF2::File* parse(const juce::File& sf2path );
    ~Parser();
    
    std::shared_ptr<SF2Preset> buildCompletePreset(int presetNum, const SF2::File& file);
    JUCE_DECLARE_SINGLETON(Parser, true)
    
    Generator* findGenerator( const Array<Generator*>& generators, GeneratorType selector );
    Generator* findGenerator( const std::shared_ptr<SF2Instrument>& instrument, GeneratorType selector );
    const GeneratorInfo* getDefaultGenerator(GeneratorType selector);
    Generator getGenerator( const Array<Generator*>& generators, GeneratorType selector );
private:
    File* parseSF2(FileInputStream& file);
    int64 parseChunk(Chunk& chunk, SF2::File& target);
    int64 parseString(String& str, Chunk &chunk);
    std::vector< std::unique_ptr<SF2::File> > parsedFiles;
    
    template<typename Type>
    int64 parse(Chunk &chunk,
                const std::function<void(MemoryInputStream&)>& parseImpl,
                Array<Type>& array)
    {
        MemoryBlock mb(chunk.getSize(), true);
        if( chunk.read(mb.getData() ) )
        {
            MemoryInputStream mis(mb, false);
            
            while( !mis.isExhausted() )
            {
                parseImpl(mis);
            }
            
            DBG( "array size: " << array.size() );
            return chunk.getEnd();
        }
        
        jassertfalse;
        return chunk.getStart() + 1;
    }
    
    template<typename T>
    static void GetGenerators(int start,
                              int end,
                              const Array<T>& list,
                              Array<T*>& generators)
    {
        for(auto genNum = start;
            genNum < end;
            ++genNum)
        {
            auto& generator = list.getReference( genNum );
            generator.printDescription();
            generators.add( &generator );
        }
    }
    
    template<typename T>
    static void GetModulators(int start,
                              int end,
                              const Array<T>& list,
                              Array<T*>& modulators)
    {
        GetGenerators(start,end,list,modulators);
    }
    
    std::shared_ptr<SF2Sample> getSampleBuffer(const SF2::SampleHeader& header,
                                               const SF2::File& file);
    
    SF2ResourcePool resourcePool;
    std::shared_ptr<SF2Instrument> getOrMakeInstrument(int instrumentIDX,
                                                       const SF2::File& file);
    
    std::shared_ptr<SF2Instrument> makeInstrument(int instrumentIDX,
                                                  const SF2::File& file);
    WeakReference<Parser>::Master masterReference;
    friend class WeakReference<Parser>;
    JUCE_LEAK_DETECTOR(Parser)
};

}//end namespace SF2
}//end namespace SoundFont

#undef BEGIN_SF2_NAMESPACE
#undef BEGIN_SOUNDFONT_NAMESPACE
