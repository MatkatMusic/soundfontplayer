/*
  ==============================================================================

    ResourcePool.h
    Created: 25 Nov 2018 2:26:02am
    Author:  Charles Schiermeyer

  ==============================================================================
*/

#pragma once
#include "../../JuceLibraryCode/JuceHeader.h"

template<typename T>
struct ReleasePool : private Timer
{
    ReleasePool(const String& n) : name(n) { startTimer(5000); }
    void add(const std::shared_ptr<T>& t)
    {
        if( t.get() == nullptr ) return;
        const ScopedLock sl(lock);
        pool.emplace_back(t);
    }
    bool contains(const T& t)
    {
        const ScopedLock sl(lock);
        for( auto& item : pool )
        {
            if( *item.get() == t ) return true;
        }
        return false;
    }
    const std::vector< std::shared_ptr<T> > getPool() { return pool; }
private:
    void timerCallback() override
    {
        if( poolSize != pool.size() )
        {
            DBG( "[" << name << "] num objects in pool: " << (int)pool.size() );
            poolSize = pool.size();
        }
        const ScopedLock sl(lock);
        pool.erase(std::remove_if(pool.begin(),
                                  pool.end(),
                                  [](const std::shared_ptr<T>& object)
                                  {
                                      return object.use_count() <= 1;
                                  }),
                   pool.end() );
    }
    CriticalSection lock;
    std::vector< std::shared_ptr<T> > pool;
    size_t poolSize = 0;
    String name;
};
