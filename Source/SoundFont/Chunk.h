/*
  ==============================================================================

    Chunk.h
    Created: 25 Nov 2018 2:22:15am
    Author:  Charles Schiermeyer

  ==============================================================================
*/

#pragma once
#include "../../JuceLibraryCode/JuceHeader.h"
#include "Types.h"
namespace SoundFont { namespace SF2 {

    struct Chunk
    {
        Chunk(const File& f, int64 start_);
        /** gets the type of data within the chunk. */
        //Types::ChunkType getType() { return type; }
        /** gets the ID of type of data within the chunk. */
        Types::FOURCC getID() { return chunkID; }
        /** get the size of the chunk data in bytes, excluding any pad byte. */
        Types::DWORD getSize() { return chunkDataSize; }
        /** gets the end of the chunkData */
        int64 getEnd() { return end; }
        /** gets the start of the chunkData */
        int64 getStart() { return start; }
        /** gets the start of the chunkID */
        int64 getBegin() { return begin; }
        /** gets the file we'll be reading. */
        const File& getFile() { return file; }
        String getDescription();
        bool read(void* dest);
    private:
        //Types::ChunkType type;
        /** identifies the type of data within the chunk. */
        Types::FOURCC chunkID;
        /** The size of the chunk data in bytes, excluding any pad byte. */
        Types::DWORD chunkDataSize;
        
        /** the end of the chunkData */
        int64 end;
        /** the start of the chunkData */
        int64 start;
        /** the start of the chunkID */
        int64 begin;
        /** the file we'll be reading. */
        const File& file;
    };
} //end namespace SF2
}//end namespace SoundFont
