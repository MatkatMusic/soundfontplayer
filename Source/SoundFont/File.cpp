/*
  ==============================================================================

    File.cpp
    Created: 25 Nov 2018 2:11:36am
    Author:  Charles Schiermeyer

  ==============================================================================
*/

#include "File.h"

namespace SoundFont { namespace SF2 {
    //==============================================================================
    Types::BYTE Reader::readByte(juce::InputStream& file)
    {
        return static_cast<Types::BYTE>(file.readByte());
    }
    
    Types::CHAR Reader::readChar(juce::InputStream& file)
    {
        return static_cast<Types::CHAR>(file.readByte());
    }
    
    Types::WORD Reader::readWord(juce::InputStream& file)
    {
        return static_cast<Types::WORD>(file.readShort());
    }
    
    Types::SHORT Reader::readShort(juce::InputStream& file)
    {
        return static_cast<Types::SHORT>(file.readShort());
    }
    
    Types::DWORD Reader::readDword(juce::InputStream& file)
    {
        return static_cast<Types::DWORD>(file.readInt());;
    }
    
    Types::FOURCC Reader::readFourCC(juce::InputStream& file)
    {
        Types::FOURCC fourCC;
        file.read( &fourCC, sizeof(Types::FOURCC) );
        return fourCC;
    }
    //==============================================================================
bool File::parsedOk()
{
    jassert( presetHeadersList.size() >= 2 );       //minimum 2 PHDR records
    jassert( presetZonesList.size() >= 2 );          //minimum 2 PBAG records
    jassert( presetModulatorsList.size() >= 1 );      //minimum 1 PMOD records
    jassert( presetGeneratorsList.size() >= 2 );      //minimum 2 PGEN records
    jassert( instrumentsList.size() >= 2 );         //minimum 2 INST records
    jassert( instrumentZonesList.size() >= 2 );      //minimum 2 IBAG records
    jassert( instrumentModulatorsList.size() >= 1 );  //minimum 1 IMOD records
    jassert( instrumentGeneratorsList.size() >= 2 );  //minimum 2 IGEN records
    jassert( sampleHeadersList.size() >= 2 );       //minimum 2 SHDR records
    
    return  presetHeadersList.size() >= 2 && // minimum 2 PHDR records
    presetZonesList.size() >= 2 && //minimum 2 PBAG records
    presetModulatorsList.size() >= 1 && //minimum 1 PMOD records
    presetGeneratorsList.size() >= 2 && //minimum 2 PGEN records
    instrumentsList.size() >= 2 && //minimum 2 INST records
    instrumentZonesList.size() >= 2 && //minimum 2 IBAG records
    instrumentModulatorsList.size() >= 1 && //minimum 1 IMOD records
    instrumentGeneratorsList.size() >= 2 && //minimum 2 IGEN records
    sampleHeadersList.size() >= 2; //minimum 2 SHDR records
}
}//end namespace SF2
}//end namespace SoundFont
