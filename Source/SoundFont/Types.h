/*
 ==============================================================================
 
 Types.h
 Created: 25 Nov 2018 1:58:43am
 Author:  Charles Schiermeyer
 
 ==============================================================================
 */

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include <stdexcept>
#include <map>
#include <type_traits>

template<typename IntegralType, IntegralType min, IntegralType max>
struct WrappingInteger
{
    //====================== Usage Requirements ====================//
    static_assert(std::is_integral<IntegralType>::value, "Integral type required");
    static_assert( min < max, "min must be less than max" );
    static_assert( max-min < std::numeric_limits<IntegralType>::max() / 2, "max - min must be less than half of the maximum allowed value for Integral Type to prevent overflow");
    
    //====================== Prefix & PostFix ====================//
    WrappingInteger& operator++() { ++val; return wrap(); }
    WrappingInteger  operator++(int) { auto tmp = *this; ++val; wrap(); return tmp; }
    WrappingInteger& operator--() { --val; return wrap();  }
    WrappingInteger  operator--(int) { auto tmp = *this; --val; wrap(); return tmp; }
    
    //====================== Addition Assignment =========================//
    template<typename OtherType>
    WrappingInteger& operator+=(const OtherType& other)
    {
        IntegralType temp = static_cast<IntegralType>(other);
        while( temp > range )
        {
            val += range;
            wrap();
            temp -= range;
        }
        val += temp;
        return wrap();
    }
    
    WrappingInteger& operator+=( const WrappingInteger& other) { val += other.val; return wrap(); }
    
    //====================== Subtraction Assignment =========================//
    template<typename OtherType>
    WrappingInteger& operator-=(const OtherType& other)
    {
        IntegralType temp = static_cast<IntegralType>(other);
        while( temp > range )
        {
            val -= range;
            wrap();
            temp -= range;
        }
        val -= temp;
        return wrap();
    }
    
    WrappingInteger& operator-=(const WrappingInteger& other) { val -= other.val; return wrap(); }
    
    //===============================================//
    ~WrappingInteger() = default;
    
    //======================= Constructors ========================//
    /** allows you to specify a default value, which will be wrapped to remain within the specified range */
    WrappingInteger(IntegralType v = min) : val(v) { wrap(); }
    
    WrappingInteger(const WrappingInteger& other) = default;
    
    template<typename OtherIntegralType>
    WrappingInteger(const OtherIntegralType& otherType) { val = otherType; wrap(); }
    
    //==================== Assignment Operator ===========================//
    WrappingInteger& operator=(const WrappingInteger& other) = default;
    
    template<typename OtherIntegralType>
    WrappingInteger& operator=(const OtherIntegralType& otherType) { val = otherType; return wrap(); }
    
    //==================== Explicit Conversion and Getters ===========================//
    //allows static_cast<int>(WrappedInteger<T, min, max>) and prevents implicit conversion to IntegralType
    explicit operator IntegralType() const { return val; }
    IntegralType value() const { return val; }
    
    //===================== Addition & Subtraction ==========================//
    //these allow for buffer[idx + 3]; and buffer[3 + idx] to be used, where idx is a WrappedInteger
    friend WrappingInteger operator+(WrappingInteger lhs, WrappingInteger rhs) { return lhs += rhs; }
    friend WrappingInteger operator-(WrappingInteger lhs, WrappingInteger rhs) { return lhs -= rhs; }
    
    //===================== Range info ==========================//
    static constexpr IntegralType rangeMin { min };
    static constexpr IntegralType rangeMax { max };
private:
    WrappingInteger& wrap()
    {
        val -= min;//shift everything so the min/max is 0, (max-min)
        while( val < 0 ) { val += range; }
        val %= (range);
        val += min; //shift everything back so min/max is min, max
        return *this;
    }
    IntegralType val = min;
    static constexpr IntegralType range = max - min;
};

template<typename T, size_t Size>
struct CircularBuffer
{
    CircularBuffer()
    {
        buffer.reserve(Size);
        numAvailableForReading = Size;
        /*
         start writing to the last valid index
         when the object is written, the wrapIndex() will wrap around so the next index written to is [0]
         this sets the number of readable values to 'size'.
         */
        indexToWriteTo = buffer.size() - 1; //start writing at the last valid index
        /*
         start reading from the first valid index
         this is the furthest distance away from the last written position
         */
        indexToReadFrom = 0;
    }
    void push(const T& t)
    {
        //assumes T has a copy constructor
        auto newT = std::make_unique<T>(t);
        buffer[indexToWriteTo++].swap(newT);
        numAvailableForReading += 1;
    }
    
    T* read(int& indexReadFrom)
    {
        T* t = nullptr;
        if( numAvailableForReading > 0 )
        {
            indexReadFrom = indexToReadFrom;
            t = buffer[indexToReadFrom++].get();
            numAvailableForReading -= 1;
        }
        return t;
    }
    
    int getNumAvailableForReading() { return numAvailableForReading; }
private:
    std::vector< std::unique_ptr<T> > buffer;
    
    WrappingInteger<int, 0, Size> indexToWriteTo, indexToReadFrom;
    int numAvailableForReading = 0;
};

/*
 if I ever add support for SfArk:
 https://github.com/raboof/sfArkLib
 https://github.com/raboof/sfarkxtc
 */
/*
 if I ever add support for SFZ
 http://www.sfzformat.com/legacy/
 */
/*
 if I ever add support for SF3
 https://musescore.org/en/node/63911
 basically, Decode wav data as OGG.
 */

#define BEGIN_SOUNDFONT_NAMESPACE namespace SoundFont {
#define BEGIN_SF2_NAMESPACE namespace SF2 {

BEGIN_SOUNDFONT_NAMESPACE

BEGIN_SF2_NAMESPACE
namespace Types
{
    using BYTE = juce::uint8;
    using CHAR = juce::int8;
    using WORD = juce::uint16;
    using SHORT = juce::int16;
    using DWORD = juce::uint32;
    
    using FOURCC = DWORD;
    
    constexpr FOURCC operator""_fourcc(const char* str, std::size_t size)
    {
        if( size != 4)
        {
            //FOURCC code must be 4 characters long
            throw std::runtime_error("FOURCC code must be 4 characters long");
        }
        return str[3] << 24 | str[2] << 16 | str[1] << 8 | str[0];
    }
    
    String fourCCtoString(const FOURCC& fourCC);
    
#define DEFINE_FOURCC(name) constexpr FOURCC name = #name ## _fourcc
    DEFINE_FOURCC(RIFF);
    DEFINE_FOURCC(LIST);
    DEFINE_FOURCC(sfbk);
    DEFINE_FOURCC(INFO);
    DEFINE_FOURCC(ifil);
    DEFINE_FOURCC(INAM);
    DEFINE_FOURCC(isng);
    DEFINE_FOURCC(IPRD);
    DEFINE_FOURCC(IENG);
    DEFINE_FOURCC(ISFT);
    DEFINE_FOURCC(ICRD);
    DEFINE_FOURCC(ICMT);
    DEFINE_FOURCC(ICOP);
    DEFINE_FOURCC(sdta);
    DEFINE_FOURCC(smpl);
    DEFINE_FOURCC(sm24);
    DEFINE_FOURCC(phdr);
    DEFINE_FOURCC(pbag);
    DEFINE_FOURCC(pmod);
    DEFINE_FOURCC(pgen);
    DEFINE_FOURCC(inst);
    DEFINE_FOURCC(ibag);
    DEFINE_FOURCC(imod);
    DEFINE_FOURCC(igen);
    DEFINE_FOURCC(shdr);
    
};

struct VersionTag
{
    Types::WORD wMajor{0};
    Types::WORD wMinor{0};
    void printDescription() const;
};
/**
 The PHDR sub-chunk is a required sub-chunk listing all presets within the SoundFont compatible file.
 */
struct PresetHeader
{
    /** achPresetName[20] contains the name of the preset expressed in ASCII, with unused terminal characters filled with zero valued bytes. */
    String presetName;
    /** wPreset: contains the MIDI Preset Number */
    Types::WORD midiPresetNumber;
    /** wBank: contains the MIDI Bank Number */
    Types::WORD midiBankNumber;
    /** wPresetBagNdx:  an index to the preset’s zone list in the PBAG sub-chunk. */
    Types::WORD presetZoneStartingIndex;
    /** not used */
    Types::DWORD dwLibrary;
    /** not used */
    Types::DWORD dwGenre;
    /** not used */
    Types::DWORD dwMorphology;
    
    void printDescription() const;
    bool operator==(const PresetHeader& other) const;
};

/**
 The PBAG sub-chunk is a required sub-chunk listing all preset zones within the SoundFont compatible file.
 */
struct PresetZone
{
    /** wGenNdx: this is the index where this preset zone's list of generators begins in the PGEN array. */
    Types::WORD generatorListStartingIndex;
    /** wModNdx: an index to its list of modulators in the PMOD sub-chunk */
    Types::WORD modulatorListStartingIndex;
    
    void printDescription() const;
    bool operator==(const PresetZone& other) const;
};

enum ModulatorDestinations
{
    NoteOnVelocity_to_InitialAttenuation,
    NoteOnVelocity_to_FilterCutoff,
    ChannelPressure_to_VibratoLFOPitchDepth,
    CC1_to_VibratoLFOPitchDepth,
    CC7_to_InitialAttenuation,
    CC10_to_Pan,
    CC11_to_InitialAttenuation,
    CC91_to_ReverbSend,
    CC93_to_ChorusSend,
    PitchWheel_to_InitialPitch,
    MODULATOR_LIST_END,
};

struct Modulator
{
    Modulator(Types::WORD val);
    int getIndex() const;
    bool getCCTypeFlag() const;
    bool getDirectionFlag() const;
    bool getPolarityFlag() const;
    int getContinuityType() const;
    /**
     returns true if this modulator should be ignored because it contains an invalid value
     */
    bool shouldIgnore() const;
    Types::WORD getVal() const;
    bool operator==(const Modulator& other) const { return other.bi == bi; }
private:
    BigInteger bi;
};

enum GeneratorType
{
    startAddressOffset,
    endAddressOffset,
    startLoopAddressOffset,
    endLoopAddressOffset,
    startAddressCoarseOffset,
    modLfoToPitch,
    vibLfoToPitch,
    modEnvToPitch,
    initialFilterFC,
    initialFilterQ,
    modLfoToFilterFc,
    modEnvToFilterFc,
    endAddrsCoarseOffset,
    modLfoToVolume,
    unused1,
    chorusEffectsSend,
    reverbEffectsSend,
    pan,
    unused2,
    unused3,
    unused4,
    delayModLFO,
    freqModLFO,
    delayVibLFO,
    freqVibLFO,
    delayModEnv,
    attackModEnv,
    holdModEnv,
    decayModEnv,
    sustainModEnv,
    releaseModEnv,
    keynumToModEnvHold,
    keynumToModEnvDecay,
    delayVolEnv,
    attackVolEnv,
    holdVolEnv,
    decayVolEnv,
    sustainVolEnv,
    releaseVolEnv,
    keynumToVolEnvHold,
    keynumToVolEnvDecay,
    instrument,
    reserved1,
    keyRange,
    velRange,
    startloopAddrsCoarseOffset,
    keynum,
    velocity,
    initialAttenuation,
    reserved2,
    endloopAddrsCoarseOffset,
    coarseTune,
    fineTune,
    sampleID,
    sampleModes,
    reserved3,
    scaleTuning,
    exclusiveClass,
    overridingRootKey,
    unused5,
    endOper,
    
    /* the initial pitch is not a "standard" generator. It is not
     * mentioned in the list of generator in the SF2 specifications. It
     * is used, however, mentioned as the destination for the default pitch wheel
     * modulator. */
    initialPitch,
    GENERATOR_LIST_END
};

extern const std::map<GeneratorType, String> GeneratorNames;

struct GeneratorOperator
{
    GeneratorOperator(Types::WORD d) : data(d) { type = static_cast<GeneratorType>(d); }
    Types::WORD data;
    GeneratorType type;
    bool operator==(const GeneratorOperator& other) const { return data == other.data; }
};

enum TransformType
{
    Linear,
    unused,
    AbsoluteValue,
    TRANSFORM_TYPES_LIST_END,
};

struct SFTransform
{
    SFTransform(Types::WORD d) : data(d) { type = static_cast<TransformType>(data); }
    Types::WORD data;
    TransformType type;
    bool operator==(const SFTransform& other) const { return data == other.data; }
};
/*
 The PMOD sub-chunk is a required sub-chunk listing all preset zone modulators within the SoundFont compatible file.
 */
struct ZoneModulator
{
    ZoneModulator(Types::WORD src, Types::WORD dest, Types::SHORT amt, Types::WORD amtSrc, Types::WORD trans);
    
    void printDescription() const;
    /**  a value of one of the SFModulator enumeration type values */
    Modulator modSrcOper;
    /**
     indicates the destination of the modulator.
     The destination is either a value of one of the SFGenerator enumeration type values or a link to the sfModSrcOper of another modulator block. The latter is indicated by the top bit of the sfModDestOper field being set, the other 15 bits designates the index value of the modulator whose source should be the output of the current modulator RELATIVE TO the first modulator in the instrument zone. Unknown or undefined values are ignored. Modulators with links that point to a modulator index that exceeds the total number of modulators for a given zone are ignored. Linked modulators that are part of circular links are ignored.
     */
    GeneratorOperator modDestOper;
    /** a signed value indicating the degree to which the source modulates the destination. A zero value indicates there is no fixed amount. */
    Types::SHORT modAmount;
    /**
     a value of one of the SFModulator enumeration type values.
     Unknown or undefined values are ignored. Modulators with sfModAmtSrcOper set to ‘link’ are ignored. This value indicates the degree to which the source modulates the destination is to be controlled by the specified modulation source. Note that this enumeration is two bytes in length.
     */
    Modulator modAmtSrcOper;
    /**
     a value of one of the SFTransform enumeration type values.
     Unknown or undefined values are ignored. This value indicates that a transform of the specified type will be applied to the modulation source before application to the modulator. Note that this enumeration is two bytes in length.
     */
    SFTransform modTransOper;
    
    bool isIdenticalTo(const ZoneModulator& other) const
    {
        return
            other.modSrcOper == modSrcOper &&
            other.modDestOper == modDestOper &&
            other.modAmtSrcOper == modAmtSrcOper &&
            other.modTransOper == modTransOper;
    }
};

struct RangesType
{
    RangesType() {};
    RangesType(Types::BYTE lo, Types::BYTE hi) : byLo(lo), byHi(hi) {}
    Types::BYTE byLo = 0;
    Types::BYTE byHi = 0;
    bool operator==(const RangesType& other) const
    {
        return byLo == other.byLo && byHi == other.byHi;
    }
};

union GeneratorAmountType
{
    GeneratorAmountType() { shortAmount = 0; }
    GeneratorAmountType(float f) {shortAmount = static_cast<Types::SHORT>(f); }
    GeneratorAmountType(Types::BYTE lo, Types::BYTE hi) { range.byLo = lo; range.byHi = hi; }
    ///a pair of unsigned 8-bit values
    RangesType range;
    ///the signed 16bit value
    Types::SHORT shortAmount;
    ///the unsigned 16-bit value
    Types::WORD wordAmount;
};
/**
 The PGEN chunk is a required chunk containing a list of preset zone generators for each preset zone within the SoundFont
 compatible file.
 */
struct Generator
{
    /**  a value of one of the SFGenerator enumeration type values. */
    GeneratorOperator genOper;
    /**  the value to be assigned to the specified generator. */
    GeneratorAmountType genAmount;
    
    Generator(Types::WORD genOp, GeneratorAmountType genAmt);
    
    void printDescription() const;
};

struct GeneratorInfo
{
    GeneratorType type;
    bool requiresInitialization;
    char nrpn_scale;
    float min;
    float max;
    GeneratorAmountType defaultValue;
};

/**
 The inst sub-chunk is a required sub-chunk listing all instruments within the SoundFont compatible file.
 */
struct Instrument
{
    String name;
    /** wInstBagNdx: an index to the instrument’s zone list in the IBAG sub-chunk. */
    Types::WORD instrumentZoneListStartingIndex;
    
    void printDescription() const;
    bool operator==(const Instrument& other) const;
};

/**
 The IBAG sub-chunk is a required sub-chunk listing all instrument zones within the SoundFont compatible file.
 */
struct InstrumentZone
{
    /** wInstGenNdx: an index to the instrument zone’s list of generators in the IGEN sub-chunk */
    Types::WORD generatorListStartingIndex;
    /** wInstModNdx: an index to its list of modulators in the IMOD sub-chunk */
    Types::WORD modulatorListStartingIndex;
    
    void printDescription() const;
    bool operator==(const InstrumentZone& other) const;
};

/**
 The SHDR chunk is a required sub-chunk listing all samples within the smpl sub-chunk and any referenced ROM samples.
 */
struct SampleHeader
{
    enum SFSampleLink
    {
        monoSample = 1,
        rightSample = 2,
        leftSample = 4,
        linkedSample = 8,
        RomMonoSample = 32769,
        RomRightSample = 32770,
        RomLeftSample = 32772,
        RomLinkedSample = 32776
    };
    
    /** the 20-character name of the sample */
    String name;
    /** the index, in sample data points, from the beginning of the sample data field to the first data point of this sample */
    Types::DWORD startingSampleIndex;
    
    /** contains the index, in sample data points, from the beginning of the sample data field to the first of the set of 46 zero valued data points following this sample */
    Types::DWORD endingSampleIndex;
    
    /** contains the index, in sample data points, from the beginning of the sample data field to the first data point in the loop of this sample. */
    Types::DWORD loopStartSampleIndex;
    
    /**  dwEndloop contains the index, in sample data points, from the beginning of the sample data field to the first data point following the loop of this sample */
    Types::DWORD loopEndSampleIndex;
    
    /** contains the sample rate, in hertz, at which this sample was acquired or to which it was most recently converted. */
    Types::DWORD sampleRate;
    
    /** contains the MIDI key number of the recorded pitch of the sample. */
    Types::BYTE originalMIDIPitch;
    
    /** contains a pitch correction in cents that should be applied to the sample on playback. The purpose of this field is to compensate for any pitch errors during the sample recording process. */
    Types::CHAR playbackPitchCorrectionCents;
    
    /** wSampleLink is the sample header index of the associated right or left stereo sample respectively. */
    Types::WORD wSampleLink;
    
    /**
     the Type of the sample.  if sampleType is MONO, then wSampleLink is undefined and wSampleLink's value should be ignored.
     if sampleType is left or right: wSampleLink holds the index of the complementary sample.
     */
    SFSampleLink sampleType;
    
    bool operator==(const SampleHeader& other) const;
    void printDescription() const;
};
}   //end namespace SF2
}   //end namespace SoundFont

#undef BEGIN_SF2_NAMESPACE
#undef BEGIN_SOUNDFONT_NAMESPACE

