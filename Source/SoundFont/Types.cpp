/*
  ==============================================================================

    Types.cpp
    Created: 25 Nov 2018 1:58:43am
    Author:  Charles Schiermeyer

  ==============================================================================
*/

#include "Types.h"

#define BEGIN_SOUNDFONT_NAMESPACE namespace SoundFont {
#define BEGIN_SF2_NAMESPACE namespace SF2 {

BEGIN_SOUNDFONT_NAMESPACE
BEGIN_SF2_NAMESPACE
String Types::fourCCtoString(const Types::FOURCC& fourCC)
{
    return { reinterpret_cast<const char*>(&fourCC), sizeof(FOURCC) };
}
//==============================================================================
void VersionTag::printDescription() const
{
    String str;
    str << "Major: " << wMajor << " ";
    str << "minor: " << wMinor;
    DBG( "VersionTag: " << str );
}
//==============================================================================
void PresetHeader::printDescription() const
{
    String str;
    
    str << "presetName: " << presetName << " ";
    str << "midiPresetNumber: " << midiPresetNumber << " ";
    str << "midiBankNumber: " << midiBankNumber << " ";
    str << "presetZoneStartingIndex: " << presetZoneStartingIndex << " ";
    str << "dwLibrary: " << (int)dwLibrary << " ";
    str << "dwGenre: " << (int)dwGenre << " ";
    str << "dwMorphology: " << (int)dwMorphology << " ";
    
    DBG("PresetHeader: " << str );
}

bool PresetHeader::operator==(const PresetHeader& other) const
{
    return
    presetName == other.presetName &&
    midiPresetNumber == other.midiPresetNumber &&
    midiBankNumber == other.midiBankNumber &&
    presetZoneStartingIndex == other.presetZoneStartingIndex &&
    dwLibrary == other.dwLibrary &&
    dwGenre == other.dwGenre &&
    dwMorphology == other.dwMorphology;
}
//==============================================================================
void PresetZone::printDescription() const
{
    String str;
    str << "genList starting index: " << generatorListStartingIndex << " ";
    str << "modList starting index: " << modulatorListStartingIndex << " ";
    DBG( "PresetZone (PGAB): " << str );
}

bool PresetZone::operator==(const PresetZone &other) const
{
    return generatorListStartingIndex == other.generatorListStartingIndex &&
    modulatorListStartingIndex == other.modulatorListStartingIndex;
}
//==============================================================================
Modulator::Modulator(Types::WORD val) : bi(val)
{}

int Modulator::getIndex() const
{
    return bi.getBitRangeAsInt(0, 7);
}

bool Modulator::getCCTypeFlag() const
{
    return bi[7];
}

bool Modulator::getDirectionFlag() const
{
    return bi[8];
}

bool Modulator::getPolarityFlag() const
{
    return bi[9];
}

int Modulator::getContinuityType() const
{
    return bi.getBitRangeAsInt(10, 6);
}

bool Modulator::shouldIgnore() const
{
    int index = getIndex();
    if( getCCTypeFlag() == 0 )
    {
        //0,2,3,10,13,14,16,127
        switch(index)
        {
            case 0:
            case 2:
            case 3:
            case 10:
            case 13:
            case 14:
            case 16:
            case 127:
            {
                return false; //you should not ignore this modulator
                break;
            }
            default:
            {
                return true;
            }
        }
    }
    else
    {
        /*
         If the ‘C’ bit is set to ‘1’, the MIDI Controller Palette is selected. The ‘index’ field value corresponds to one of the 128
         MIDI Continuous Controller messages as defined in the MIDI specification.
         
         Per the spec:
         index values 0, 6, 32, 38, 98 through 101, and 120 through 127 are ILLEGAL
         index values 33 through 63 are also ILLEGAL (reserved)
         */
        static const std::vector<int> allowedCCs {
            1,2,3,4,5,//6
            7,8,9,10,11,
            12,13,14,15,16,
            17,18,19,20,21,
            22,23,24,25,26,
            27,28,29,30,31,
            //32,33-63
            64,65,66,67,68,
            69,70,71,72,73,
            74,75,76,77,78,
            79,80,81,82,83,
            84,85,86,87,88,
            89,90,91,92,93,
            94,95,96,97, //98 - 101
            102,103,104,105,106,
            107,108,109,110,111,
            112,113,114,115,116,
            117,118,119
            //12-127
        };
        bool allowed = false;
        for( auto cc : allowedCCs )
        {
            if( index == cc )
            {
                allowed = true;
                break;
            }
        }
        return !allowed;
    }
    return true;
}

Types::WORD Modulator::getVal() const
{
    return bi.getBitRangeAsInt(0, 16);
}
//==============================================================================
#define MAP_GENERATOR(x) { GeneratorType::x, #x }

const std::map<GeneratorType, String> GeneratorNames
{
    MAP_GENERATOR(startAddressOffset),
    MAP_GENERATOR(endAddressOffset),
    MAP_GENERATOR(startLoopAddressOffset),
    MAP_GENERATOR(endLoopAddressOffset),
    MAP_GENERATOR(startAddressCoarseOffset),
    MAP_GENERATOR(modLfoToPitch),
    MAP_GENERATOR(vibLfoToPitch),
    MAP_GENERATOR(modEnvToPitch),
    MAP_GENERATOR(initialFilterFC),
    MAP_GENERATOR(initialFilterQ),
    MAP_GENERATOR(modLfoToFilterFc),
    MAP_GENERATOR(modEnvToFilterFc),
    MAP_GENERATOR(endAddrsCoarseOffset),
    MAP_GENERATOR(modLfoToVolume),
    MAP_GENERATOR(unused1),
    MAP_GENERATOR(chorusEffectsSend),
    MAP_GENERATOR(reverbEffectsSend),
    MAP_GENERATOR(pan),
    MAP_GENERATOR(unused2),
    MAP_GENERATOR(unused3),
    MAP_GENERATOR(unused4),
    MAP_GENERATOR(delayModLFO),
    MAP_GENERATOR(freqModLFO),
    MAP_GENERATOR(delayVibLFO),
    MAP_GENERATOR(freqVibLFO),
    MAP_GENERATOR(delayModEnv),
    MAP_GENERATOR(attackModEnv),
    MAP_GENERATOR(holdModEnv),
    MAP_GENERATOR(decayModEnv),
    MAP_GENERATOR(sustainModEnv),
    MAP_GENERATOR(releaseModEnv),
    MAP_GENERATOR(keynumToModEnvHold),
    MAP_GENERATOR(keynumToModEnvDecay),
    MAP_GENERATOR(delayVolEnv),
    MAP_GENERATOR(attackVolEnv),
    MAP_GENERATOR(holdVolEnv),
    MAP_GENERATOR(decayVolEnv),
    MAP_GENERATOR(sustainVolEnv),
    MAP_GENERATOR(releaseVolEnv),
    MAP_GENERATOR(keynumToVolEnvHold),
    MAP_GENERATOR(keynumToVolEnvDecay),
    MAP_GENERATOR(instrument),
    MAP_GENERATOR(reserved1),
    MAP_GENERATOR(keyRange),
    MAP_GENERATOR(velRange),
    MAP_GENERATOR(startloopAddrsCoarseOffset),
    MAP_GENERATOR(keynum),
    MAP_GENERATOR(velocity),
    MAP_GENERATOR(initialAttenuation),
    MAP_GENERATOR(reserved2),
    MAP_GENERATOR(endloopAddrsCoarseOffset),
    MAP_GENERATOR(coarseTune),
    MAP_GENERATOR(fineTune),
    MAP_GENERATOR(sampleID),
    MAP_GENERATOR(sampleModes),
    MAP_GENERATOR(reserved3),
    MAP_GENERATOR(scaleTuning),
    MAP_GENERATOR(exclusiveClass),
    MAP_GENERATOR(overridingRootKey),
    MAP_GENERATOR(unused5),
    MAP_GENERATOR(endOper),
};
#undef MAP_GENERATOR

//==============================================================================
ZoneModulator::ZoneModulator(Types::WORD src,
                             Types::WORD dest,
                             Types::SHORT amt,
                             Types::WORD amtSrc,
                             Types::WORD trans) :
modSrcOper(src),
modDestOper(dest),
modAmount(amt),
modAmtSrcOper(amtSrc),
modTransOper(trans)
{
    
}

void ZoneModulator::printDescription() const
{
    String str;
    str << "modSrcOper: " << modSrcOper.getVal() << " ";
    str << "modDestOper: " << modDestOper.data << " ";
    str << "modAmount: " << modAmount << " ";
    str << "modAmtSrcOper: " << modAmtSrcOper.getVal() << " ";
    str << "modTransOper: " << modTransOper.data;
    
    DBG( "ModList: " << str );
}
//==============================================================================
Generator::Generator(Types::WORD genOp, GeneratorAmountType genAmt) :
genOper(genOp),
genAmount(genAmt)
{
    
}

void Generator::printDescription() const
{
    String str;
    //str << genOper.data << " ";
    str << "name: " << GeneratorNames.at(genOper.type) << " ";
    str << "value: ";
    switch(genOper.type)
    {
        case keyRange:
        case velRange:
        {
            str << genAmount.range.byLo << " " << genAmount.range.byHi << " ";
            break;
        }
        case startAddressOffset:
        case endAddressOffset:
        case startLoopAddressOffset:
        case endLoopAddressOffset:
        case startAddressCoarseOffset:
        case initialFilterFC:
        case initialFilterQ:
        case endAddrsCoarseOffset:
        case chorusEffectsSend:
        case reverbEffectsSend:
        case sustainModEnv:
        case sustainVolEnv:
        case startloopAddrsCoarseOffset:
        case keynum:
        case velocity:
        case initialAttenuation:
        case endloopAddrsCoarseOffset:
        case scaleTuning:
        case exclusiveClass:
        case overridingRootKey:
        {
            str << genAmount.wordAmount << " "; //unsigned generator types
            break;
        }
        default:
        {
            str << genAmount.shortAmount << " "; //signed generator types
            break;
        }
    }
    
    DBG( "Generator: " << str );
}
//==============================================================================
void Instrument::printDescription() const
{
    String str;
    str << "name: " << name << " ";
    str << "starting index: " << instrumentZoneListStartingIndex << " ";
    
    DBG( "Instrument: " << str );
}

bool Instrument::operator==(const Instrument& other) const
{
    return name == other.name &&
    instrumentZoneListStartingIndex == other.instrumentZoneListStartingIndex;
}
//==============================================================================
void InstrumentZone::printDescription() const
{
    String str;
    str << "genList starting index: " << generatorListStartingIndex << " ";
    str << "modList starting index: " << modulatorListStartingIndex << " ";
    
    DBG( "InstrumentZone (IBAG): " << str );
}

bool InstrumentZone::operator==(const InstrumentZone &other) const
{
    return  generatorListStartingIndex == other.generatorListStartingIndex &&
            modulatorListStartingIndex == other.modulatorListStartingIndex;
}
//==============================================================================
bool SampleHeader::operator==(const SampleHeader& other) const
{
    return name == other.name &&
    startingSampleIndex == other.startingSampleIndex &&
    endingSampleIndex == other.endingSampleIndex &&
    loopStartSampleIndex == other.loopStartSampleIndex &&
    loopEndSampleIndex == other.loopEndSampleIndex &&
    sampleRate == other.sampleRate &&
    originalMIDIPitch == other.originalMIDIPitch &&
    playbackPitchCorrectionCents == other.playbackPitchCorrectionCents &&
    wSampleLink == other.wSampleLink &&
    sampleType == other.sampleType;
}

void SampleHeader::printDescription() const
{
    String str;
    
    str << "name: " <<  name << " ";
    str << "startingSampleIndex: " << (int)startingSampleIndex << " ";
    str << "endingSampleIndex: " << (int)endingSampleIndex << " ";
    str << "loopStartSampleIndex: " << (int)loopStartSampleIndex << " ";
    str << "loopEndSampleIndex: " << (int)loopEndSampleIndex << " ";
    str << "sampleRate: " << (int)sampleRate << " ";
    str << "originalMIDIPitch: " << originalMIDIPitch << " ";
    str << "playbackPitchCorrectionCents: " << playbackPitchCorrectionCents << " ";
    str << "wSampleLink: " << wSampleLink << " ";
    str << "sampleType: " << sampleType << " ";
    
    DBG("SampleHeader: " << str );
}

}//end namespace SF2
}//end namespace SoundFont

#undef BEGIN_SF2_NAMESPACE
#undef BEGIN_SOUNDFONT_NAMESPACE
