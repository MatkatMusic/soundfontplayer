/*
 ==============================================================================
 
 Parser.cpp
 Created: 25 Nov 2018 2:08:00am
 Author:  Charles Schiermeyer
 
 ==============================================================================
 */

#include "Parser.h"

#define BEGIN_SOUNDFONT_NAMESPACE namespace SoundFont {
#define BEGIN_SF2_NAMESPACE namespace SF2 {

BEGIN_SOUNDFONT_NAMESPACE
BEGIN_SF2_NAMESPACE

void SF2Sample::printDescription()
{
    jassert(header != nullptr);
    header->printDescription();
}

void SF2InstrumentZone::printDescription()
{
    DBG( "---SF2InstrumentZone---");
    
    DBG( "  Generators: " << generators.size() );
    for( auto* gen : generators ) gen->printDescription();
    
    DBG( "  Modulators: " << modulators.size() );
    for( auto* mod : modulators ) mod->printDescription();
    
    if( sample != nullptr )
    {
        DBG( "  Attached Sample" );
        sample->printDescription();
    }
}

void SF2Instrument::printDescription()
{
    jassert( inst != nullptr );
    
    DBG( "---SF2Instrument---" );
    inst->printDescription();
    
    DBG( "  Instrument Globals" );
    globalZone.printDescription();
    
    DBG( "  zones: " << zones.size() );
    for( auto& zone : zones ) zone.printDescription();
}

void SF2PresetZone::printDescription()
{
    DBG( "---SF2PresetZone---" );
    
    DBG( "  Generators: " << generators.size() );
    for( auto* gen : generators ) gen->printDescription();
    
    DBG( "  Modulators: " << modulators.size() );
    for( auto* mod : modulators ) mod->printDescription();
    
    if( instrument != nullptr )
    {
        DBG( "  Linked Instrument" );
        instrument->printDescription();
    }
}

void SF2Preset::printDescription()
{
    jassert(header != nullptr);
    
    DBG("SF2 Preset Description" );
    header->printDescription();
    
    DBG( "**** preset globals ****" );
    globalZone.printDescription();
    
    DBG( "**** Preset zones: " << zones.size() << " ****" );
    for( auto& zone : zones ) zone.printDescription();
    
}
//==============================================================================
JUCE_IMPLEMENT_SINGLETON(Parser)

Parser::~Parser()
{
    masterReference.clear();
    clearSingletonInstance();
}

File* Parser::parse(const juce::File &sf2path)
{
    for( auto& parsed : parsedFiles )
    {
        if( parsed->file == sf2path )
        {
            return parsed.get();
        }
    }
    
    FileInputStream fis( sf2path );
    if( fis.openedOk() )
        return parseSF2( fis );
    
    return nullptr;
}

File* Parser::parseSF2(FileInputStream &fis)
{
    int64 pos = fis.getPosition();
    std::unique_ptr<File> sf2 = std::make_unique<File>(fis.getFile());
    
    while( pos < fis.getTotalLength() )
    {
        Chunk chunk(fis.getFile(), pos);
        pos = parseChunk(chunk, *sf2);
    }
    
    parsedFiles.push_back( std::move( sf2 ) );
    
    return parse(fis.getFile());
}

Generator* Parser::findGenerator(const Array<Generator *> &generators, GeneratorType selector)
{
    for( auto* gen : generators )
    {
        if( gen->genOper.type == selector )
        {
            return gen;
        }
    }
    return nullptr;
}

Generator* Parser::findGenerator( const std::shared_ptr<SF2Instrument>& instrument, GeneratorType selector )
{
    if( instrument.get() != nullptr )
    {
        if( auto* gen = findGenerator(instrument->globalZone.generators, selector) )
        {
            return gen;
        }
        
        for( auto& zone : instrument->zones )
        {
            if( auto* gen = findGenerator(zone.generators, selector ) )
            {
                return gen;
            }
        }
    }
    return nullptr;
}

const std::array<GeneratorInfo, GeneratorType::GENERATOR_LIST_END> DefaultGeneratorValues =
{{
    /* number/name                               init  scale         min        max         default */
    { GeneratorType::startAddressOffset,            1,     1,       0.0f,     1e10f,       0.0f },
    { GeneratorType::endAddressOffset,              1,     1,     -1e10f,      0.0f,       0.0f },
    { GeneratorType::startLoopAddressOffset,        1,     1,     -1e10f,     1e10f,       0.0f },
    { GeneratorType::endLoopAddressOffset,          1,     1,     -1e10f,     1e10f,       0.0f },
    { GeneratorType::startAddressCoarseOffset,      0,     1,       0.0f,     1e10f,       0.0f },
    { GeneratorType::modLfoToPitch,                 1,     2,  -12000.0f,  12000.0f,       0.0f },
    { GeneratorType::vibLfoToPitch,                 1,     2,  -12000.0f,  12000.0f,       0.0f },
    { GeneratorType::modEnvToPitch,                 1,     2,  -12000.0f,  12000.0f,       0.0f },
    { GeneratorType::initialFilterFC,               1,     2,    1500.0f,  13500.0f,   13500.0f },
    { GeneratorType::initialFilterQ,                1,     1,       0.0f,    960.0f,       0.0f },
    { GeneratorType::modLfoToFilterFc,              1,     2,  -12000.0f,  12000.0f,       0.0f },
    { GeneratorType::modEnvToFilterFc,              1,     2,  -12000.0f,  12000.0f,       0.0f },
    { GeneratorType::endAddrsCoarseOffset,          0,     1,     -1e10f,      0.0f,       0.0f },
    { GeneratorType::modLfoToVolume,                1,     1,    -960.0f,    960.0f,       0.0f },
    { GeneratorType::unused1,                       0,     0,       0.0f,      0.0f,       0.0f },
    { GeneratorType::chorusEffectsSend,             1,     1,       0.0f,   1000.0f,       0.0f },
    { GeneratorType::reverbEffectsSend,             1,     1,       0.0f,   1000.0f,       0.0f },
    { GeneratorType::pan,                           1,     1,    -500.0f,    500.0f,       0.0f },
    { GeneratorType::unused2,                       0,     0,       0.0f,      0.0f,       0.0f },
    { GeneratorType::unused3,                       0,     0,       0.0f,      0.0f,       0.0f },
    { GeneratorType::unused4,                       0,     0,       0.0f,      0.0f,       0.0f },
    { GeneratorType::delayModLFO,                   1,     2,  -12000.0f,   5000.0f,  -12000.0f },
    { GeneratorType::freqModLFO,                    1,     4,  -16000.0f,   4500.0f,       0.0f },
    { GeneratorType::delayVibLFO,                   1,     2,  -12000.0f,   5000.0f,  -12000.0f },
    { GeneratorType::freqVibLFO,                    1,     4,  -16000.0f,   4500.0f,       0.0f },
    { GeneratorType::delayModEnv,                   1,     2,  -12000.0f,   5000.0f,  -12000.0f },
    { GeneratorType::attackModEnv,                  1,     2,  -12000.0f,   8000.0f,  -12000.0f },
    { GeneratorType::holdModEnv,                    1,     2,  -12000.0f,   5000.0f,  -12000.0f },
    { GeneratorType::decayModEnv,                   1,     2,  -12000.0f,   8000.0f,  -12000.0f },
    { GeneratorType::sustainModEnv,                 0,     1,       0.0f,   1000.0f,       0.0f },
    { GeneratorType::releaseModEnv,                 1,     2,  -12000.0f,   8000.0f,  -12000.0f },
    { GeneratorType::keynumToModEnvHold,            0,     1,   -1200.0f,   1200.0f,       0.0f },
    { GeneratorType::keynumToModEnvDecay,           0,     1,   -1200.0f,   1200.0f,       0.0f },
    { GeneratorType::delayVolEnv,                   1,     2,  -12000.0f,   5000.0f,  -12000.0f },
    { GeneratorType::attackVolEnv,                  1,     2,  -12000.0f,   8000.0f,  -12000.0f },
    { GeneratorType::holdVolEnv,                    1,     2,  -12000.0f,   5000.0f,  -12000.0f },
    { GeneratorType::decayVolEnv,                   1,     2,  -12000.0f,   8000.0f,  -12000.0f },
    { GeneratorType::sustainVolEnv,                 0,     1,       0.0f,   1440.0f,       0.0f },
    { GeneratorType::releaseVolEnv,                 1,     2,  -12000.0f,   8000.0f,  -12000.0f },
    { GeneratorType::keynumToVolEnvHold,            0,     1,   -1200.0f,   1200.0f,       0.0f },
    { GeneratorType::keynumToVolEnvDecay,           0,     1,   -1200.0f,   1200.0f,       0.0f },
    { GeneratorType::instrument,                    0,     0,       0.0f,      0.0f,       0.0f },
    { GeneratorType::reserved1,                     0,     0,       0.0f,      0.0f,       0.0f },
    { GeneratorType::keyRange,                      0,     0,       0.0f,    127.0f,       {0,127} },
    { GeneratorType::velRange,                      0,     0,       0.0f,    127.0f,       {0,127} },
    { GeneratorType::startloopAddrsCoarseOffset,    0,     1,     -1e10f,     1e10f,       0.0f },
    { GeneratorType::keynum,                        1,     0,       0.0f,    127.0f,      -1.0f },
    { GeneratorType::velocity,                      1,     1,       0.0f,    127.0f,      -1.0f },
    { GeneratorType::initialAttenuation,            1,     1,       0.0f,   1440.0f,       0.0f },
    { GeneratorType::reserved2,                     0,     0,       0.0f,      0.0f,       0.0f },
    { GeneratorType::endloopAddrsCoarseOffset,      0,     1,     -1e10f,     1e10f,       0.0f },
    { GeneratorType::coarseTune,                    0,     1,    -120.0f,    120.0f,       0.0f },
    { GeneratorType::fineTune,                      0,     1,     -99.0f,     99.0f,       0.0f },
    { GeneratorType::sampleID,                      0,     0,       0.0f,      0.0f,       0.0f },
    { GeneratorType::sampleModes,                   0,     0,       0.0f,      0.0f,       0.0f },
    { GeneratorType::reserved3,                     0,     0,       0.0f,      0.0f,       0.0f },
    { GeneratorType::scaleTuning,                   0,     1,       0.0f,   1200.0f,     100.0f },
    { GeneratorType::exclusiveClass,                0,     0,       0.0f,      0.0f,       0.0f },
    { GeneratorType::overridingRootKey,             1,     0,       0.0f,    127.0f,      -1.0f },
    { GeneratorType::initialPitch,                  1,     0,       0.0f,    127.0f,       0.0f }
}};

const std::array<ZoneModulator, ModulatorDestinations::MODULATOR_LIST_END> DefaultModulatorValues =
{{
    ZoneModulator(0x0502, initialAttenuation, 960, 0x0, 0),
    ZoneModulator(0x0102, initialFilterFC, -2400, 0x0, 0),
    ZoneModulator(0x000D, vibLfoToPitch, 50, 0x0, 0),
    ZoneModulator(0x0081, vibLfoToPitch, 50, 0x0, 0),
    ZoneModulator(0x0582, initialAttenuation, 960, 0x0, 0),
    ZoneModulator(0x028A, initialAttenuation, 1000, 0x0, 0),
    ZoneModulator(0x058B, initialAttenuation, 960, 0x0, 0),
    ZoneModulator(0x00DB, reverbEffectsSend, 200, 0x0, 0),
    ZoneModulator(0x00DD, chorusEffectsSend, 200, 0x0, 0),
    ZoneModulator(0x020E, initialPitch, 12700, 0x0010, 0),
}};

const GeneratorInfo* Parser::getDefaultGenerator(GeneratorType selector)
{
    auto& info = DefaultGeneratorValues.at(selector);
    return &info;
}

Generator Parser::getGenerator(const Array<Generator *> &generators, GeneratorType selector)
{
    if( auto* foundGen = findGenerator(generators, selector) )
    {
        return *foundGen;
    }
    
    auto* defaultGen = getDefaultGenerator(selector);
    
    return Generator(defaultGen->type, defaultGen->defaultValue);
}

std::shared_ptr<SF2Preset> Parser::buildCompletePreset(int presetNum, const SF2::File& file)
{
    auto preset = std::make_shared<SF2Preset>();
    
    jassert( presetNum < file.presetHeadersList.size() );
    
    auto& presetHeader = file.presetHeadersList.getReference(presetNum);
    preset->header = &presetHeader;
    auto& nextPresetHeader = file.presetHeadersList.getReference(presetNum+1);
    auto& presetZoneStart = presetHeader.presetZoneStartingIndex;
    auto& presetZoneEnd = nextPresetHeader.presetZoneStartingIndex;
    
    //gather all of the generators and modulators for each zone for this preset
    for(auto presetZoneNum = presetZoneStart;
        presetZoneNum < presetZoneEnd;
        ++presetZoneNum )
    {
        SF2PresetZone presetZone;
        DBG( "processing preset zone..." );
        auto& zone = file.presetZonesList.getReference(presetZoneNum);
        auto& nextPresetZone = file.presetZonesList.getReference(presetZoneNum+1);
        
        GetGenerators(zone.generatorListStartingIndex,
                      nextPresetZone.generatorListStartingIndex,
                      file.presetGeneratorsList,
                      presetZone.generators);
        
        GetModulators(zone.modulatorListStartingIndex,
                      nextPresetZone.modulatorListStartingIndex,
                      file.presetModulatorsList,
                      presetZone.modulators);
        
        preset->zones.add( std::move(presetZone) );
    }
    
    //now process zones that were added to the preset,
    //working backwards in case you need to remove an invalid zone from the zone list
    for( auto zoneNum = preset->zones.size(); --zoneNum >= 0; )
    {
        auto& presetZone = preset->zones.getReference(zoneNum);
        bool hasInstrument = false;
        for( auto* generator : presetZone.generators )
        {
            if( generator->genOper.type == GeneratorType::instrument )
            {
                /*
                 build a SF2Instrument
                 add it to the resourcePool.loadedInstruments
                 populate the instrument member of the SF2PresetZone
                 */
                
                presetZone.instrument = getOrMakeInstrument(generator->genAmount.wordAmount, file);
                hasInstrument = true;
                break;
            }
        }
        if( !hasInstrument )
        {
            if( zoneNum == 0 )
            {
                preset->globalZone = presetZone;
            }
            /*
             If a zone other than the first zone lacks an Instrument generator as its last generator, that zone should be ignored.
             */
            preset->zones.removeFirstMatchingValue( presetZone );
        }
        
        if( presetZone.generators.isEmpty() && presetZone.modulators.isEmpty() )
        {
            /*
             A global zone with no modulators and no generators should also be ignored.
             */
            preset->zones.removeFirstMatchingValue( presetZone );
        }
    }
    //preset->printDescription();
    resourcePool.loadedPresets.add(preset);
    return preset;
}

std::shared_ptr<SF2Instrument> Parser::makeInstrument(int instrumentIDX, const SF2::File &file)
{
    auto instrument = std::make_shared<SF2Instrument>();
    SF2::Instrument& inst = file.instrumentsList.getReference(instrumentIDX);
    instrument->inst = &inst;
    DBG( "making instrument " << inst.name );
    SF2::Instrument& nextInst = file.instrumentsList.getReference(instrumentIDX + 1);
    //get the generators and modulators for each zone of this instrument
    for( auto zoneNum = inst.instrumentZoneListStartingIndex;
        zoneNum < nextInst.instrumentZoneListStartingIndex;
        ++zoneNum )
    {
        SF2InstrumentZone zone;
        auto& instZone = file.instrumentZonesList.getReference(zoneNum);
        auto& nextZone = file.instrumentZonesList.getReference(zoneNum+1);
        
        GetGenerators(instZone.generatorListStartingIndex,
                      nextZone.generatorListStartingIndex,
                      file.instrumentGeneratorsList,
                      zone.generators);
        
        GetModulators(instZone.modulatorListStartingIndex,
                      nextZone.modulatorListStartingIndex,
                      file.instrumentModulatorsList,
                      zone.modulators);
        
        if( zone.generators.isEmpty() && zone.modulators.isEmpty() )
        {
            //invalid instrument zone
            continue;
        }
        
        instrument->zones.add( std::move(zone) );
    }
    
    //find sampleID generators for this preset
    Array<SF2::SampleHeader*> usedSamples;
    for(auto zoneNum = instrument->zones.size();
        --zoneNum >= 0; )
    {
        auto& instrumentZone = instrument->zones.getReference(zoneNum);
        for( auto* generator : instrumentZone.generators )
        {
            if( generator->genOper.type == GeneratorType::sampleID )
            {
                SF2::SampleHeader* header = &file.sampleHeadersList.getReference( generator->genAmount.wordAmount );
                usedSamples.addIfNotAlreadyThere(header);
            }
        }
    }
    //extract all of the used samples for this preset from the soundfont file and put them into the SampleBufferPool
    
    for( auto* sample : usedSamples )
    {
        getSampleBuffer( *sample, file );
    }
    
    //now attach the extracted samples to the appropriate zone
    for(auto zoneNum = instrument->zones.size();
        --zoneNum >= 0; )
    {
        auto& instrumentZone = instrument->zones.getReference(zoneNum);
        for( auto* generator : instrumentZone.generators )
        {
            if( generator->genOper.type == GeneratorType::sampleID )
            {
                SF2::SampleHeader* header = &file.sampleHeadersList.getReference( generator->genAmount.wordAmount );
                instrumentZone.sample = getSampleBuffer(*header, file);
                break;
            }
        }
        if( instrumentZone.sample == nullptr)
        {
            if( zoneNum > 0 )
            {
                //If a zone other than the first zone lacks a sampleID generator as its last generator, that zone should be ignored.
                
                instrument->zones.remove( &instrumentZone );
                continue;
            }
            // else it's a global zone. no need for a sampleID
            else
            {
                instrument->globalZone = instrumentZone;
                instrument->zones.remove( &instrumentZone );
            }
            continue;
        }
        
        DBG( "\n\n getting sample:" << instrumentZone.sample->header->name );
    }//end find sampleID and load sample buffer
    
    return instrument;
}

std::shared_ptr<SF2Instrument> Parser::getOrMakeInstrument(int instrumentIDX, const SF2::File &file)
{
    auto& pool = resourcePool.loadedInstruments.getPool();
    auto& targetInstrument = file.instrumentsList.getReference(instrumentIDX);
    for( auto& inst : pool )
    {
        if( inst->inst == &targetInstrument )
        {
            DBG( "instrument already exists in pool" );
            return inst;
        }
    }
    
    auto result = makeInstrument(instrumentIDX, file);
    resourcePool.loadedInstruments.add(result);
    return result;
}

std::shared_ptr<SF2Sample> Parser::getSampleBuffer(const SF2::SampleHeader& sampleHeader, const SF2::File& file)
{
    const auto& pool = resourcePool.loadedSamples.getPool();
    for(const auto& shared_sample : pool )
    {
        if( *shared_sample->header == sampleHeader )
        {
            DBG( "sample found in pool...") ;
            return shared_sample;
        }
    }
    
    DBG("sample not found in pool.  extracting...");
    auto start = sampleHeader.startingSampleIndex;
    auto end = sampleHeader.endingSampleIndex;
    auto numSamples = end - start;
    AudioBuffer<float> buffer(1, numSamples + 4); //4 extra samples of silence
    
    HeapBlock<SoundFont::SF2::Types::SHORT> heapBlock(numSamples * sizeof( SoundFont::SF2::Types::SHORT ));
    
    jassert(file.sampleBufferBLocation == nullptr ); //we don't support 24bit samples yet in SF2 files
    
    auto& chunk = *file.sampleBufferALocation;
    
    FileInputStream fis( file.file );
    fis.setPosition(chunk.getStart() + start * sizeof( SoundFont::SF2::Types::SHORT ) );
    
    fis.read(heapBlock.getData(),
             numSamples * sizeof( SoundFont::SF2::Types::SHORT ));
    
    AudioDataConverters::convertInt16LEToFloat(heapBlock.getData(),
                                               buffer.getWritePointer(0),
                                               numSamples);
    //buffer now contains all of the audio samples in float format for sampleHeader
    //let's export it to an audio file and make sure..
    //#define EXPORT_BUFFER_TO_DISK 1
#if EXPORT_BUFFER_TO_DISK
    {
        juce::File audioFile (juce::File::getSpecialLocation(juce::File::SpecialLocationType::currentApplicationFile).getChildFile(sampleHeader.name).withFileExtension(".aiff"));
        
        audioFile.deleteFile();
        audioFile.create();
        if( audioFile.existsAsFile() )
        {
            ScopedPointer<FileOutputStream> fos(audioFile.createOutputStream());
            if( fos->openedOk() )
            {
                AiffAudioFormat aiff;
                
                ScopedPointer<AudioFormatWriter> writer(aiff.createWriterFor(fos.get(),
                                                                             (double)sampleHeader.sampleRate,
                                                                             1,
                                                                             16,
                                                                             {},
                                                                             0));
                if( writer != nullptr )
                {
                    fos.release();
                    if(!writer->writeFromAudioSampleBuffer(buffer,
                                                           0,
                                                           buffer.getNumSamples()) )
                    {
                        jassertfalse; //failed to write from audio buffer to disk.
                    }
                    else
                    {
                        audioFile.revealToUser();
                    }
                }
                else
                {
                    //failed to create writer
                    jassertfalse;
                }
            }
        }
        jassertfalse; //should be ok for you to look at the exported aiff in the DAW
    }
#endif
    auto instrumentSample = std::make_shared<SF2Sample>();
    instrumentSample->header = const_cast<SF2::SampleHeader*>(&sampleHeader);
    instrumentSample->buffer = std::move(buffer);
    
    resourcePool.loadedSamples.add( instrumentSample );
    return instrumentSample;
}

int64 Parser::parseChunk(Chunk &chunk, File& target)
{
    switch( chunk.getID() )
    {
        case Types::RIFF:
        case Types::LIST:
        {
            /*
             RIFF or LIST //consumed
             {size} //consumed
             */
            DBG( chunk.getDescription() );
            ScopedPointer<FileInputStream> fis = chunk.getFile().createInputStream();
            fis->setPosition( chunk.getStart() );
            Reader::readFourCC(*fis);
            Chunk c(chunk.getFile(), fis->getPosition() );
            return parseChunk(c, target);
        }
        case Types::sfbk:
        {
            jassertfalse;
            DBG( chunk.getDescription() );
            ScopedPointer<FileInputStream> fis = chunk.getFile().createInputStream();
            fis->setPosition( chunk.getStart() );
            
            Chunk c(chunk.getFile(), fis->getPosition() );
            return parseChunk(c, target);
            break;
        }
        case Types::INFO:
        {
            jassertfalse;
            break;
        }
        case Types::ifil:
        {
            //SoundFontTypes::VersionTag version;
            if( chunk.read(&target.version) )
            {
                return chunk.getEnd();
            }
            jassertfalse;
            return chunk.getStart() + 1;
        }
        case Types::INAM:
        {
            return parseString(target.soundFontName, chunk);
        }
        case Types::isng:
        {
            return parseString(target.soundEngine, chunk);
        }
        case Types::IPRD:
        {
            return parseString(target.intendedProduct, chunk);
        }
        case Types::IENG:
        {
            return parseString(target.soundFontEngineers, chunk);
        }
        case Types::ISFT:
        {
            return parseString(target.soundFontEditor, chunk);
        }
        case Types::ICRD:
        {
            return parseString(target.creationDate, chunk);
        }
        case Types::ICMT:
        {
            return parseString(target.comments, chunk);
        }
        case Types::ICOP:
        {
            return parseString(target.copyright, chunk);
        }
        case Types::sdta:
        {
            jassertfalse;
            break;
        }
        case Types::smpl:
        {
            const auto& f = chunk.getFile();
            Chunk c(f, chunk.getBegin());
            target.sampleBufferALocation = new Chunk(f, chunk.getBegin());
            //return parseSamples(sampleBufferA, *sampleBufferALocation.get());
            return target.sampleBufferALocation->getEnd();
        }
        case Types::sm24:
        {
            //return parseSamples(sampleBufferB, chunk);
            //don't parse it yet.
            //just mark the location
            const auto& f = chunk.getFile();
            Chunk c(f, chunk.getBegin());
            target.sampleBufferBLocation = new Chunk(f, chunk.getBegin());
            return target.sampleBufferBLocation->getEnd();
        }
        case Types::phdr:
        {
            auto parseImpl = [this, &target](MemoryInputStream& mis)
            {
                PresetHeader header;
                MemoryBlock mb(20, true);
                mis.read(mb.getData(), (int)mb.getSize());
                
                header.presetName = String(static_cast<const char*>(mb.getData()), mb.getSize());
                header.midiPresetNumber = Reader::readWord(mis);
                header.midiBankNumber = Reader::readWord(mis);
                header.presetZoneStartingIndex = Reader::readWord(mis);
                header.dwLibrary = Reader::readDword(mis);
                header.dwGenre = Reader::readDword(mis);
                header.dwMorphology = Reader::readDword(mis);
                
                target.presetHeadersList.add(header);
                //presetHeaders.getReference( presetHeaders.size() - 1).printDescription();
            };
            jassert( chunk.getSize() >= 38*2);
            jassert( chunk.getSize() % 38 == 0 );
            return parse(chunk, parseImpl, target.presetHeadersList);
        }
        case Types::pbag:
        {
            auto parseImpl = [this, &target](MemoryInputStream& mis)
            {
                PresetZone bag;
                bag.generatorListStartingIndex = Reader::readWord(mis);
                bag.modulatorListStartingIndex = Reader::readWord(mis);
                
                target.presetZonesList.add(bag);
                //presetBags.getReference( presetBags.size() - 1).printDescription();
            };
            jassert( chunk.getSize() >= 4);
            jassert( chunk.getSize() % 4 == 0 );
            return parse(chunk, parseImpl, target.presetZonesList);
        }
        case Types::pmod:
        {
            auto parseImpl = [this, &target](MemoryInputStream& mis)
            {
                Types::WORD src = Reader::readWord(mis);
                Types::WORD dest = Reader::readWord(mis);
                Types::SHORT amt = Reader::readShort(mis);
                Types::WORD amtSrc = Reader::readWord(mis);
                Types::WORD trans = Reader::readWord(mis);
                
                ZoneModulator mod(src, dest, amt, amtSrc, trans);
                target.presetModulatorsList.add(mod);
                //                    presetModLists.getReference( presetModLists.size() - 1).printDescription();
            };
            jassert( chunk.getSize() >= 10);
            jassert( chunk.getSize() % 10 == 0 );
            return parse(chunk, parseImpl, target.presetModulatorsList);
        }
        case Types::pgen:
        {
            auto parseImpl = [this, &target](MemoryInputStream& mis)
            {
                Types::WORD genOp = Reader::readWord(mis);
                GeneratorAmountType genAmt;
                genAmt.shortAmount = Reader::readShort(mis);
                
                Generator genList(genOp, genAmt);
                target.presetGeneratorsList.add(genList);
                //target.presetGeneratorsList.getReference( target.presetGeneratorsList.size() - 1).printDescription();
            };
            jassert( chunk.getSize() >= 4);
            jassert( chunk.getSize() % 4 == 0 );
            return parse(chunk, parseImpl, target.presetGeneratorsList);
        }
        case Types::inst:
        {
            auto parseImpl = [this, &target](MemoryInputStream& mis)
            {
                MemoryBlock mb(20, true);
                mis.read(mb.getData(), (int)mb.getSize());
                
                Instrument instrument;
                instrument.name = String(static_cast<const char*>(mb.getData()), mb.getSize());
                instrument.instrumentZoneListStartingIndex = Reader::readWord(mis);
                
                target.instrumentsList.add(instrument);
                //instruments.getReference(instruments.size() - 1).printDescription();
            };
            jassert( chunk.getSize() >= 22*2);
            jassert( chunk.getSize() % 22 == 0 );
            return parse(chunk, parseImpl, target.instrumentsList);
        }
        case Types::ibag:
        {
            auto parseImpl = [this, &target](MemoryInputStream& mis)
            {
                InstrumentZone ibag;
                ibag.generatorListStartingIndex = Reader::readWord(mis);
                ibag.modulatorListStartingIndex = Reader::readWord(mis);
                
                target.instrumentZonesList.add(ibag);
                //instrumentBags.getReference(instrumentBags.size() - 1).printDescription();
            };
            jassert( chunk.getSize() >= 4);
            jassert( chunk.getSize() % 4 == 0 );
            return parse(chunk, parseImpl, target.instrumentZonesList);
        }
        case Types::imod:
        {
            auto parseImpl = [this, &target](MemoryInputStream& mis)
            {
                Types::WORD src = Reader::readWord(mis);
                Types::WORD dest = Reader::readWord(mis);
                Types::SHORT amt = Reader::readShort(mis);
                Types::WORD amtSrc = Reader::readWord(mis);
                Types::WORD trans = Reader::readWord(mis);
                
                ZoneModulator mod(src, dest, amt, amtSrc, trans);
                target.instrumentModulatorsList.add(mod);
                //instrumentModLists.getReference( instrumentModLists.size() - 1).printDescription();
            };
            jassert( chunk.getSize() >= 10);
            jassert( chunk.getSize() % 10 == 0 );
            return parse(chunk, parseImpl, target.instrumentModulatorsList);
        }
        case Types::igen:
        {
            auto parseImpl = [this, &target](MemoryInputStream& mis)
            {
                Types::WORD genOp = Reader::readWord(mis);
                GeneratorAmountType genAmt;
                genAmt.shortAmount = Reader::readShort(mis);
                
                Generator genList(genOp, genAmt);
                target.instrumentGeneratorsList.add(genList);
                //instrumentGenLists.getReference( instrumentGenLists.size() - 1).printDescription();
            };
            jassert( chunk.getSize() >= 4);
            jassert( chunk.getSize() % 4 == 0 );
            return parse(chunk, parseImpl, target.instrumentGeneratorsList);
        }
        case Types::shdr:
        {
            auto parseImpl = [this, &target](MemoryInputStream& mis)
            {
                MemoryBlock mb(20, true);
                mis.read(mb.getData(), (int)mb.getSize());
                
                SampleHeader sampleHeader;
                sampleHeader.name = String(static_cast<const char*>(mb.getData()), mb.getSize());
                sampleHeader.startingSampleIndex = Reader::readDword(mis);
                sampleHeader.endingSampleIndex = Reader::readDword(mis);
                sampleHeader.loopStartSampleIndex = Reader::readDword(mis);
                sampleHeader.loopEndSampleIndex = Reader::readDword(mis);
                sampleHeader.sampleRate = Reader::readDword(mis);
                sampleHeader.originalMIDIPitch = Reader::readByte(mis);
                sampleHeader.playbackPitchCorrectionCents = Reader::readChar(mis);
                sampleHeader.wSampleLink = Reader::readWord(mis);
                sampleHeader.sampleType = static_cast<SampleHeader::SFSampleLink>(Reader::readWord(mis));
                
                target.sampleHeadersList.add(sampleHeader);
                //sampleHeaders.getReference( sampleHeaders.size() - 1).printDescription();
            };
            jassert( chunk.getSize() >= 46);
            jassert( chunk.getSize() % 46 == 0 );
            return parse(chunk, parseImpl, target.sampleHeadersList);
        }
            //case Types::ChunkType::UNKNOWN:
        default:
        {
            DBG("unknown chunk type: " << Types::fourCCtoString(chunk.getID()) );
            jassertfalse;
            return chunk.getStart() + 1;
            break;
        }
    }
    return 0;
}

int64 Parser::parseString(juce::String& str, Chunk& chunk)
{
    MemoryBlock block(chunk.getSize(), true);
    if( chunk.read( block.getData() ) )
    {
        str = String(static_cast<const char*>(block.getData()),
                     block.getSize());
        DBG( str );
        return chunk.getEnd();
    }
    else
    {
        jassertfalse;
    }
    return chunk.getStart() + 1;
}
}//end namespace SF2
}//end namespace SoundFont

#undef BEGIN_SF2_NAMESPACE
#undef BEGIN_SOUNDFONT_NAMESPACE

