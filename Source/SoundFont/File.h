/*
  ==============================================================================

    File.h
    Created: 25 Nov 2018 2:11:36am
    Author:  Charles Schiermeyer

  ==============================================================================
*/

#pragma once

#include "../../JuceLibraryCode/JuceHeader.h"
#include "Chunk.h"
#include "Types.h"
namespace SoundFont { namespace SF2 {
    
    namespace Reader
    {
        Types::BYTE readByte(juce::InputStream& file);
        Types::CHAR readChar(juce::InputStream& file);
        Types::WORD readWord(juce::InputStream& file);
        Types::SHORT readShort(juce::InputStream& file);
        Types::DWORD readDword(juce::InputStream& file);
        Types::FOURCC readFourCC(juce::InputStream& file);
    };
    
    /**
     the SF2 file format, via JUCE containers and objects
     */
    struct File
    {
        File(const juce::File& f) : file(f) { }
        bool parsedOk();
        const juce::File file;
        
        SF2::VersionTag version;
        String soundFontName;
        String soundEngine;
        String intendedProduct;
        String soundFontEngineers;
        String soundFontEditor;
        String creationDate;
        String comments;
        String copyright;
        
        /*
         These hold the location in the file where the sample data is stored.
         */
        ScopedPointer<SF2::Chunk> sampleBufferALocation; //lower 16 bits of samples
        ScopedPointer<SF2::Chunk> sampleBufferBLocation; //remaining 8 bits if samples are 24bit
        
        /** PHDR */
        Array<SF2::PresetHeader> presetHeadersList;
        /** PBAG */
        Array<SF2::PresetZone> presetZonesList;
        /** PMOD */
        Array<SF2::ZoneModulator> presetModulatorsList;
        /** PGEN */
        Array<SF2::Generator> presetGeneratorsList;
        /** inst */
        Array<SF2::Instrument> instrumentsList;
        /** IBAG */
        Array<SF2::InstrumentZone> instrumentZonesList;
        /** IMOD */
        Array<SF2::ZoneModulator> instrumentModulatorsList;
        /** IGEN */
        Array<SF2::Generator> instrumentGeneratorsList;
        /** SHDR */
        Array<SF2::SampleHeader> sampleHeadersList;
    };
}//end namespace SF2
}//end namespace SoundFont
