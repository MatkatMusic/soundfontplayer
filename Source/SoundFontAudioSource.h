/*
  ==============================================================================

    SoundFontAudioSource.h
    Created: 14 Nov 2018 12:34:55am
    Author:  Charles Schiermeyer

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "SoundFontSynth.h"

struct SoundFontAudioSource : public AudioSource
{
    SoundFontAudioSource(MidiKeyboardState& keyState);
    
    void prepareToPlay( int, double sampleRate ) override;
    
    void releaseResources() override;
    
    void getNextAudioBlock(const AudioSourceChannelInfo& bufferToFill) override;

    SoundFontSynth synth;
private:
    MidiKeyboardState& state;
};
