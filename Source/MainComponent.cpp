/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"
#include "SoundFontParser.h"
#include "SoundFontSound.h"
#include "SoundFontVoice.h"

//==============================================================================
int SineWaveVoice::voiceIDCounter = 0;

//==============================================================================

MainComponent::MainComponent()
{
    deviceManager.initialiseWithDefaultDevices(0, 2);
    if( auto* device = deviceManager.getCurrentAudioDevice() )
    {
        DBG( "current audio device: " << device->getName() );
    }
    addAndMakeVisible(keyboard);
    keyboard.setAvailableRange(21, 21+87);
    keyboard.setOctaveForMiddleC(4);
    addAndMakeVisible(availablePresetsComboBox);
    // Make sure you set the size of the component after
    // you add any child components.
    setSize (1000, 600);

    // specify the number of input and output channels that we want to open
    setAudioChannels (2, 2);
    
    auto devices = MidiInput::getDevices();
    for( auto& device : devices )
    {
        DBG("opening device: " << device);
        this->deviceManager.setMidiInputEnabled(device, true);
        this->deviceManager.addMidiInputCallback(device, this);
    }
    
    parser = SoundFont::SF2::Parser::getInstance();
    auto SF2 = File::getSpecialLocation(File::SpecialLocationType::currentApplicationFile).getChildFile("Contents").getChildFile("Resources").getChildFile("MuseScore_General.sf2");
    if( SF2.existsAsFile() && false)
    {
        if( SoundFont::SF2::File* sf2File = parser->parse(SF2) )
        {
            if( sf2File->parsedOk() )
            {
            //populate the combobox with patch names
                StringArray presetNames;
                for( auto& preset : sf2File->presetHeadersList )
                {
                    auto& name = preset.presetName;
                    if( name == "EOP" ) continue; //ignore EOP preset
                    String bank(preset.midiBankNumber);
                    bank = bank.paddedLeft('0', 3);
                    String num(preset.midiPresetNumber);
                    num = num.paddedLeft('0', 3);
                    String str;
                    str << bank << ":" << num << " " << name;
                    DBG( str );
                    presetNames.add(str);
                }
                presetNames.sort(true);
                availablePresetsComboBox.addItemList(presetNames, 1);
                
                availablePresetsComboBox.onChange = [&, sf2File]()
                {
                    DBG( "you chose preset: " << availablePresetsComboBox.getSelectedItemIndex() << " " << availablePresetsComboBox.getText() );
                    //parse the xxx:yyy to find the bank number
                    auto text = availablePresetsComboBox.getText();
                    if( !text.contains(":") )
                    {
                        jassertfalse; //didn't select a valid preset name
                        return;
                    }
                    
                    auto bank = text.substring(0, text.indexOf(":") ).getIntValue();
                    auto num = text.substring(text.indexOf(":")+1, text.indexOf(" ")).getIntValue();
                    
                    DBG( "BANK: " << bank );
                    DBG( "NUM: " << num );
                    
                    //find presetHeadersList entry with matching bank and num
                    SoundFont::SF2::PresetHeader* presetHeader = nullptr;
                    for( auto& preset : sf2File->presetHeadersList )
                    {
                        if(preset.midiBankNumber == bank &&
                           preset.midiPresetNumber == num)
                        {
                            presetHeader = &preset;
                            break;
                        }
                    }
                    if( !presetHeader )
                    {
                        jassertfalse; //couldn't find the preset, which is super odd..
                        return;
                    }
                    
                    DBG( "Preset: " << presetHeader->presetName << " bank: " << presetHeader->midiBankNumber << " num: " << presetHeader->midiPresetNumber );
                    
                    //auto* parser = SoundFont::SF2::Parser::getInstance();
                    auto presetIndex = sf2File->presetHeadersList.indexOf(*presetHeader);
                    //auto preset = parser->buildCompletePreset(presetIndex, *sf2File);
                    
                    audioSource.synth.clearSounds();
                    audioSource.synth.addSound(new SoundFont2Sound(presetIndex, *sf2File) );
//                    for( int i = 0; i < 16; ++i )
//                    {
//                        synth.addVoice( new SoundFont2Voice() );
//                    }
                    audioSource.synth.changePreset(presetIndex, *sf2File);
                };
                
            }
        }
    }
    
    addAndMakeVisible(bezierComponent);
    addAndMakeVisible(quadComponent);
    addAndMakeVisible(bezierBox);
    addAndMakeVisible( curveEditor );
}

MainComponent::~MainComponent()
{
    auto devices = MidiInput::getDevices();
    for( auto& device : devices )
    {
        this->deviceManager.setMidiInputEnabled(device, false);
        this->deviceManager.removeMidiInputCallback(device, this);
    }
    // This shuts down the audio device and clears the audio source.
    shutdownAudio();
    if( !parser.wasObjectDeleted() )
        SoundFont::SF2::Parser::deleteInstance();
}

//==============================================================================
void MainComponent::prepareToPlay (int samplesPerBlockExpected, double sampleRate)
{
    // This function will be called when the audio device is started, or when
    // its settings (i.e. sample rate, block size, etc) are changed.

    // You can use this function to initialise any resources you might need,
    // but be careful - it will be called on the audio thread, not the GUI thread.

    // For more details, see the help for AudioProcessor::prepareToPlay()
    audioSource.prepareToPlay(samplesPerBlockExpected, sampleRate);
    synthAudioSource.prepareToPlay(samplesPerBlockExpected, sampleRate);
}

void MainComponent::getNextAudioBlock (const AudioSourceChannelInfo& bufferToFill)
{
    // Your audio-processing code goes here!

    // For more details, see the help for AudioProcessor::getNextAudioBlock()

    // Right now we are not producing any data, in which case we need to clear the buffer
    // (to prevent the output of random noise)
    bufferToFill.clearActiveBufferRegion();
    //audioSource.getNextAudioBlock(bufferToFill);
    synthAudioSource.getNextAudioBlock(bufferToFill);
}

void MainComponent::releaseResources()
{
    // This will be called when the audio device stops, or when it is being
    // restarted due to a setting change.

    // For more details, see the help for AudioProcessor::releaseResources()
    audioSource.releaseResources();
    
    synthAudioSource.releaseResources();
}

//==============================================================================
void MainComponent::paint (Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll (getLookAndFeel().findColour (ResizableWindow::backgroundColourId));

    // You can add your drawing code here!
}

void MainComponent::resized()
{
    // This is called when the MainContentComponent is resized.
    // If you add any child components, this is where you should
    // update their positions.
    keyboard.setBounds(0, getHeight() * (1.0f - 0.3f), getWidth(), getHeight() * 0.3f);
    keyboard.setKeyWidth((float)getWidth() / ((float(keyboard.getRangeEnd() - keyboard.getRangeStart() ) / 12.f) * 7.f) );
    
    
    availablePresetsComboBox.setBounds(0, 0, getWidth(), 30);
    
    bezierComponent.setBounds(20, availablePresetsComboBox.getBottom() + 20, 150, 150);
    quadComponent.setBounds(bezierComponent.getBounds().withX(bezierComponent.getRight() + 10));
    bezierBox.setBounds(quadComponent.getBounds().withX(quadComponent.getRight()+10).withWidth(quadComponent.getWidth() * 3));
    
    curveEditor.setBounds(bezierBox.getBounds().withY( bezierBox.getBottom() + 5));
}

//==============================================================================
void MainComponent::handleIncomingMidiMessage(MidiInput *source, const MidiMessage &message)
{
    state.processNextMidiEvent(message);
    if( message.isNoteOn() )
    {
        //audioSource.synth.noteOn(message.getChannel(), message.getNoteNumber(), message.getFloatVelocity());
        synthAudioSource.synth.noteOn(message.getChannel(), message.getNoteNumber(), message.getFloatVelocity());
    }
    else if( message.isNoteOff() )
    {
        //audioSource.synth.noteOff(message.getChannel(), message.getNoteNumber(), message.getFloatVelocity(), true);
        synthAudioSource.synth.noteOff(message.getChannel(), message.getNoteNumber(), message.getFloatVelocity(), true);
        
    }
}
