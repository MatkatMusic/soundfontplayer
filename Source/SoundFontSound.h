/*
  ==============================================================================

    SoundFontSound.h
    Created: 14 Nov 2018 12:19:50am
    Author:  Charles Schiermeyer

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

//#include "SoundFontParser.h"
#include "SoundFont/File.h"
#include "SoundFont/Parser.h"

struct SF2InstrumentZoneParameters
{
    /*
     holds the actual parsed generator values for an instrument zone in a soundFont patch
     
     these values are applied to the zone, to modify its playback properties
     
     Generators in the IGEN sub-chunk are absolute in nature. This means that an IGEN generator replaces, rather than adds to, the default value for the generator.
     
     Generators in the PGEN sub-chunk are applied relative to generators in the IGEN sub-chunk in an additive manner. In other words, PGEN generators increase or decrease the value of an IGEN generator.
     
     So, this means the correct parsing of generators into this SoundFont2Zone structure is:
     
     1a) keyRange = Parser::getInstance()->getGenerator(preset.zones[i].instrument->zones[j].generators, GeneratorTypes::keyRange)->genAmount.range;
     1b) keyRange = Parser::getInstance()->getGenerator(preset.zones[i].instrument->globalZone.generators, GeneratorTypes::keyRange)->genAmount.range;
     2) keyRange += Parser::getInstance()->getGenerator(preset.zones[i].generators, GeneratorTypes::keyRange)->genAmount.range;
     3) keyRange += Parser::getInstance()->getGenerator(preset.globalZone.generators, GeneratorTypes::keyRange)->genAmount.range;
     
     During run time, modulators are applied to this final generator value
     
     Modulators in the IMOD sub-chunk are absolute. This means that an IMOD modulator replaces, rather than adds to, a default modulator. However the effect of a modulator on a generator is additive, IE the output of a modulator adds to a generator value.
     
     Modulators in the PMOD sub-chunk act as additively relative modulators with respect to those in the IMOD sub-chunk. In
     other words, a PMOD modulator can increase or decrease the amount of an IMOD modulator.
     
     to calculate modulators correctly, do this:
     
     4) keyRange += getModulatorValue( preset.zones[i].instrument->zones[j].modulators, ModulatorType::KeyRange );
     
     
     
     */
    /*
     these are the default generator values for each generator type
     
     ***
     A generator at the preset level adds to a generator at the instrument level if both generators are identical.
     ***
     
     global instrument zone generators replace the default generator
     local instrument zone generators replace global instrument zone generators
     preset zone generators add to generator value
     global preset generators add to generator value.
     */
    juce::uint16 startAddressOffset = 0;
    juce::int16 endAddressOffset = 0;
    juce::int16 startLoopAddressOffset = 0;
    juce::int16 endLoopAddressOffset = 0;
    juce::int16 startAddressCoarseOffset = 0;
    juce::int16 endAddressCoarseOffset = 0;
    juce::int16 modLFOtoPitch = 0; //probably preset level, not instrument level
    juce::int16 vibLFOtoPitch = 0;
    juce::int16 modENVtoPitch = 0;
    juce::uint16 initialFilterFrequency = 13500;
    juce::uint16 initialFilterQ = 0;
    juce::int16 modLFOtoFilterFc = 0;
    juce::int16 modENVtoFilterFc = 0;
    juce::int16 modLFOtoVolume = 0;
    juce::uint16 chorusEffectsSend = 0;
    juce::uint16 reverbEffectsSend = 0;
    juce::int16 pan = 0;
    juce::int16 delayModLFO = -12000; //delay time before Mod LFO kicks in
    juce::int16 freqModLFO = 0;
    juce::int16 delayVibLFO = -12000;
    juce::int16 freqVibLFO = 0;
    juce::int16 delayModENV = -12000;
    juce::int16 attackModENV = -12000;
    juce::int16 holdModENV = -12000;
    juce::int16 decayModENV = -12000;
    juce::uint16 sustainModENV = 0; //unsigned
    juce::int16 releaseModENV = -12000;
    juce::int16 keynumToModENVhold = 0;
    juce::int16 keynumToModENVdecay = 0;
    juce::int16 delayVolENV = -12000;
    juce::int16 attackVolENV = -12000;
    juce::int16 holdVolENV = -12000;
    juce::int16 decayVolENV = -12000;
    juce::uint16 sustainVolENV = 0;
    juce::int16 releaseVolENV = -12000;
    juce::int16 keynumToVolENVhold = 0;
    juce::int16 keynumToVolENVdecay = 0;
    juce::uint16 instrument = 0;
    SoundFont::SF2::RangesType keyRange = {0,127};
    SoundFont::SF2::RangesType velRange = {0,127};
    juce::int16 startLoopAddressCoarseOffset = 0;
    juce::uint8 keyNum = -1;
    juce::uint8 velocity = -1;
    juce::uint8 initialAttenuation = 0;
    juce::int16 endloopAddressCoarseOffset = 0;
    juce::int8 coarseTune = 0;
    juce::int8 fineTune = 0;
    juce::uint8 sampleID = 0;
    juce::int16 sampleModes = 0;
    juce::uint16 scaleTuning = 100;
    juce::uint8 exclusiveClass = 0;
    juce::uint8 overridingRootkey = -1;
    void parseGenerator(const SoundFont::SF2::Generator &generator, bool fromPreset);
    AudioBuffer<float> buffer;
};

/**
 A subclass of SynthesiserSound that represents a sampled audio clip.
 
 This is a pretty basic sampler, and just attempts to load the whole audio stream
 into memory.
 
 To use it, create a Synthesiser, add some SamplerVoice objects to it, then
 give it some SampledSound objects to play.
 
 @see SamplerVoice, Synthesiser, SynthesiserSound
 
 @tags{Audio}
 */
class SoundFont2Sound    : public SynthesiserSound
{
public:
    //==============================================================================
    /** Creates a sampled sound from an audio reader.
     
     This will attempt to load the audio from the source into memory and store
     it in this object.
     
     @param name         a name for the sample
     @param source       the audio to load. This object can be safely deleted by the
     caller after this constructor returns
     @param midiNotes    the set of midi keys that this sound should be played on. This
     is used by the SynthesiserSound::appliesToNote() method
     @param midiNoteForNormalPitch   the midi note at which the sample should be played
     with its natural rate. All other notes will be pitched
     up or down relative to this one
     @param attackTimeSecs   the attack (fade-in) time, in seconds
     @param releaseTimeSecs  the decay (fade-out) time, in seconds
     @param maxSampleLengthSeconds   a maximum length of audio to read from the audio
     source, in seconds
     */
//    SamplerSound (const String& name,
//                  AudioFormatReader& source,
//                  const BigInteger& midiNotes,
//                  int midiNoteForNormalPitch,
//                  double attackTimeSecs,
//                  double releaseTimeSecs,
//                  double maxSampleLengthSeconds);
    /**
     creates a SF2Sound from a SF2Preset
     
     */
    SoundFont2Sound(int presetIndex, SoundFont::SF2::File& );
    
    /** Destructor. */
    ~SoundFont2Sound();
    
    //==============================================================================
    
    //==============================================================================
    bool appliesToNote (int midiNoteNumber) override;
    bool appliesToChannel (int midiChannel) override;
    
private:
    //==============================================================================
    friend class SoundFont2Voice;
    std::shared_ptr<SoundFont::SF2::SF2Preset> preset;
    SoundFont::SF2::File& soundFontFile;
    
    double sourceSampleRate;
    //BigInteger midiNotes;
    int length = 0, attackSamples = 0, releaseSamples = 0;
    int midiRootNote = 0;
    
    using Generator = SoundFont::SF2::Generator;
    using GeneratorType = SoundFont::SF2::GeneratorType;
    using Instrument = SoundFont::SF2::SF2Instrument;
    
    
    
    JUCE_LEAK_DETECTOR (SoundFont2Sound)
};
