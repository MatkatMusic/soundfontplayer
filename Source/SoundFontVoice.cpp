/*
  ==============================================================================

    SoundFontVoice.cpp
    Created: 14 Nov 2018 12:19:56am
    Author:  Charles Schiermeyer

  ==============================================================================
*/

/*
           o (x2,y2)
          /|
         / |
        /  |
       o   | (x,y)
      /|   |
     / |   |
    /  |   |
   /   |   |
  /    |   |
 o-----|---|
   (1-d) d
(x1,y1)
 */

#include "SoundFontVoice.h"
#include "SoundFontSound.h"

SoundFont2Voice::SoundFont2Voice() {}
SoundFont2Voice::~SoundFont2Voice() {}

bool SoundFont2Voice::canPlaySound (SynthesiserSound* sound)
{
    return dynamic_cast<const SoundFont2Sound*> (sound) != nullptr;
}

bool SoundFont2Voice::isPlayingOneShot()
{
    /*
     the SampleMode for this layer being played is set to
     0
     
     This enumerator indicates a value which gives a variety of Boolean flags describing the sample for the current instrument zone. The sampleModes should only appear in the IGEN sub-chunk, and should not appear in the global zone. The two LS bits of the value indicate the type of loop in the sample: 0 indicates a sound reproduced with no loop, 1 indicates a sound which loops continuously, 2 is unused but should be interpreted as indicating no loop, and 3 indicates a sound which loops for the duration of key depression then proceeds to play the remainder of the sample
     */
    return false;
}

void SoundFont2Voice::stopPlayingAndSkipRelease()
{
    /*
     set the flag to stop reading from the buffer
     */
    skipRelease = true;
}

void SoundFont2Voice::setGenerator(SoundFont::SF2::GeneratorType type, SoundFont::SF2::GeneratorAmountType amount)
{
    jassert( type < SoundFont::SF2::GeneratorType::GENERATOR_LIST_END );
    generatorValues[type] = amount;
    limitGenerator(type);
}

void SoundFont2Voice::offsetGenerator(SoundFont::SF2::GeneratorType type, SoundFont::SF2::GeneratorAmountType amount)
{
    jassert( type < SoundFont::SF2::GeneratorType::GENERATOR_LIST_END );
    generatorValues[type].shortAmount += amount.shortAmount;
    limitGenerator(type);
}

void SoundFont2Voice::limitGenerator(SoundFont::SF2::GeneratorType type)
{
    auto* parser = SoundFont::SF2::Parser::getInstance();
    
    jassert( type < SoundFont::SF2::GeneratorType::GENERATOR_LIST_END );
    auto* info = parser->getDefaultGenerator(type);
    
    using T = decltype(generatorValues[type].shortAmount);
    
    generatorValues[type].shortAmount = jlimit((T)info->min,
                                               (T)info->max,
                                               generatorValues[type].shortAmount);
}

void SoundFont2Voice::overwriteModulator(const SoundFont::SF2::ZoneModulator *mod)
{
    for( int i = 0; i < modulatorValues.size(); ++i )
    {
        if( modulatorValues.getReference(i).isIdenticalTo(*mod) )
        {
            modulatorValues.getReference(i).modAmount = mod->modAmount;
            return;
        }
    }
    
    //modulator doesn't exist.  copy it.
    modulatorValues.add( *mod );
}

void SoundFont2Voice::appendModulator(const SoundFont::SF2::ZoneModulator *mod)
{
    if( mod == nullptr ) return;
    
    for( int i = 0; i < modulatorValues.size(); ++i )
    {
        if( modulatorValues.getReference(i).isIdenticalTo(*mod) )
        {
            modulatorValues.getReference(i).modAmount += mod->modAmount;
            return;
        }
    }
}

void SoundFont2Voice::resetModulators()
{
    modulatorValues.clear();
}

void SoundFont2Voice::setBuffer(const std::shared_ptr<SoundFont::SF2::SF2Sample> &buf)
{
    buffer = buf;
}



void SoundFont2Voice::startNote (int midiNoteNumber, float velocity, SynthesiserSound* s, int currentPitchWheelPosition)
{
    if( buffer.get() == nullptr )
    {
        /*
         somehow, the synthesiser noteOn picked a layer that didn't have a sample attached to it.
         */
        jassertfalse;
        return;
    }
    playingNoteNum = midiNoteNumber;
    pitchWheel = currentPitchWheelPosition;
    //playedVelocity = velocity;
    /*
     startNote's job is to apply the generators and modulators to the voice parameters
     and also set up the pitch/rate playback of the sample
     */
    applyModulators();
    updateVoiceParams();
    /*
     set up sample playback parameters
     */
    sourceSamplePosition = 0;
    
    using GT = SoundFont::SF2::GeneratorType;
    loopStart = buffer->header->loopStartSampleIndex +
                generatorValues[GT::startLoopAddressOffset].wordAmount +
                generatorValues[GT::startloopAddrsCoarseOffset].wordAmount * 32768;
    loopEnd =   buffer->header->loopEndSampleIndex +
                generatorValues[GT::endLoopAddressOffset].wordAmount +
                generatorValues[GT::endloopAddrsCoarseOffset].wordAmount * 32768;
    
    loopStart -= buffer->header->startingSampleIndex;
    loopEnd -= buffer->header->startingSampleIndex;
    
    DBG( "sample name: " << buffer->header->name );
    DBG( "loop start: " << loopStart << " loopEnd: " << loopEnd );
    loopLength = loopEnd - loopStart;
    isLoopable = generatorValues[GT::sampleModes].shortAmount & 0x03;
    DBG( "sample is loopable: " << (int)isLoopable );
    
    isInAttack = true;
    attackDelta = 0.01f;
    releaseDelta = 0.001f;
    calcPitchRatio();
    /*
     set up volume envelope parameters
     */
    
    volumeEG.reset();
    
    {
        auto delayVolTime = generatorValues[GT::delayVolEnv].shortAmount;
        auto attackVolTime = generatorValues[GT::attackVolEnv].shortAmount;
        auto sustainLevel = (float)generatorValues[GT::sustainVolEnv].shortAmount / 100.f;
        auto holdVolTime = generatorValues[GT::holdVolEnv].shortAmount;
        auto decayVolTime = generatorValues[GT::decayVolEnv].shortAmount;
        auto samplesPerSecond = getSampleRate();
        
        volumeEG.setSustainLevel(juce::Decibels::decibelsToGain(-sustainLevel));
        volumeEG.setAttackStages({
            EnvelopeGenerator<double>::MakeSegment("Delay",
                                                   samplesPerSecond * timecents2Secs(delayVolTime),
                                                   0),
            EnvelopeGenerator<double>::MakeSegment("Attack",
                                                   samplesPerSecond * timecents2Secs(attackVolTime),
                                                   1.f),
            EnvelopeGenerator<double>::MakeSegment("Hold",
                                                   samplesPerSecond * timecents2Secs(holdVolTime),
                                                   1.f),
            EnvelopeGenerator<double>::MakeSegment("Decay",
                                                   samplesPerSecond * timecents2Secs(decayVolTime),
                                                   juce::Decibels::decibelsToGain(-sustainLevel))
        });
        
        auto releaseVolTime = generatorValues[GT::releaseVolEnv].shortAmount;
        volumeEG.setReleaseStages({
            EnvelopeGenerator<double>::MakeSegment("Release",
                                                   samplesPerSecond * timecents2Secs(releaseVolTime),
                                                   0.f),
        });
        
        volumeEG.printDescription();
        volumeEG.initAfterAddingStages();
    }
    /*
     set up filter envelope parameters
     */
    /*
     set up filter parameters
     */
    /*
     set up amplifier parameters
     */
    
    double volume_dB = static_cast<double>(-1 * generatorValues[GT::initialAttenuation].shortAmount) / 100.0;
    double noteGainDB = -1.0 + volume_dB;
    double velocityGainDB;
    {
        double maxVelocity = 127 * 127;
        double playedVelocity;
        if( generatorValues[GT::velocity].shortAmount != -1 )
        {
            /*
             This enumerator forces the MIDI velocity to effectively be interpreted as the value given. This generator can only appear at the instrument level. Valid values are from 0 to 127.
             */
            playedVelocity = generatorValues[GT::velocity].shortAmount / 127.0;
            playedVelocity = playedVelocity * playedVelocity;
        }
        else
        {
            playedVelocity = (velocity * 127.0) * (velocity * 127.0);
        }
        velocityGainDB = -20.0 * std::log10( maxVelocity / playedVelocity );
        /*
         in the SFZ spec, velocity tracking is a thing.  in SF2 spec, it is not
         if we ever get around to supporting SFZ, this is where I would pull the velocity tracking from the list of generators
         */
        {
            double velocityTracking = 100.0;
            velocityGainDB *= velocityTracking / 100.0;
        }
        noteGainDB += velocityGainDB;
    }
    lgain = static_cast<float>(juce::Decibels::decibelsToGain(noteGainDB));
    rgain = lgain;
    
    {
        double pan = static_cast<double>(generatorValues[GT::pan].shortAmount);
        pan = jmap(pan, -500.0, 500.0, 0.0, 1.0);
        lgain *= static_cast<float>(std::sqrt(1.0 - pan));
        rgain *= static_cast<float>(std::sqrt(pan));
    }
}

void SoundFont2Voice::stopNote (float /*velocity*/, bool allowTailOff)
{
    DBG( "stopNote" );
    if (allowTailOff)
    {
        //trigger release phase of sample
        //isInAttack = false;
        //isInRelease = true;
        DBG( "trigger release" );
        isLooping = false;
        isLoopable = false;
        volumeEG.beginRelease();
    }
    else
    {
        DBG( "killing note" );
        //clearCurrentNote();
        volumeEG.releaseQuick(static_cast<int>(getSampleRate()));
    }
}

void SoundFont2Voice::pitchWheelMoved (int newValue)
{
    pitchWheel = newValue;
    calcPitchRatio();
    updateVoiceParams();
}
void SoundFont2Voice::controllerMoved (int controllerNumber, int newValue)
{
    ccValues[controllerNumber] = newValue;
    applyModulators();
    updateVoiceParams();
}

void SoundFont2Voice::applyModulators()
{
    /*
     applies each modulator's calculated value to the target generator
     MuseScore's Fluid's voice does this in 3 steps:
     0) float modval = 0.0;
     1) int g = mod->get_dest();
        for( auto* mod : mods )
            if( mod_has_dest(mod, g) )
                modVal += mod[k].getValue(channel, this);
     2) gen[g].set_mod(modVal);
     3) update_param(g);
     */
}

void SoundFont2Voice::updateVoiceParams()
{
    /*
     applies all of the generators to their respective synth property
     
     */
}

void SoundFont2Voice::calcPitchRatio()
{
    //this is ported from SfZero
    using GT = SoundFont::SF2::GeneratorType;
    double note = static_cast<double>(playingNoteNum);
    note += generatorValues.at(GT::coarseTune).shortAmount;
    note += generatorValues.at(GT::fineTune).shortAmount / 100.0;

    auto pitch_keyCenter = generatorValues.at(GT::overridingRootKey).shortAmount;
    DBG( "pitch_keyCenter: " << pitch_keyCenter );
    double adjustedPitch;
    {
        auto pitch_keyTrack = generatorValues.at(GT::scaleTuning).shortAmount;
        
        adjustedPitch = pitch_keyCenter + (note - pitch_keyCenter) * (pitch_keyTrack / 100.0 );
    }
    DBG( "adjustedPitch: " << adjustedPitch );
    
    if( pitchWheel != 8192 )
    {
        double wheel = ((2.0 * pitchWheel / 16383.0) - 1.0);
        //hard-coded for 200 cents up and down from adjustedPitch.
        if (wheel > 0)
        {
            adjustedPitch += wheel * 200.0 / 100.0;
        }
        else
        {
            adjustedPitch += wheel * -200.0 / -100.0;
        }
    }
    
    DBG( "adjustedPitch: " << adjustedPitch );
    
    double targetFreq = fractionalMidiNoteInHz(adjustedPitch);
    double naturalFreq = juce::MidiMessage::getMidiNoteInHertz(pitch_keyCenter);
    pitchRatio = (targetFreq * buffer->header->sampleRate) / (naturalFreq * getSampleRate());
}

double SoundFont2Voice::fractionalMidiNoteInHz(double note, double freqOfA)
{
    note -= 69;
    return freqOfA * std::pow(2.0, note / 12.0);
}

//==============================================================================
void SoundFont2Voice::renderNextBlock (AudioBuffer<float>& outputBuffer, int startSample, int numSamples)
{
    if (auto* playingSound = static_cast<SoundFont2Sound*> (getCurrentlyPlayingSound().get()))
    {
        /*
         this is the audio buffer in SoundFont2Sound, which holds the audio file that was passed to it.
         */
        
        if( buffer.get() == nullptr )
            return;
        
        auto& data = buffer->buffer;
        
        const float* const inL = data.getReadPointer (0);
        const float* const inR = data.getNumChannels() > 1 ? data.getReadPointer (1) : nullptr;
        
        float* outL = outputBuffer.getWritePointer (0, startSample);
        float* outR = outputBuffer.getNumChannels() > 1 ? outputBuffer.getWritePointer (1, startSample) : nullptr;
        
        while( --numSamples >= 0 )
        {
            bool envelopeCompleted = volumeEG.tick();
            if( envelopeCompleted )
            {
                stopNote(0.f, false);
                break;
            }
            
            
            if( sourceSamplePosition >= data.getNumSamples()  )
            {
                //you read past the audio buffer's end.
                stopNote(0.f, false);
                break;
            }
            else
            {
                auto pos = int(sourceSamplePosition);
                
                int nextPos;
                float curLSample = inL[pos];
                float curRSample = inR ? inR[pos] : curLSample;
                float nextLSample = 0.f;
                float nextRSample = 0.f;
                if( isLooping && isLoopable )
                {
                    nextPos = pos + 1;
                    if( nextPos > loopEnd )
                    {
                        nextPos = loopStart;
                    }
                    nextLSample = inL[nextPos];
                    nextRSample = inR ? inR[nextPos] : nextLSample;
                }
                else
                {
                    nextPos = pos + 1;
                    if( nextPos >= data.getNumSamples() )
                    {
                        nextLSample = 0.f;
                        nextRSample = 0.f;
                    }
                    else
                    {
                        nextLSample = inL[nextPos];
                        nextRSample = inR ? inR[nextPos] : nextLSample;
                    }
                }
                
                //compute interpolated output sample using linear interpolation
                float d = float(sourceSamplePosition - pos);
                float interpolatedL, interpolatedR;
                interpolatedL = curLSample + d * ( nextLSample - curLSample );
            
                if( inR != nullptr )
                {
                    interpolatedR = curRSample + d * ( nextRSample - curRSample );
                }
                else
                {
                    interpolatedR = interpolatedL;
                }
                
                /*
                 apply envelope to panned gain
                 */
                {
                    auto volumeLevel = volumeEG.getLevel();
                    lgain *= volumeLevel;
                    rgain *= volumeLevel;
                }
                
                /*
                 apply panned gain to samples
                 */
                interpolatedL *= lgain;
                interpolatedR *= rgain;
                
                //MIX our sample with the existing buffer
                if (outR != nullptr) //stereo output
                {
                    *outL++ += interpolatedL;
                    *outR++ += interpolatedR;
                }
                else //mono output
                {
                    *outL++ += (interpolatedL + interpolatedR) * 0.5f;
                }
            } //end writing valid sample to buffer
            
            sourceSamplePosition += pitchRatio;
            
            if( isLoopable )
            {
                if( (loopStart < loopEnd) && (sourceSamplePosition > loopEnd) )
                {
                    sourceSamplePosition = loopStart;
                }
            }
        }
    }
}
