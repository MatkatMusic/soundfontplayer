/*
  ==============================================================================

    BezierBox.cpp
    Created: 14 Dec 2018 3:28:03pm
    Author:  Charles Schiermeyer

  ==============================================================================
*/

#include "BezierBox.h"

//==============================================================================
/*
 CurveNode
 */
const Colour CurveNode::NodeColor = Colour(100u, 178u, 253u);
CurveNode::CurveNode(ComponentBoundsConstrainer* c) : constrainer(c)
{
//    setInterceptsMouseClicks(false, false);
    setColour(ColorIDs::idleColor, NodeColor);
    setColour(ColorIDs::draggedColor, Colour(220u, 62u, 164u));
    setSize(NodeDiameter, NodeDiameter);
}
void CurveNode::mouseDown(const MouseEvent& e)
{
    isDragged = true;
    dragger.startDraggingComponent(this, e);
    repaint();
}
void CurveNode::mouseDrag(const MouseEvent& e)
{
    dragger.dragComponent(this, e, constrainer);
}
void CurveNode::mouseUp(const MouseEvent &e)
{
    isDragged = false;
    repaint();
}
void CurveNode::paint(Graphics& g)
{
    auto bounds = getLocalBounds();
    g.setColour( isDragged ? findColour(ColorIDs::draggedColor) : findColour(ColorIDs::idleColor ) );
    g.fillEllipse(bounds.toFloat());
}
void CurveNode::setIsDragged(bool dragged)
{
    isDragged = dragged;
    repaint();
}
//==============================================================================
/*
 Curve
 */
const Colour Curve::CurveColor = Colour(100u, 178u, 253u);

void Curve::linkLeft(CurveNode &l)
{
    l.attachedCurveRight = this;
    left = &l;
}

void Curve::linkRight(CurveNode &r)
{
    r.attachedCurveLeft = this;
    right = &r;
}

bool Curve::contains(double x)
{
    return false;
}

double Curve::getValueAt(double x)
{
    return 0.0;
}

Curve::Curve(ComponentBoundsConstrainer* c) : control(c)
{
    setColour(ColorIDs::idleColor, CurveColor);
    setColour(ColorIDs::draggedColor, Colour(220u, 62u, 164u));
    setInterceptsMouseClicks(false, false);
    setPaintingIsUnclipped(true);
    addAndMakeVisible(control);
    control.setInterceptsMouseClicks(false, false);
}
//void Curve::mouseDown(const MouseEvent& e)
//{
//
//}
//void Curve::mouseDrag(const MouseEvent& e)
//{
//
//}
void Curve::paint(Graphics& g)
{
    g.setColour(isDragged ? findColour(ColorIDs::draggedColor) : findColour(ColorIDs::idleColor ) );
    //Path p;
    updatePath();
//    Point<float> start = left->getBounds().getCentre().toFloat() - getPosition().toFloat();
//    Point<float> end = right->getBounds().getCentre().toFloat() - getPosition().toFloat();
//    //DBG( "start: " << start.toString() << " end: " << end.toString() );
//    p.startNewSubPath(start);
//    p.lineTo(end);
    
    if( left->getY() == right->getY() )
        g.strokePath(p, PathStrokeType(1));
    else
        g.strokePath(p, PathStrokeType(2));
    
    if( showDrag )
    {
        g.setColour(Colours::yellow.withAlpha(0.5f));
        //g.strokePath(p, PathStrokeType(curveMargin * 2));
        PathStrokeType pst(curveMargin*2);
        Path dest;
        pst.createStrokedPath(dest, p);
        //g.fillPath(p);
        g.strokePath(dest, PathStrokeType(1));
    }
    else if( isDragged )
    {
        
        PathStrokeType pst(curveMargin*2);
        Path dest;
        pst.createStrokedPath(dest, p);
        
        g.setColour(Colours::orange.withAlpha(0.5f));
        g.strokePath(dest, PathStrokeType(1));
    }
    else if( showCurve )
    {
        
    }
    
    Rectangle<int> area(left->getBounds().getCentre() - getPosition(),
                     right->getBounds().getCentre() - getPosition());
    g.setColour(Colours::green);
    Rectangle<float> r (CurveNode::NodeDiameter, CurveNode::NodeDiameter);
    int x;
    if( left->getY() < right->getY() )
    {
        x = area.getX() + area.getWidth() * (1.0 - controlY);
    }
    else
    {
        x = area.getX() + area.getWidth() * controlY;
    }
    
    auto y = area.getY() + area.getHeight() * controlY;
    r.setCentre(x, y);
    g.fillEllipse(r);
}

void Curve::updatePath()
{
    p.clear();
    Point<float> start = left->getBounds().getCentre().toFloat() - getPosition().toFloat();
    Point<float> end = right->getBounds().getCentre().toFloat() - getPosition().toFloat();
    if( controlX == 0.5 )
    {
        Rectangle<int> r(left->getBounds().getCentre() - getPosition(),
                         right->getBounds().getCentre() - getPosition());

        float x;
        if( left->getY() < right->getY() )
        {
            x = r.getX() + r.getWidth() * (1.0 - controlY);
        }
        else
        {
            x = r.getX() + r.getWidth() * controlY;
        }
        float y = r.getY() + r.getHeight() * controlY;
        DBG( "updatePath() " << r.toString() << " x,y: " << x << ", " << y);
        p.startNewSubPath(start);
        p.quadraticTo({x,y}, end);
    }
    else if( controlY == 0.5 )
    {
        
    }
}

Point<float> Curve::getControlXY()
{
    Rectangle<float> r{left->getBounds().getCentre().toFloat() - getPosition().toFloat(),
                      right->getBounds().getCentre().toFloat() - getPosition().toFloat()};
    float x = r.getX() + r.getWidth() * controlX;
    float y = r.getY() + r.getHeight() * controlY;
    return {x,y};
}

void Curve::resized()
{
    control.setCentrePosition(getControlXY().roundToInt());
}
bool Curve::hitTest(int x, int y)
{
    /*
     x and y are passed in relative to the owning component
     updatePath() calculates the path relative to us
     we must subtract our position from the passed-in x/y to make them relative to us
     */
    updatePath();
    Path path;
    PathStrokeType pst(curveMargin * 2);
    pst.createStrokedPath(path, p);
    return path.contains(x - getX(), y - getY());
}
void Curve::setIsDragged(bool dragged)
{
    left->setIsDragged(dragged);
    right->setIsDragged(dragged);
    
    isDragged = dragged;
    
    repaint();
}

void Curve::showDragArea(bool show)
{
    showDrag = show;
    repaint();
}

void Curve::showCurveArea(bool show)
{
    showCurve = show;
    repaint();
}
//==============================================================================
/*
 CurveEditor
 
 TODO:
 show draggable area in Curve
 show curvable area in Curve
 add Curvability to curves
 limit maximum number of nodes
 add node snapshot on mouseDown for restoring nodes if you drag past them, and back.
 */
CurveEditor::CurveEditor(Type t) : type(t)
{
    startNode = createControlNode();
    endNode = createControlNode();
    
    if( t == Type::Unipolar )
    {
        startNode->pos = {0, 0};
        endNode->pos = {1, 0};
    }
    else if( t == Type::Bipolar )
    {
        startNode->pos = {0, 0.5};
        endNode->pos = {1, 0.5};
    }
    
    //createCurve(startNode, endNode);
    
    auto* node1 = createControlNode();
    node1->pos = {0.3, 0.3};
    createCurve(startNode, node1);
    
    auto* node2 = createControlNode();
    node2->pos = {0.7, 0.8};
    createCurve(node2, endNode);
    createCurve(node1, node2);
}

Rectangle<int> CurveEditor::getUsableArea()
{
    return getLocalBounds().reduced(2);
}

Rectangle<int> CurveEditor::getNodeArea()
{
    return getUsableArea().reduced(2);
}

Rectangle<int> CurveEditor::getXYArea()
{
    return getNodeArea().reduced(CurveNode::NodeDiameter/2);
}

void CurveEditor::resized()
{
    auto placementBounds = getNodeArea();
    auto xyBounds = getXYArea();
    constrainer.setMinimumOnscreenAmounts(0xffffff, 0xffffff, 0xffffff, 0xffffff);
    constrainer.setSizeLimits(placementBounds.getX(), placementBounds.getY(), placementBounds.getWidth(), placementBounds.getHeight());
    
    for( auto* point : controlPoints )
    {
        point->setCentrePosition(xyBounds.getX() + int(point->pos.x * xyBounds.getWidth()),
                                 xyBounds.getY() + int((1 - point->pos.y) * xyBounds.getHeight()) );
        if( point == startNode )
        {
            startNodeXLimit.left = point->getX();
            startNodeXLimit.right = point->getRight();
        }
        if( point == endNode )
        {
            endNodeXLimit.left = point->getX();
            endNodeXLimit.right = point->getRight();
        }
    }
    
    for( auto* curve : curves )
    {
        resizeCurve(curve);
    }
}

void CurveEditor::paint(Graphics& g)
{
    g.fillAll(Colours::black);
    g.setColour(Colours::darkcyan);
//    g.drawRect(getLocalBounds().reduced(2), 2);
    g.fillRect(getUsableArea());
    g.setColour(Colours::black);
    g.fillRect(getNodeArea());
    g.drawDashedLine({ Point<float>(0, getHeight()/2),
                       Point<float>(getWidth(), getHeight()/2) },
                     dashLengths.getRawDataPointer(),
                     dashLengths.size(),
                     0.5f);
    
}
void CurveEditor::mouseDown(const MouseEvent& e)
{
    auto clickPos = e.getEventRelativeTo(this).getPosition();
    //assume we clicked/dragged on nothing
    draggedComponent = nullptr;
    clickedCurve = nullptr;
    
    bool clickedNode = false;
    for( auto* node : controlPoints )
    {
        if( node->getBounds().contains(clickPos) )
        {
            draggedComponent = node;
            clickedNode = true;
            break;
        }
    }
    
    if( !clickedNode )
    {
        //set all curves to isDragged = false, showDragArea = false
        //find curve that has click in bounding box
        //perform hittest to see if clicked in dragging region
        //  if yes, drag
        //else curve
        for( auto* curve : curves )
        {
            curve->setIsDragged(false);
            curve->showCurveArea(false);
            curve->showDragArea(false);
        }
        
        for( auto* curve : curves )
        {
            if( curve->getBounds().contains(clickPos) )
            {
                clickedCurve = curve;
                break;
            }
        }
        
        if( clickedCurve )
        {
            if( clickedCurve->hitTest(clickPos.x, clickPos.y) )
            {
                //drag the curve
                draggedComponent = clickedCurve;
                clickedCurve->setIsDragged(true);
                clickedCurve->toFront(false);
                clickedCurve->left->toFront(false);
                clickedCurve->right->toFront(false);
            }
            /*
            else
            {
                //modify the curve via dragging
            }
             */
        }
    }
    
    /*
     nodes handle dragging themselves, but we listen for their drags as we update their normalized positions when they're dragged.
     curve drags are handled by us.
     */
    if( draggedComponent && !clickedNode )
    {
        dragger.startDraggingComponent(draggedComponent, e);
    }
    
    lastClickPos = clickPos;
}

void CurveEditor::mouseUp(const MouseEvent& e)
{
    if( auto* curve = dynamic_cast<Curve*>(draggedComponent) )
    {
        curve->setIsDragged(false);
        curve->showDragArea(false);
        draggedComponent = nullptr;
        clickedCurve = nullptr;
    }
}

void CurveEditor::mouseDrag(const MouseEvent& e)
{
    auto clickPos = e.getEventRelativeTo(this).getPosition();
    
    if( auto* curve = dynamic_cast<Curve*>(draggedComponent) )
    {
        //drag the curve object
        for( auto* c : curves )
        {
            c->setIsDragged(false);
            c->showDragArea(false);
            c->showCurveArea(false);
        }
        
        curve->setIsDragged(true);
        
        dragger.dragComponent(draggedComponent, e, &constrainer);
        
        //update the normalized position for the curve's nodes
        updateCurvePosition(e, *curve);
        
        //check for dragging past nodes
        for( auto* node : controlPoints )
        {
            if( node != curve->left && node != curve->right )
            {
                if( node->getX() > curve->left->getX() && node->getX() < curve->right->getX() )
                {
                    deleteNode(node);
                    break;
                }
            }
        }
        
        //reposition the nodes.
        resized(); //this repositions the attached nodes, after their normalized position is updated.
        
        //update the curves that are attached to the nodes
        if( curve->left->attachedCurveLeft )
        {
            curve->left->attachedCurveLeft->repaint();
        }
        if( curve->right->attachedCurveRight )
        {
            curve->right->attachedCurveRight->repaint();
        }
    }
    else if( auto* node = dynamic_cast<CurveNode*>(draggedComponent) )
    {
        //dragging a node
        limitNodeX(*node);
        limitNodeY(*node);
        //update position of node
        updateNodeNormalizedPosition(*node);
        //update curve its attached to.
        
        int idx = controlPoints.indexOf(node);
        for( int i = 0; i < controlPoints.size(); ++i )
        {
            auto* otherNode = controlPoints[i];
            //DBG( "otherNode pos: " << otherNode->pos.x );
            if( node != otherNode )
            {
                if( (idx < i && otherNode->getX() < node->getX()) ||
                   (idx > i && otherNode->getX() > node->getX()))
                {
                    /*
                     originally we looked like this:
                     ------o---------o------
                         node      otherNode
                     
                     now we look like this:
                     ------o---------o------
                        otherNode   node
                     */
                    //the dragged node was originally earlier in controlPoints[], but is positioned AFTER
                    //the node that comes after it in controlPoints[]. delete the otherNode
                    deleteNode(otherNode);
                    break;
                }
            }
        }
        
        if( node->attachedCurveLeft )
        {
            resizeCurve(node->attachedCurveLeft);
        }
        
        if( node->attachedCurveRight )
        {
            resizeCurve(node->attachedCurveRight);
        }
    }
    else if( clickedCurve != nullptr )
    {
        //modifying the curve
        /*
         
         dragging changes the position of this control
         dragging up/down?  create normal curve (quadratic)
         dragging left/right? create Cubic S-curve
         
         if creating quadratic,
         the control node moves along the y = -x axis, from upper left to lower right
         */
        
        if( clickPos.x > lastClickPos.x || clickPos.x < lastClickPos.x )
        {
            //dragged to the right, make Integral shape
            //DBG( "drag right or left" );
            //every pixel corresponds to a 0.01 change of curve.controlX/Y
            if( std::abs(clickedCurve->controlY - 0.5) < 0.1 )
            {
                clickedCurve->controlY = 0.5;
                auto dif = clickPos.x - lastClickPos.x;
                clickedCurve->controlX += dif / 100.0;
                clickedCurve->controlX = jlimit(0.0, 1.0, clickedCurve->controlX);
            }
        }
        else if( clickPos.y <= lastClickPos.y || clickPos.y >= lastClickPos.y )
        {
            //DBG( "drag up or down" );
            //dragged to top of screen, control point goes to upper left
            //dragged to bottom of screen.  control point goes to lower right
            if( std::abs(clickedCurve->controlX - 0.5 ) < 0.1 )
            {
                clickedCurve->controlX = 0.5;
                auto dif = clickPos.y - lastClickPos.y;
                clickedCurve->controlY += dif / 100.0;
                clickedCurve->controlY = jlimit(0.0, 1.0, clickedCurve->controlY);
            }
        }
        clickedCurve->resized(); //reposition control
        clickedCurve->repaint(); //redraw curve based on control position
    }
    lastClickPos = clickPos;
}

void CurveEditor::mouseMove(const MouseEvent& e)
{
    auto clickPos = e.getEventRelativeTo(this).getPosition();
    Curve* c = nullptr;
    for( auto* curve : curves )
    {
        curve->showDragArea(false);
        if( curve->getBounds().contains(clickPos) )
        //if( curve->hitTest(clickPos.x, clickPos.y) )
        {
            c = curve;
        }
    }
    
    if( c )
    {
        c->showDragArea(true);
    }
}


void CurveEditor::mouseDoubleClick(const MouseEvent& e)
{
    //remove a node if you clicked on one
    //add a node if you didn't
    CurveNode* nodeToDelete = nullptr;
    auto clickPos = e.getEventRelativeTo(this).getPosition();
    
    for( auto* node : controlPoints )
    {
        //start and endNodes are not allowed to be deleted;
        if( node == startNode || node == endNode ) continue;
        //did we click on a node?
        if( node->getBounds().contains(clickPos) )
        {
            nodeToDelete = node;
            break;
        }
    }
    
    if( nodeToDelete )
    {
        deleteNode(nodeToDelete);
    }
    else
    {
        insertNodeInCurve(clickPos);
    }
    resized();
}

void CurveEditor::insertNodeInCurve(Point<int> clickPos)
{
    //create a new node
    auto* newNode = createControlNode();
    newNode->setCentrePosition(clickPos.x, clickPos.y);
    limitNodeX(*newNode);
    limitNodeY(*newNode);
    
    updateNodeNormalizedPosition(*newNode);
    
    //find the surrounding nodes
    CurveNode* left = nullptr;
    CurveNode* right = nullptr;
    
    for( auto* curve : curves )
    {
        if( curve->getBounds().contains(clickPos) )
        {
            left = curve->left;
            right = curve->right;
            break;
        }
    }
    
    if( left && right )
    {
        //delete the curve we just split with a node
        curves.removeObject(left->attachedCurveRight);
        //create 2 new curves
        createCurve(left, newNode);
        createCurve(newNode, right);
    }
    
    resortNodes();
}

void CurveEditor::deleteNode(CurveNode* nodeToDelete)
{
    CurveNode* newleft = nullptr;
    CurveNode* newRight = nullptr;
    //store node->attachedCurveLeft->left
    if( nodeToDelete->attachedCurveLeft && nodeToDelete->attachedCurveLeft->left )
    {
        newleft = nodeToDelete->attachedCurveLeft->left;
    }
    //store node->attachedCurveRight->right
    if( nodeToDelete->attachedCurveRight && nodeToDelete->attachedCurveRight->right )
    {
        newRight = nodeToDelete->attachedCurveRight->right;
    }
    
    if( newleft && newRight )
    {
        //delete node->attachedCurveLeft
        curves.removeObject(nodeToDelete->attachedCurveLeft);
        //delete node->attachedCurveRight
        curves.removeObject(nodeToDelete->attachedCurveRight);
        //create new curve using stored nodes
        createCurve(newleft, newRight);
        //delete node
        controlPoints.removeObject(nodeToDelete);
    }
    
    resortNodes();
}

void CurveEditor::updateNodeNormalizedPosition(CurveNode &node)
{
    auto nodeArea = getXYArea().toFloat();
    
    if( &node != startNode && &node != endNode )
        node.pos.x = (node.getBounds().getCentreX() - nodeArea.getX()) / nodeArea.getWidth();
    
    node.pos.y = 1 - (node.getBounds().getCentreY() - nodeArea.getY()) / nodeArea.getHeight();
    //DBG( "node pos: " << node.pos.toString() );
}

void CurveEditor::updateCurvePosition(const MouseEvent &e, Curve &curve)
{
    auto nodeArea = getNodeArea().toFloat();
    
    /*
     however much we've been dragged in either x/y direction is how much we need to change our node positions by
     but we need to convert that change into a normalized value
     */
    
    auto clickPos = e.getPosition();
    float changeX = (clickPos.x - lastClickPos.x) / nodeArea.getWidth();
    float changeY = (clickPos.y - lastClickPos.y) / nodeArea.getHeight();
    
    if( curve.left != startNode ) curve.left->pos.x += changeX;
    if( curve.right != endNode ) curve.right->pos.x += changeX;
    
    curve.left->pos.y -= changeY;
    curve.right->pos.y -= changeY;
    
    curve.left->pos.y = jlimit(0.0, 1.0, curve.left->pos.y);
    curve.left->pos.x = jlimit(0.0, 1.0, curve.left->pos.x);
    
    curve.right->pos.x = jlimit(0.0, 1.0, curve.right->pos.x);
    curve.right->pos.y = jlimit(0.0, 1.0, curve.right->pos.y);
}

CurveNode* CurveEditor::createControlNode()
{
    auto* node = controlPoints.add( new CurveNode(&constrainer) );
    node->addMouseListener(this, false);
    addAndMakeVisible(node);
    
    resortNodes();
    
    for( auto* node : controlPoints )
        node->toFront(true);
    return node;
}

void CurveEditor::resortNodes()
{
    struct C
    {
        static int compareElements (CurveNode* first, CurveNode* second)
        {
            if( first->pos.x < second->pos.x ) return -1;
            if( first->pos.x == second->pos.x ) return 0;
            return 1;
        }
    } comp;
    
    controlPoints.sort(comp);
    for( auto* node : controlPoints )
    {
        DBG( "node pos.x: " << node->pos.x );
    }
}

void CurveEditor::resortCurves()
{
    struct C
    {
        static int compareElements (Curve* first, Curve* second)
        {
            if( first->left->pos.x < second->left->pos.x ) return -1;
            if( first->left->pos.x == second->left->pos.x ) return 0;
            return 1;
        }
    } comp;
    
    curves.sort(comp);
}

Curve* CurveEditor::createCurve(CurveNode *left, CurveNode *right)
{
    auto* curve = curves.add( new Curve(&constrainer) );
    curve->linkLeft(*left);
    curve->linkRight(*right);
    curve->addMouseListener(this, false);
    addAndMakeVisible(curve);
    left->toBehind(right);
    curve->toBehind(left);
    resortCurves();
    return curve;
}

void CurveEditor::resizeCurve(Curve *curve)
{
    /*
     the curve "bounds" stretches from the top of the editor to the bottom, for the purpose of double-click detection
     */
    curve->setBounds(curve->left->getX(),
                     0,
                     curve->right->getRight() - curve->left->getX(),
                     getHeight());
    curve->resized(); //repositions control node
    curve->repaint();
}

void CurveEditor::limitNodeX(CurveNode& node)
{
    int x;
    if( &node == startNode )
    {
        x = startNodeXLimit.left;
    }
    else if( &node == endNode )
    {
        x = endNodeXLimit.left;
    }
    else
    {
        x = jlimit(startNode->getX() + CurveNode::NodeDiameter/2,
                   endNode->getX() - CurveNode::NodeDiameter/2,
                   node.getX());
    }
    constrainer.applyBoundsToComponent(node, node.getBounds().withX(x));
}

void CurveEditor::limitNodeY(CurveNode &node)
{
    //every node gets constrained on their y position
    auto xy = getNodeArea();
    auto y = jlimit(xy.getY(), xy.getBottom() - node.getHeight(), node.getY());
    constrainer.applyBoundsToComponent(node, node.getBounds().withY(y));
}
//==============================================================================
void BezierNode::mouseDown(const MouseEvent& e)
{
    dragger.startDraggingComponent(this, e);
}
void BezierNode::mouseDrag(const MouseEvent& e)
{
    dragger.dragComponent(this, e, constrainer);
}

void BezierNode::paint(Graphics& g)
{
    auto bounds = getLocalBounds().toFloat();
    g.setColour(Colours::red);
    g.fillEllipse(bounds);
    
    g.setColour(Colours::black);
    g.drawEllipse(bounds.reduced(1), 2);
    g.drawFittedText(n, getLocalBounds().reduced(3), Justification::centred, 1);
}
//==============================================================================
double Solvers::linearSolver(double in,
                             double min,
                             double max,
                             Point<double> a,
                             Point<double> b,
                             double /*h*/)
{
    auto t = jmap(in, min, max, 0.0, 1.0);
    Line<double> line(a, b);
    auto y = line.getPointAlongLineProportionally(t).y;
    return y;
//    return (h - y)/h;
}

double Solvers::piecewiseSolver(int x,
                                int xmin,
                                int xmax,
                                const std::vector<Point<double>>& points,
                                double bias,
                                double h )
{
    if( points.size() == 2 )
    {
        return linearSolver(x,
                            xmin,
                            xmax,
                            points.front(),
                            points.back(),
                            h);
    }
    
    //find the 2 nodes x is in between
    int leftIDX{-1}, rightIDX{-1};
    for( int i = 1; i < points.size(); ++i )
    {
        if( x >= points[i-1].x && x <= points[i].x )
        {
            leftIDX = i-1;
            rightIDX = i;
            //DBG( "between " << i-1 << " and " << i );
            break;
        }
    }
    
    if( leftIDX == -1 || rightIDX == -1 )
        return 0.0;
    
    if( bias == 1.0 )
    {
        //use linear solving for the whole path
        return linearSolver(x,
                            points[leftIDX].x,
                            points[rightIDX].x,
                            points[leftIDX],
                            points[rightIDX],
                            h);
    }
    
    /*
     left side and right side straight segments have special handling
     */
    if( leftIDX == 0 || rightIDX == points.size() - 1 )
    {
        Line<double> leftSide, rightSide;
        Point<double> leftNode, rightNode;
        double min, max;
        
        if( leftIDX == 0 )
        {
            leftSide = Line<double> {points[leftIDX], points[rightIDX]};
            rightSide = Line<double>{points[rightIDX], points[rightIDX+1]};
        }
        else
        {
            leftSide = Line<double> {points[leftIDX-1], points[leftIDX]};
            rightSide = Line<double>{points[leftIDX], points[rightIDX]};
        }
        
        leftNode = leftSide.getPointAlongLineProportionally(0.5 + bias/2.0);
        rightNode = rightSide.getPointAlongLineProportionally(0.5 - bias/2.0);
        
        if( x < leftNode.x && leftIDX == 0 )
        {   //between points[leftIDX] and leftnode of bezier curve
            return linearSolver(x,
                                points[leftIDX].x,
                                leftNode.x,
                                points[leftIDX],
                                leftNode,
                                h);
        }
        
        if( x > rightNode.x && rightIDX == points.size() - 1)
        { //between straight part after right node and end
            return linearSolver(x,
                                rightNode.x,
                                points[rightIDX].x,
                                rightNode,
                                points[rightIDX],
                                h);
        }
        
        /*
         left side and right side curved segments have special handling
         */
        
        if( leftIDX == 0 )
        { //between left node of bezier curve and points[rightIDX]
            auto r = BezierSolver(jmap(static_cast<double>(x),
                                       leftNode.x,
                                       rightNode.x,
                                       0.0,
                                       1.0),
                                  {leftNode, points[rightIDX], rightNode}).y;
            //return (h - r) / h;
            return r;
        }
        
        if( rightIDX == points.size() - 1 )
        { //between points[leftIDX] and rightNode on bezier curve
            auto r = BezierSolver(jmap(static_cast<double>(x),
                                       leftNode.x,
                                       rightNode.x,
                                       0.0,
                                       1.0),
                                  {leftNode, points[leftIDX], rightNode}).y;
            //return (h - r) / h;
            return r;
        }
    }
    
    /*
     Generic solution:
     it's between 2 nodes (N1, N2) that each control a bezier curve
     there are 3 possible places the point can exist
     (N0)---o--(N1)------o---------o--------(N2)---o--(N3)
           n1L          n1R       n2L             n2R
     between [N1, n1R]  <- bezier
     between [N1R, N2L] <- linear
     between [N2L, N2]  <- bezier
     */
    
    Line<double> N0N1(points[leftIDX-1], points[leftIDX] );
    Line<double> N1N2(points[leftIDX], points[rightIDX] );
    Line<double> N2N3(points[rightIDX], points[rightIDX+1]);
    
    //find n1R, n2L
    auto n1r = N1N2.getPointAlongLineProportionally(0.5 - bias/2.0); //rightSide of N1
    auto n2L = N1N2.getPointAlongLineProportionally(0.5 + bias/2.0); //leftSide of N2
    
    if( x <= n1r.x )
    {
        auto n1L = N0N1.getPointAlongLineProportionally(0.5 + bias/2.0);
        //along N1, n1R bezier
        auto r = BezierSolver(jmap(static_cast<double>(x), n1L.x, n1r.x, 0.0, 1.0),
                            { n1L, points[leftIDX], n1r }).y;
        //return (h - r) / h;
        return r;
    }
    if( x > n1r.x && x < n2L.x )
    {
        //along N1R, N2L linear
        return linearSolver(x, n1r.x, n2L.x, n1r, n2L, h);
    }
    
    //along N2L, N2 bezier
    auto n2R = N2N3.getPointAlongLineProportionally(0.5 - bias/2.0);
    auto r = BezierSolver(jmap(static_cast<double>(x), n2L.x, n2R.x, 0.0, 1.0),
                        {n2L,points[rightIDX],n2R}).y;
    //return (h - r) / h;
    return r;

}

Point<double> Solvers::BezierSolver(double t, const std::vector<Point<double> > &points)
{
    /*
     https://en.wikipedia.org/wiki/B%C3%A9zier_curve#Recursive_definition
     
     sum( binomialCoefficient(n,i) * (1-t)^(n-i) * t^i * p_i );
     */
    
    auto binomialCoefficient = [](int n, int i)
    {
        /*
         Commonly, a binomial coefficient is indexed by a pair of integers n ≥ k ≥ 0
         */
        jassert( n >= i );
        jassert( i >= 0 );
        
        return (Solvers::factorial(n)) / ( Solvers::factorial(i) * Solvers::factorial(n - i));
    };
    
    auto pow = [](double x, int k)
    {
        double intermediate = 1;
        for( int n = 0; n < k; ++n )
        {
            intermediate *= x;
        }
        return intermediate;
    };
    
    Point<double> out;
    size_t n = points.size() - 1;
    for( size_t i = 0; i < points.size(); ++i )
    {
        out += binomialCoefficient(static_cast<int>(n), static_cast<int>(i))
        * pow(1.0-t, static_cast<int>(n-i))
        * pow(t,static_cast<int>(i))
        * points[i];
    }
    return out;
}

int Solvers::factorial(int n)
{
    if( n > 1 ) return n * factorial(n-1);
    return 1;
}

double Solvers::ParabolaSolver(double x, const std::vector<Point<double> > &points)
{
    /*
     http://www.vb-helper.com/howto_find_quadratic_curve.html
     */
    jassert( points.size() == 3 );
    
    const double& x1 = points[0].x;
    const double& y1 = points[0].y;
    const double& x2 = points[1].x;
    const double& y2 = points[1].y;
    const double& x3 = points[2].x;
    const double& y3 = points[2].y;
    
    double A = ((y2-y1)*(x1-x3) + (y3-y1)*(x2-x1)) / ((x1-x3)*(x2*x2-x1*x1) + (x2-x1)*(x3*x3-x1*x1));
    double B = ((y2-y1) - A*(x2*x2 - x1*x1)) / (x2-x1);
    double C = y1 - A*x1*x1 - B*x1;
    
    return A * x * x + B * x + C;
}

//==============================================================================
QuadraticBezierComponent::QuadraticBezierComponent()
{
    addAndMakeVisible(controlNode);
    controlNode.addMouseListener(this, false);
    bias = -1;
}

void QuadraticBezierComponent::resized()
{
    BezierBase<QuadraticBezierComponent>::resized();
    controlNode.setCentrePosition(getLocalBounds().getBottomRight()+Point<int>(-controlNode.getWidth()/2,
                                                                               -controlNode.getHeight()/2));
}
void QuadraticBezierComponent::mouseDrag(const MouseEvent &e)
{
    repaint();
}

std::vector<Point<double>> QuadraticBezierComponent::getPoints()
{
    return
    {
        getPathStart(),
        controlNode.getBounds().getCentre().toDouble(),
        getPathEnd()
    };
}

Point<double> QuadraticBezierComponent::getPathStart()
{
    return getLocalBounds().toDouble().getBottomLeft()+Point<double>(BezierNode::nodeDiameter/2.0,
                                                                     -BezierNode::nodeDiameter/2.0);
}

Point<double> QuadraticBezierComponent::getPathEnd()
{
    return getLocalBounds().toDouble().getTopRight()+Point<double>(-BezierNode::nodeDiameter/2.0,
                                                                   BezierNode::nodeDiameter/2.0);
}
//==============================================================================
CubicBezierComponent::CubicBezierComponent()
{
    addAndMakeVisible(topRightNode);
    addAndMakeVisible(bottomLeftNode);
    //topRightNode.addMouseListener(this, false);
    topRightNode.setInterceptsMouseClicks(false, false);
    //bottomLeftNode.addMouseListener(this, false);
    bottomLeftNode.setInterceptsMouseClicks(false, false);
    bias = 0.0;
}

std::vector<Point<double>> CubicBezierComponent::getPoints()
{
    return
    {
        getPathStart(),
        bottomLeftNode.getBounds().getCentre().toDouble(),
        //getLocalBounds().getCentre().toDouble(),
        topRightNode.getBounds().getCentre().toDouble(),
        getPathEnd()
    };
}

Point<double> CubicBezierComponent::getPathStart()
{
    return getLocalBounds().toDouble().getBottomLeft()
           + Point<double>(BezierNode::nodeDiameter/2.0,
                           -BezierNode::nodeDiameter/2.0);
}

Point<double> CubicBezierComponent::getPathEnd()
{
    return getLocalBounds().toDouble().getTopRight()+Point<double>(-BezierNode::nodeDiameter/2.0,
                                              BezierNode::nodeDiameter/2.0);
}

void CubicBezierComponent::resized()
{
    BezierBase<CubicBezierComponent>::resized();
    topRightNode.setCentrePosition(getLocalBounds().getTopRight()+Point<int>(-topRightNode.getWidth()/2,
                                                                             topRightNode.getHeight()/2));
    bottomLeftNode.setCentrePosition(getLocalBounds().getBottomLeft()+Point<int>(bottomLeftNode.getWidth()/2, -
                                                                                 bottomLeftNode.getHeight()/2));
    lastTopRightNodePos = topRightNode.getBounds().getCentre();
    lastBottomLeftNodePos = bottomLeftNode.getBounds().getCentre();
}

void CubicBezierComponent::mouseDown(const MouseEvent& e)
{
    if( topRightNode.getBounds().contains(e.getPosition() ))
    {
        draggedNode = &topRightNode;
    }
    else if( bottomLeftNode.getBounds().contains(e.getPosition()))
    {
        draggedNode = &bottomLeftNode;
    }
    else
    {
        draggedNode = nullptr;
    }
}
void CubicBezierComponent::mouseDrag(const MouseEvent& e)
{
    //mirror the movements of each node on the y=x axis
    //limit the x position of each node to their respective half of the screen
    auto clickPos = e.getPosition();
    auto y = jlimit(BezierNode::nodeDiameter/2,
                    getHeight() - BezierNode::nodeDiameter/2,
                    clickPos.y);
    int x;
    if( draggedNode == &bottomLeftNode )
    {
        x = jlimit(BezierNode::nodeDiameter/2,
                   //getWidth() / 2,
                   getWidth() - BezierNode::nodeDiameter/2,
                   clickPos.x);
        draggedNode->setCentrePosition(x, y);
        auto dif = bottomLeftNode.getBounds().getCentre() - lastBottomLeftNodePos;
        topRightNode.setCentrePosition( topRightNode.getBounds().getCentre() - dif);
    }
    else if( draggedNode == &topRightNode )
    {
        x = jlimit(BezierNode::nodeDiameter / 2, //getWidth()/2,
                   getWidth() - BezierNode::nodeDiameter/2,
                   clickPos.x);
        draggedNode->setCentrePosition(clickPos.x, y);
        auto dif = topRightNode.getBounds().getCentre() - lastTopRightNodePos;
        bottomLeftNode.setCentrePosition( bottomLeftNode.getBounds().getCentre() - dif);
    }
    
    //if( draggedNode )
        
    
    
//    //this mirrors the movements of each node
//    if( e.originalComponent == &topRightNode )
//    {
//        DBG( "moved a1" );
//
//    }
//    else
//    {
//        DBG( "moved a2" );
//
//    }
    lastTopRightNodePos = topRightNode.getBounds().getCentre();
    lastBottomLeftNodePos = bottomLeftNode.getBounds().getCentre();
    repaint();
}
//==============================================================================
BezierBox::BezierBox()
{
    addAndMakeVisible(startNode);
    addAndMakeVisible(endNode);
    //disable ComponentDragger for these nodes, we'll handle it ourselves
    startNode.setInterceptsMouseClicks(false, false);
    endNode.setInterceptsMouseClicks(false, false);
}
void BezierBox::resized()
{
    BezierBase<BezierBox>::resized();
    startNode.setCentrePosition(getLocalBounds().getBottomLeft()
                                + Point<int>(BezierNode::nodeDiameter/2,
                                             -BezierNode::nodeDiameter/2));
    endNode.setCentrePosition(getLocalBounds().getTopRight()
                              + Point<int>(-BezierNode::nodeDiameter/2,
                                           BezierNode::nodeDiameter/2));
    updateNormalizedNodes();
}

void BezierBox::mouseDown(const MouseEvent& e)
{
    auto clickPos = e.getEventRelativeTo(this).getPosition();
    if( startNode.getBounds().contains(clickPos) )
    {
        draggedNode = &startNode;
    }
    else if( endNode.getBounds().contains(clickPos) )
    {
        draggedNode = &endNode;
    }
    else
    {
        draggedNode = nullptr;
    }
    updateNormalizedNodes();
}

void BezierBox::mouseMove(const MouseEvent& e)
{
    auto mousePos = e.getPosition();
//    DBG( "y = " << piecewiseSolver(mousePos.x,
//                                   getPathStart().x,
//                                   getPathEnd().x,
//                                   getPoints(),
//                                   bias,
//                                   getHeight() - BezierNode::nodeDiameter) );
}

void BezierBox::mouseDrag(const MouseEvent &e)
{
    auto clickPos = e.getEventRelativeTo(this).getPosition();
    auto y = jlimit(BezierNode::nodeDiameter/2,
                    getHeight() - BezierNode::nodeDiameter/2,
                    clickPos.y);
    
    bool needsRepainting = true;
    
    if( draggedNode == &startNode )
    {
        auto x = BezierNode::nodeDiameter / 2;
        startNode.setCentrePosition(x, y);
    }
    else if( draggedNode == &endNode )
    {
        auto x = getWidth() - BezierNode::nodeDiameter / 2;
        endNode.setCentrePosition(x, y);
    }
    else
    {
        //you dragged a user-created node.
        //prevent fold-backs in the path.
        
        /*
         check if node being dragged's x < previous node's x in list of nodes
         check if node being dragged's x > next nodes's x in list of nodes
         reorder node list,
         */
        BezierNode* dragged = nullptr;
        for( auto* node : nodes )
        {
            if( node->getBounds().contains(clickPos) )
            {
                dragged = node;
                break;
            }
        }
        
        if( dragged )
        {
            auto draggedIndex = nodes.indexOf(dragged);
            bool needsReordering = false;
            for( int i = 0; i < nodes.size(); ++i )
            {
                if(dragged->getX() < nodes[i]->getX() && i < draggedIndex)
                {
                    //dragged node has been moved before a node with a lower number
                    needsReordering = true;
                    nodes.swap(i, draggedIndex);
                    break;
                    
                }
                else if(nodes[i]->getX() < dragged->getX() && i > draggedIndex )
                {
                    needsReordering = true;
                    nodes.swap(i, draggedIndex);
                    break;
                }
            }
            
            if( needsReordering )
            {
                renumberNodes();
            }
        }
        else
        {
            needsRepainting = false;
        }
    }
    
    updateNormalizedNodes();
    if( needsRepainting )
    {
        repaint();
    }
    
}

void BezierBox::renumberNodes()
{
    for( int i = 0; i < nodes.size(); ++i )
    {
        nodes[i]->n = String(i+1);
        nodes[i]->repaint();
    }
}



void BezierBox::mouseDoubleClick(const MouseEvent &e)
{
    auto clickPos = e.getEventRelativeTo(this).getPosition();
    BezierNode* nodeToRemove = nullptr;
    for (auto* node : nodes)
    {
        if( node->getBounds().contains(clickPos) )
        {
            nodeToRemove = node;
            break;
        }
    }
    
    if( nodeToRemove )
    {
        nodes.removeObject(nodeToRemove);
        
    }
    else
    {
        if( nodes.size() < maxNodes )
        {
            int insertIndex = 0;
            //1) figure out which node is to the right of us
            for( auto* node : nodes )
            {
                if( node->getX() > clickPos.x )
                    break;
                ++insertIndex;
            }
            
            auto* insertedNode = nodes.insert(insertIndex, new BezierNode("", &constrainer));
            
            insertedNode->setCentrePosition(clickPos);
            insertedNode->addMouseListener(this, false);
            addAndMakeVisible(insertedNode);
        }
    }
    renumberNodes();
    updateNormalizedNodes();
    repaint();
}
std::vector<Point<double>> BezierBox::getPoints()
{
    std::vector<Point<double>> points;
    
    points.push_back(getPathStart());
    for( auto* node : nodes )
    {
        auto nodePos = node->getBounds().getCentre().toDouble();
        points.push_back( nodePos );
    }
    points.push_back(getPathEnd());
    return points;
}

Point<double> BezierBox::getPathStart()
{
    return startNode.getBounds().getCentre().toDouble();
}

Point<double> BezierBox::getPathEnd()
{
    return endNode.getBounds().getCentre().toDouble();
}

void BezierBox::setScale(double s) { scale = s; repaint(); }
//=============================================================
BezierWidget::BezierWidget()
{
    addAndMakeVisible(box);
    addAndMakeVisible(slider);
    
    slider.setRange(0.0, 1.0);
    slider.setValue(1.0);
    auto& nodes = box.getNodes();
    auto& animator = Desktop::getInstance().getAnimator();
    
    slider.onDragStart = [&, this]()
    {
        for( auto* node : nodes )
        {
            animator.animateComponent(node, node->getBounds(), 0.f, 300, false, 1.0, 1.0);
        }
    };
    slider.onDragEnd = [&, this]()
    {
        for( auto* node : nodes )
        {
            animator.animateComponent(node, node->getBounds(), 1.f, 300, false, 1.0, 1.0);
        }
    };
    slider.onValueChange = [this]()
    {
        //box.setScale( slider.getValue() );
        box.bias = slider.getValue();
        box.repaint();
    };
}

void BezierWidget::resized()
{
    auto bounds = getLocalBounds();
    box.setBounds(bounds.removeFromTop(getHeight()*0.8));
    slider.setBounds(bounds.reduced(getWidth()*0.3, 0));
}


